/**
 * copyback
 * (c) 2016 by Tristan Bauer
 */
(copyback) {

	(shader) {
		id = "copyback.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords = texcoords;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			out vec4 target;

			uniform sampler2D source;

			void main() {
				target = texture(source,vtexcoords);
			}
			"
		}
	}

}