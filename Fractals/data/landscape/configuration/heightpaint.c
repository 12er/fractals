/**
 * Resources for manual editing of the terrain
 * (c) 2016 by Tristan Bauer
 */
(heightpaint) {

	(vector) {
		id = "heightpaint.paintlocation.vector";
	}

	(float) {
		id = "heightpaint.size.float";
		value = 30;
	}

	(float) {
		id = "heightpaint.intensity.float";
		value = 0.02;
	}

	(float) {
		id = "heightpaint.raise.float";
		value = 1;
	}

	//do the actual painting
	//after a position of the paint tool was established

	(shader) {
		id = "heightpaint.movevertices.shader";
		(transformfeedback) {
			"out_vertices"
		}
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out vec3 out_vertices;

			uniform float maximumHeight;
			uniform float toolsize;
			uniform float intensity;
			uniform float raise;
			uniform vec4 toolposition;
			
			void main() {
				float d = distance(vertices,toolposition.xyz);
				float dr = d/toolsize;
				float influence = clamp(exp(-pow(dr,2.0))-0.00013, 0, 1);

				float h = vertices.z + influence * raise * intensity * maximumHeight;
				h = clamp(h, 0, maximumHeight);

				out_vertices = vec3(vertices.xy,h);
			}
			"
		}
	}

	(pass) {
		id = "heightpaint.movevertices1_2.pass";
		shader = "heightpaint.movevertices.shader";
		(floats) {
			maximumHeight = "terrain.maximumheight";
			toolsize = "heightpaint.size.float";
			intensity = "heightpaint.intensity.float";
			raise = "heightpaint.raise.float";
		}
		(vectors) {
			toolposition = "heightpaint.paintlocation.vector";
		}
		(objects) {
			"terrain.object1"
		}
		geometryoutput = "terrain.mesh2";
	}

	(pass) {
		id = "heightpaint.movevertices2_1.pass";
		shader = "heightpaint.movevertices.shader";
		(floats) {
			maximumHeight = "terrain.maximumheight";
			toolsize = "heightpaint.size.float";
			intensity = "heightpaint.intensity.float";
			raise = "heightpaint.raise.float";
		}
		(vectors) {
			toolposition = "heightpaint.paintlocation.vector";
		}
		(objects) {
			"terrain.object2"
		}
		geometryoutput = "terrain.mesh1";
	}

	//use a separate heightmap to influence the
	//heightvalues of the terrain vertices

	(texture) {
		id = "heightpaint.heightmap.texture";
		width = 2;
		height = 2;
		format = "float";
	}

	(matrix) {
		id = "heightpaint.heightmap.texcoord";
		(value) {
			(translate) {"0.5 0.5 0"}
			(scale) {"0.5 0.5 1"}
			(scale) {"0.0025 0.0025 1"}
		}
	}

	(texture) {
		id = "heightpaint.blendmap.texture";
		width = 2;
		height = 2;
		format = "float";
	}

	(matrix) {
		id = "heightpaint.blendmap.texcoord";
		(value) {
			(translate) {"0.5 0.5 0"}
			(scale) {"0.5 0.5 1"}
			(scale) {"0.0025 0.0025 1"}
		}
	}

	(float) {
		id = "heightpaint.useblendmap.float";
		value = 0;
	}

	//perform the dislocation of the vertices
	//depending on the heightmap and the blendmap

	(shader) {
		id = "heightpaint.applyheightmap.shader";
		(transformfeedback) {
			"out_vertices"
		}
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out vec3 out_vertices;

			uniform float useblendmap;
			uniform float maximumHeight;

			uniform mat4 heightmap_texcoord;
			uniform mat4 blendmap_texcoord;

			uniform sampler2D heightmap;
			uniform sampler2D blendmap;
			
			void main() {
				vec2 t_heightmap = (heightmap_texcoord * vec4(vertices,1)).xy;
				t_heightmap = clamp(t_heightmap,0.01,0.99);
				vec2 t_blendmap = (blendmap_texcoord * vec4(vertices,1)).xy;
				t_blendmap = clamp(t_blendmap,0.01,0.99);
				
				float oldheight = vertices.z;
				float newheight = texture(heightmap,t_heightmap).r * maximumHeight;
				float blendfactor = texture(blendmap,t_blendmap).r;
				
				//if the blendmap is not used
				//the heightvalues are solely determined
				//by newheight
				blendfactor = max(blendfactor, 1-useblendmap);

				//blendfactor determines how much
				//of newheight influences h
				
				float h = newheight * blendfactor + oldheight * (1 - blendfactor);

				out_vertices = vec3(vertices.xy,h);
			}
			"
		}
	}

	(pass) {
		id = "heightpaint.applyheightmap1_2.pass";
		shader = "heightpaint.applyheightmap.shader";
		(floats) {
			maximumHeight = "terrain.maximumheight";
			useblendmap = "heightpaint.useblendmap.float";
		}
		(matrices) {
			heightmap_texcoord = "heightpaint.heightmap.texcoord";
			blendmap_texcoord = "heightpaint.blendmap.texcoord";
		}
		(textures) {
			heightmap = "heightpaint.heightmap.texture";
			blendmap = "heightpaint.blendmap.texture";
		}
		(objects) {
			"terrain.object1"
		}
		geometryoutput = "terrain.mesh2";
	}

	(pass) {
		id = "heightpaint.applyheightmap2_1.pass";
		shader = "heightpaint.applyheightmap.shader";
		(floats) {
			maximumHeight = "terrain.maximumheight";
			useblendmap = "heightpaint.useblendmap.float";
		}
		(matrices) {
			heightmap_texcoord = "heightpaint.heightmap.texcoord";
			blendmap_texcoord = "heightpaint.blendmap.texcoord";
		}
		(textures) {
			heightmap = "heightpaint.heightmap.texture";
			blendmap = "heightpaint.blendmap.texture";
		}
		(objects) {
			"terrain.object2"
		}
		geometryoutput = "terrain.mesh1";
	}

}