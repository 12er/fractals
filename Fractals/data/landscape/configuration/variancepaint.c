/**
 * Resources for painting variance maps.
 * (c) 2016 by Tristan Bauer
 */

(variancepaint) {

	(rendertarget) {
		id = "variancepaint.rendertarget";
		width = 1024;
		height = 1024;
	}

	(texture) {
		id = "variancepaint.map.texture";
		width = 1024;
		height = 1024;
		format = "float";
	}

	(texture) {
		id = "variancepaint.tempbuffer.texture";
		width = 1024;
		height = 1024;
		format = "float";
	}

	(matrix) {
		id = "variancepaint.map.transform";
		(value) {
			(scale) {"0.0025 0.0025 1"}
		}
	}

	(matrix) {
		id = "variancepaint.map.texcoord";
		(value) {
			(translate) {"0.5 0.5 0"}
			(scale) {"0.5 0.5 1"}
			(scale) {"0.0025 0.0025 1"}
		}
	}

	(vector) {
		id = "variancepaint.paintlocation.vector";
	}

	(float) {
		id = "variancepaint.variance.float";
		value = 40000;
	}

	(float) {
		id = "variancepaint.h.float";
		value = 0.6;
	}

	(float) {
		id = "variancepaint.size.float";
		value = 30;
	}
	
	(float) {
		id = "variancepaint.influence.float";
		value = 1;
	}

	(float) {
		id = "variancepaint.intensity.float";
		value = 1;
	}

	//reset the variancemap

	(shader) {
		id = "variancemap.reset.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;

			void main() {
				gl_Position = vec4(vertices,1);
			}
			"
		}
		(fragment) {"
			#version 400 core
			out vec4 variancemap;
			
			void main() {
				variancemap = vec4(0,0,0,1);
			}
			"			
		}
	}

	(pass) {
		id = "variancepaint.reset.pass";
		shader = "variancemap.reset.shader";
		backgroundQuad = "true";
		(output) {
			rendertarget = "variancepaint.rendertarget";
			variancemap = "variancepaint.map.texture";
		}
	}
	
	//cast a ray starting from the position
	//of the mouse cursor, and find the first
	//point of intersection of the ray with the
	//heightmap

	(mesh) {
		id = "paint.source.mesh";
		faceSize = 1;
		maxVertexCount = 1;
		(attribute) {
			name = "mouseposition";
			attributeSize = 3;
			(data) {"
				3 1 4
				"
			}
		}
		//for some reason OpenGL
		//crashes without a second input
		//attribute, when TransformFeedback
		//emits more than one vertex attribute
		(attribute) {
			name = "fillattribute";
			attributeSize = 3;
			(data) {"
				3 1 4
				"
			}
		}
	}

	(object) {
		id = "paint.source.object";
		mesh = "paint.source.mesh";
	}

	(mesh) {
		id = "paint.toolposition.mesh";
		faceSize = 1;
		maxVertexCount = 1;
		(attribute) {
			name = "toolposition";
			attributeSize = 3;
		}
		(attribute) {
			name = "hasPosition";
			attributeSize = 1;
		}
	}

	(shader) {
		id = "paint.getposition.shader";
		(transformfeedback) {
			"toolposition"
			"hasPosition"
		}
		(vertex) {"
			#version 400 core
			in vec3 mouseposition;
			in vec3 fillattribute;
			out vec3 toolposition;
			out float hasPosition;

			uniform sampler2D heightmap;
			uniform vec4 viewerposition;
			uniform vec4 raycaststart;
			uniform vec4 raycastdirection;
			uniform mat4 heightvalues_tcoord;

			void main() {
				toolposition = vec3(0);
				hasPosition = 0;

				vec3 pos = raycaststart.xyz;
				float advancefactor = 1;
				vec2 lookup = ( heightvalues_tcoord * vec4(pos,1) ).xy;
				float h = texture(heightmap,lookup).r;
				bool aboveTerrain = h < pos.z;

				for(int i=0 ; i<1000 ; i++) {
					pos = pos + raycastdirection.xyz * advancefactor;
					lookup = ( heightvalues_tcoord * vec4(pos,1) ).xy;
					h = texture(heightmap,lookup).r;
					bool newAboveTerrain = h < pos.z;
					if(abs(pos.x) > 400 || abs(pos.y) > 400) {
						break;
					}
					if(aboveTerrain != newAboveTerrain) {
						toolposition = pos;
						hasPosition = 1;
						advancefactor = advancefactor * -0.1;
					}
					aboveTerrain = newAboveTerrain;
				}
			}
			"
		}
	}

	(pass) {
		id = "paint.getposition.pass";
		shader = "paint.getposition.shader";
		(vectors) {
			viewerposition = "viewer.position.vector";
			raycaststart = "paint.start.vector";
			raycastdirection = "paint.raydirection.vector";
		}
		(matrices) {
			heightvalues_tcoord = "normalmap.texcoord";
		}
		(textures) {
			heightmap = "normalmap.tempbuffer.texture";
		}
		(objects) {
			"paint.source.object"
		}
		geometryoutput = "paint.toolposition.mesh";
	}

	//do the actual painting
	//after a position of the paint tool was established

	(shader) {
		id = "variancepaint.paint.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords;
			out vec2 toolcoord;

			uniform mat4 variance_tcoord;
			uniform mat4 heightvalues_tcoord;
			uniform vec4 toolposition;

			void main() {
				gl_Position = vec4(vertices,1);
				toolcoord = ( variance_tcoord * toolposition ).xy;
				vtexcoords = texcoords;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			in vec2 toolcoord;
			out vec4 newMap;

			uniform sampler2D oldMap;
			uniform float toolsize;
			uniform float variance;
			uniform float H;
			uniform float influence;
			uniform float intensity;

			void main() {
				vec4 oldval = texture(oldMap,vtexcoords);
				float tsize = toolsize / 800.0;
				
				//calculate influence of painter
				//depending on the distance from
				//the painter's center
				vec3 fpos = vec3(vtexcoords,0);
				vec3 tpos = vec3(toolcoord,0);
				float distanceinfluence = clamp(1 - distance(fpos,tpos)/tsize , 0 , 1);
				distanceinfluence = clamp(distanceinfluence*2 , 0 , 1);
				distanceinfluence = distanceinfluence * intensity;

				//blend the original mapvalue with the painter's variance, H and influence
				newMap = oldval * (1-distanceinfluence) + vec4(variance,H,1,1)*influence*distanceinfluence;
			}
			"
		}
	}

	(pass) {
		id = "variancepaint.paint.pass";
		shader = "variancepaint.paint.shader";
		backgroundQuad = "true";
		(floats) {
			toolsize = "variancepaint.size.float";
			variance = "variancepaint.variance.float";
			H = "variancepaint.h.float";
			influence = "variancepaint.influence.float";
			intensity = "variancepaint.intensity.float";
		}
		(vectors) {
			toolposition = "variancepaint.paintlocation.vector";
		}
		(matrices) {
			variance_tcoord = "variancepaint.map.texcoord";
		}
		(textures) {
			oldMap = "variancepaint.map.texture";
		}
		(output) {
			rendertarget = "variancepaint.rendertarget";
			newMap = "variancepaint.tempbuffer.texture";
		}
	}

	(pass) {
		id = "variancepaint.copyback.pass";
		shader = "copyback.shader";
		backgroundQuad = "true";
		(textures) {
			source = "variancepaint.tempbuffer.texture";
		}
		(output) {
			rendertarget = "variancepaint.rendertarget";
			target = "variancepaint.map.texture";
		}
	}

	(effect) {
		id = "variancepaint.paint.effect";
		(passes) {
			"variancepaint.paint.pass"
			"variancepaint.copyback.pass"
		}
	}

}