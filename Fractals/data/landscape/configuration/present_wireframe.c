/**
 * Wireframe rendering
 * (c) 2016 by Tristan Bauer
 */
(present) {

	/**
	 * present effects
	 */

	(shader) {
		id = "present.wireframe.zbuffer.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out float zVal;

			uniform mat4 view;
			uniform mat4 projection;
			
			void main() {
				float distance = abs( ( view * vec4(vertices-vec3(0,0,1),1) ).z );
				gl_Position = projection * view * vec4(vertices-vec3(0,0,distance*0.003),1);
				zVal = (view * vec4(vertices,1)).z;
			}
			"
		}
		(fragment) {"
			#version 400 core
			out vec4 fragment;
			in float zVal;
			
			void main() {
				fragment = vec4(abs(zVal)/1000.0,0,0,1);
			}
			"
		}
	}

	(pass) {
		id = "present.wireframe.zbuffer.pass";
		shader = "present.wireframe.zbuffer.shader";
		(matrices) {
			view = "viewer.view.matrix";
			projection = "viewer.projection.matrix";
		}
		(output) {
			rendertarget = "present.target";
		}
	}

	(shader) {
		id = "present.wireframe.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;

			uniform mat4 view;
			uniform mat4 projection;

			void main() {
				gl_Position = projection * view * vec4(vertices,1);
			}
			"
		}
		(geometry) {"
			#version 400 core

			layout(triangles) in;
			layout(triangle_strip,max_vertices = 3) out;

			out float drawLine;

			void main() {
				for(int i=0 ; i<gl_in.length ; i++) {
					gl_Position = gl_in[i].gl_Position;
					drawLine = 0;
					if(i == 1) {
						drawLine = 1;
					}
					EmitVertex();
				}
				EndPrimitive();
			}
			"
		}
		(fragment) {"
			#version 400 core
			in float drawLine;
			out vec4 fragment;

			void main() {
				if(drawLine < 0.001) {
					discard;
				} else {
					fragment = vec4(1,0,0,1);
				}
			}
			"
		}
	}

	(pass) {
		id = "present.wireframe.pass";
		shader = "present.wireframe.shader";
		wireframe = "true";
		(matrices) {
			view = "viewer.view.matrix";
			projection = "viewer.projection.matrix";
		}
		(output) {
			clearDepthBuffer = "false";
			clearColorBuffer = "false";
			rendertarget = "present.target";
		}
	}

	(effect) {
		id = "present.wireframe.effect";
		(passes) {
			"present.wireframe.zbuffer.pass"
			"present.wireframe.pass"
		}
	}

}
