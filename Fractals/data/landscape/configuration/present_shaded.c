/**
 * Shaded rendering
 * (c) 2016 by Tristan Bauer
 */
(present) {

	(texture) {
		id = "present.reflectionmap.texture";
		width = 800;
		height = 600;
		format = "byte";
	}

	(vector) {
		id = "reflected.position.vector";
	}

	(matrix) {
		id = "reflected.view.matrix";
	}

	(texture) {
		id = "present.refractionmap.texture";
		width = 800;
		height = 600;
		format = "float";
	}

	(texture) {
		id = "present.underwater.texture";
		width = 800;
		height = 600;
		format = "float";
	}

	(rendertarget) {
		id = "present.offline.rendertarget";
		width = 800;
		height = 600;
	}

	(vector) {
		id = "present.lightdir";
		(value) {
			"-1" "-1" "-1"
		}
	}

	(vector) {
		id = "present.atmospherecolor.vector";
		(value) {"0.3451 0.4196 0.4666 1"}
	}

	(float) {
		id = "present.ambient";
		value = 0.25;
	}

	(float) {
		id = "present.daytime";
		value = 0.5;
	}

	(float) {
		id = "present.waterheight";
		value = 55;
	}

	(float) {
		id = "present.norefractionscene";
		value = 0;
	}

	(float) {
		id = "present.refractionscene";
		value = 1;
	}

	(float) {
		id = "present.noreflectionscene";
		value = 0;
	}

	(float) {
		id = "present.reflectionscene";
		value = 1;
	}

	(float) {
		id = "present.abovewater";
		value = 1;
	}

	(shader) {
		id = "present.shaded.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out vec3 world;
			out vec2 lookup;

			uniform mat4 view;
			uniform mat4 projection;
			uniform mat4 heightvalues_tcoord;

			void main() {
				gl_Position = projection * view * vec4(vertices,1);
				world = vertices;
				lookup = ( heightvalues_tcoord * vec4(vertices,1) ).xy;
				lookup = clamp(lookup,0.005,0.995);
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 lookup;
			in vec3 world;
			out vec4 fragment;

			uniform vec4 lightdir;
			uniform vec4 position;
			uniform vec4 atmospherecolor;
			uniform float ambient;
			uniform sampler2D normalmap;

			uniform float isRefractionscene;
			uniform float isReflectionscene;
			uniform float waterheight;
			uniform float abovewater;

			void discardControl() {
				if(isRefractionscene > 0.5 || isReflectionscene > 0.5) {
					bool cancelFrag = (world.z > waterheight);
					if(abovewater < 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(isReflectionscene > 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(cancelFrag) {
						discard;
					}
				}
				if(lookup.x < 0.01 || lookup.x > 0.99 || lookup.y < 0.01 || lookup.y > 0.99) {
					discard;
				}
			}

			void main() {
				discardControl();
				float d = distance(world.xyz,position.xyz);
				vec3 ldir = normalize(-lightdir.xyz);
				vec3 n = normalize(texture(normalmap,lookup).rgb);
				float diffuse = clamp( dot(n,ldir) , ambient , 1);
				float atmosphere = clamp((exp(d / 650.0) - 1)/2.71828182,0,1);
				fragment = vec4(0,1,0,1) * diffuse * (1-atmosphere) + atmospherecolor * atmosphere;
				fragment.a = d;
			}
			"
		}
	}

	(pass) {
		id = "present.reflection.pass";
		shader = "present.shaded.shader";
		(floats) {
			isRefractionscene = "present.norefractionscene";
			isReflectionscene = "present.reflectionscene";
			waterheight = "present.waterheight";
			abovewater = "present.abovewater";
			daytime = "present.daytime";
			ambient = "present.ambient";
		}
		(textures) {
			normalmap = "terrain.normalmap.texture";
		}
		(vectors) {
			lightdir = "present.lightdir";
			position = "reflected.position.vector";
			atmospherecolor = "present.atmospherecolor.vector";
		}
		(matrices) {
			view = "reflected.view.matrix";
			projection = "viewer.projection.matrix";
			heightvalues_tcoord = "normalmap.texcoord";
		}
		(output) {
			rendertarget = "present.offline.rendertarget";
			fragment = "present.reflectionmap.texture";
		}
	}

	(pass) {
		id = "present.underwaterreflection.pass";
		shader = "present.shaded.shader";
		(floats) {
			isRefractionscene = "present.norefractionscene";
			isReflectionscene = "present.reflectionscene";
			waterheight = "present.waterheight";
			abovewater = "present.abovewater";
			daytime = "present.daytime";
			ambient = "present.ambient";
		}
		(textures) {
			normalmap = "terrain.normalmap.texture";
		}
		(vectors) {
			lightdir = "present.lightdir";
			position = "reflected.position.vector";
			atmospherecolor = "present.atmospherecolor.vector";
		}
		(matrices) {
			view = "reflected.view.matrix";
			projection = "viewer.projection.matrix";
			heightvalues_tcoord = "normalmap.texcoord";
		}
		(output) {
			rendertarget = "present.offline.rendertarget";
			fragment = "present.underwater.texture";
		}
	}

	(pass) {
		id = "underwater.reflection.pass";
		shader = "underwater.todisplay.shader";
		backgroundQuad = "true";
		(vectors) {
			oceancolor = "water.oceancolor.vector";
			wave1 = "wave1.vector";
			wave2 = "wave2.vector";
		}
		(textures) {
			background = "present.underwater.texture";
			waves = "water.texture";
		}
		(output) {
			rendertarget = "present.offline.rendertarget";
			fragment = "present.reflectionmap.texture";
		}
	}

	(pass) {
		id = "present.refraction.pass";
		shader = "present.shaded.shader";
		(floats) {
			isRefractionscene = "present.refractionscene";
			isReflectionscene = "present.noreflectionscene";
			waterheight = "present.waterheight";
			abovewater = "present.abovewater";
			daytime = "present.daytime";
			ambient = "present.ambient";
		}
		(textures) {
			normalmap = "terrain.normalmap.texture";
		}
		(vectors) {
			lightdir = "present.lightdir";
			position = "viewer.position.vector";
			atmospherecolor = "present.atmospherecolor.vector";
		}
		(matrices) {
			view = "viewer.view.matrix";
			projection = "viewer.projection.matrix";
			heightvalues_tcoord = "normalmap.texcoord";
		}
		(output) {
			rendertarget = "present.offline.rendertarget";
			fragment = "present.refractionmap.texture";
		}
	}

	(pass) {
		id = "present.shaded.pass";
		shader = "present.shaded.shader";
		(floats) {
			isRefractionscene = "present.norefractionscene";
			isReflectionscene = "present.noreflectionscene";
			waterheight = "present.waterheight";
			abovewater = "present.abovewater";
			daytime = "present.daytime";
			ambient = "present.ambient";
		}
		(textures) {
			normalmap = "terrain.normalmap.texture";
			reflectionmap = "present.reflectionmap.texture";
			refractionmap = "present.refractionmap.texture";
		}
		(vectors) {
			lightdir = "present.lightdir";
			position = "viewer.position.vector";
			atmospherecolor = "present.atmospherecolor.vector";
		}
		(matrices) {
			view = "viewer.view.matrix";
			reflectedview = "reflected.view.matrix";
			projection = "viewer.projection.matrix";
			heightvalues_tcoord = "normalmap.texcoord";
		}
		(output) {
			rendertarget = "present.target";
		}
	}

	(pass) {
		id = "present.underwater.pass";
		shader = "present.shaded.shader";
		(floats) {
			isRefractionscene = "present.norefractionscene";
			isReflectionscene = "present.noreflectionscene";
			waterheight = "present.waterheight";
			abovewater = "present.abovewater";
			daytime = "present.daytime";
			ambient = "present.ambient";
		}
		(textures) {
			normalmap = "terrain.normalmap.texture";
			reflectionmap = "present.reflectionmap.texture";
			refractionmap = "present.refractionmap.texture";
		}
		(vectors) {
			lightdir = "present.lightdir";
			position = "viewer.position.vector";
			atmospherecolor = "present.atmospherecolor.vector";
		}
		(matrices) {
			view = "viewer.view.matrix";
			reflectedview = "reflected.view.matrix";
			projection = "viewer.projection.matrix";
			heightvalues_tcoord = "normalmap.texcoord";
		}
		(output) {
			rendertarget = "present.offline.rendertarget";
			fragment = "present.underwater.texture";
		}
	}

	(shader) {
		id = "underwater.todisplay.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords = texcoords;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			out vec4 fragment;

			uniform sampler2D background;
			uniform sampler2D waves;
			uniform vec4 oceancolor;
			uniform vec4 wave1;
			uniform vec4 wave2;

			void main() {
				vec3 normal = texture(waves,vtexcoords + wave1.xy + wave2.xy).xyz;
				normal = normalize(normal*2.0 - vec3(1));
				vec4 frag = texture(background,vtexcoords + normal.xy*0.03);
				float distance = clamp((exp(frag.a / 80.0) - 1)/2.71828182 , 0,1);
				fragment = frag * (1-distance) + oceancolor*2.0 * distance;
			}
			"
		}
	}

	(pass) {
		id = "underwater.todisplay.pass";
		shader = "underwater.todisplay.shader";
		backgroundQuad = "true";
		(vectors) {
			oceancolor = "water.oceancolor.vector";
			wave1 = "wave1.vector";
			wave2 = "wave2.vector";
		}
		(textures) {
			background = "present.underwater.texture";
			waves = "water.texture";
		}
		(output) {
			rendertarget = "present.target";
		}
	}

	(effect) {
		id = "present.shaded.effect";
		(passes) {
			"present.reflection.pass"
			"present.refraction.pass"
			"present.shaded.pass"
		}
	}

	(effect) {
		id = "present.underwater.effect";
		(passes) {
			"present.underwaterreflection.pass"
			"underwater.reflection.pass"
			"present.refraction.pass"
			"present.underwater.pass"
			"underwater.todisplay.pass"
		}
	}

}