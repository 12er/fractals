/**
 * Terrain creation, here the fractal
 * computations are happening.
 * (c) 2016 by Tristan Bauer
 */
(terrain) {

	//resources for terrain creation

	/**
	 * The red channel of this texture can be
	 * used as a random variable X ~ N(0,1).
	 */
	(texture) {
		id = "gaussiandistribution.texture";
		width = 2000;
		height = 2000;
		format = "float";
	}

	(texture) {
		id = "variancemap.texture";
		width = 2000;
		height = 2000;
		format = "float";
	}

	//Texture buffers holding terrain data
	//start out with a 4x4 heightmap
	//the connectivity of the heightmap points
	//depends if it's an even or an odd step.
	(texture) {
		id = "terrain.heightvalues.texture";
		width = 2;
		height = 2;
		format = "float";
	}

	(texture) {
		id = "terrain.normalmap.texture";
		width = 2;
		height = 2;
		format = "float";
	}

	(texture) {
		id = "normalmap.tempbuffer.texture";
		width = 2;
		height = 2;
		format = "float";
	}

	(rendertarget) {
		id = "terrain.normalmap.target";
		width = 2;
		height = 2;
	}

	(matrix) {
		id = "normalmap.transform";
	}

	(matrix) {
		id = "normalmap.texcoord";
	}

	(float) {
		id = "normalmap.pixelcount";
		value = 2;
	}

	(texture) {
		id = "terrain.tempbuffer.texture";
		width = 2;
		height = 2;
		format = "float";
	}

	(matrix) {
		id = "gaussiandistribution.transform";
		(value) {
			(translate) {"0.25 0.25 0"}
			(scale) {"0.5 0.5 1"}
			(translate) {"0.5 0.5 0"}
			(scale) {"0.00125 0.00125 1"}
		}
	}

	(float) {
		id = "terrain.stepcount";
		value = 0;
	}

	(float) {
		id = "terrain.sqrtvariance";
		value = 200;
	}

	(float) {
		id = "terrain.H";
		value = 0.6;
	}

	(float) {
		id = "terrain.initialheight";
		value = 50;
	}

	(float) {
		id = "terrain.maximumheight";
		value = 200;
	}

	//init variance map such that
	//on each location the global variance and H
	//value are used

	(shader) {
		id = "terrain.initvariancemap.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords = texcoords;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			out vec4 variancemap;

			void main() {
				variancemap = vec4(0,0,0,1);
			}
			"
		}
	}

	(rendertarget) {
		id = "terrain.initvariancemap.target";
		width = 2000;
		height = 2000;
	}

	(pass) {
		id = "terrain.initvariancemap.pass";
		shader = "terrain.initvariancemap.shader";
		backgroundQuad = "true";
		(output) {
			rendertarget = "terrain.initvariancemap.target";
			variancemap = "variancemap.texture";
		}
	}

	/**
	 * Starting with a rectangle. The mesh
	 * holding the vertex data is prepared
	 * for twenty subdivision steps. To go
	 * over 20 steps, reallocation of the 
	 * mesh must be done.
	 */
	(mesh) {
		id = "terrain.mesh";
		faceSize = 3;
		(attribute) {
			name = "vertices";
			attributeSize = 3;
			(data) {
				"-400 -400 0
				400 -400 0
				400 400 0
				400 400 0
				-400 400 0
				-400 -400 0"
			}
		}
	}

	(mesh) {
		id = "terrain.mesh1";
		maxVertexCount = 25165824;
		faceSize = 3;
		(attribute) {
			name = "vertices";
			outputName = "out_vertices";
			attributeSize = 3;
		}
	}

	(mesh) {
		id = "terrain.mesh2";
		maxVertexCount = 25165824;
		faceSize = 3;
		(attribute) {
			name = "vertices";
			outputName = "out_vertices";
			attributeSize = 3;
		}
	}

	/**
	 * RenderObject responsible for
	 * rendering the mesh on the screen.
	 */
	(object) {
		id = "terrain.object";
		mesh = "terrain.mesh";
	}

	(object) {
		id = "terrain.object1";
		mesh = "terrain.mesh1";
	}

	(object) {
		id = "terrain.object2";
		mesh = "terrain.mesh2";
		visible = "false";
	}

	//register the renderobject at the screen pass
	(pass) {
		id = "present.wireframe.zbuffer.pass";
		(objects) {
			"terrain.object1"
			"terrain.object2"
		}
	}

	(pass) {
		id = "present.wireframe.pass";
		(objects) {
			"terrain.object1"
			"terrain.object2"
		}
	}

	(pass) {
		id = "present.reflection.pass";
		(objects) {
			"terrain.object1"
			"terrain.object2"
		}
	}

	(pass) {
		id = "present.underwaterreflection.pass";
		(objects) {
			"terrain.object1"
			"terrain.object2"
		}
	}

	(pass) {
		id = "present.refraction.pass";
		(objects) {
			"terrain.object1"
			"terrain.object2"
		}
	}

	(pass) {
		id = "present.shaded.pass";
		(objects) {
			"terrain.object1"
			"terrain.object2"
		}
	}

	(pass) {
		id = "present.underwater.pass";
		(objects) {
			"terrain.object1"
			"terrain.object2"
		}
	}

	(pass) {
		id = "present.variance.pass";
		(objects) {
			"terrain.object1"
			"terrain.object2"
		}
	}

	(pass) {
		id = "present.height.pass";
		(objects) {
			"terrain.object1"
			"terrain.object2"
		}
	}

	//passes adjusting the maximum height

	(shader) {
		id = "terrain.clampheight.shader";
		(transformfeedback) {
			"out_vertices"
		}
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out vec3 out_vertices;

			uniform float maximumHeight;

			void main() {
				out_vertices = vertices;
				out_vertices.z = clamp(out_vertices.z,0,maximumHeight);
			}
			"
		}
	}

	(pass) {
		id = "terrain.clampheight.mesh1_to_mesh2";
		shader = "terrain.clampheight.shader";
		(floats) {
			maximumHeight = "terrain.maximumheight";
		}
		(objects) {
			"terrain.object1"
		}
		geometryoutput = "terrain.mesh2";
	}

	(pass) {
		id = "terrain.clampheight.mesh2_to_mesh1";
		shader = "terrain.clampheight.shader";
		(floats) {
			maximumHeight = "terrain.maximumheight";
		}
		(objects) {
			"terrain.object2"
		}
		geometryoutput = "terrain.mesh1";
	}

	//passes subdividing the terrain

	//first render heightvalues into a texture
	//then use a gaussian blur to reduce artifacts
	//the values in the texture are used to
	//access the heightvalues of all four vertices
	//in the geometry shader

	(float) {
		id = "terrain.heightvalues.kernelsize";
		value = 0.0015;
	}

	(matrix) {
		id = "terrain.heightvalues.transform";
	}

	(matrix) {
		id = "terrain.heightvalues.texcoord";
	}

	(rendertarget) {
		id = "terrain.heightvalues.target";
		width = 2;
		height = 2;
	}

	//render height values of terrain into texture
	(shader) {
		id = "terrain.heightvalues.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out float heightvalue;

			uniform mat4 transform;

			void main() {
				gl_Position = transform * vec4(vertices.xy,0,1);
				heightvalue = vertices.z;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in float heightvalue;
			out vec4 heightmap;

			void main() {
				heightmap = vec4(heightvalue,0,0,1);
			}
			"
		}
	}

	(pass) {
		id = "terrain.heightvalues.pass";
		shader = "terrain.heightvalues.shader";
		(matrices) {
			transform = "terrain.heightvalues.transform";
		}
		(objects) {
			"terrain.object1"
			"terrain.object2"
		}
		(output) {
			rendertarget = "terrain.heightvalues.target";
			heightmap = "terrain.heightvalues.texture";
		}
	}

	//separable gaussian filter removing high frequency artifacts
	//from terrain, vertical blur
	(shader) {
		id = "terrain.filterheight1.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords[15];

			uniform float kernelsize;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords[0] = texcoords + vec2(0,-1)*kernelsize;
				vtexcoords[1] = texcoords + vec2(0,-0.85714285)*kernelsize;
				vtexcoords[2] = texcoords + vec2(0,-0.71428571)*kernelsize;
				vtexcoords[3] = texcoords + vec2(0,-0.57142857)*kernelsize;
				vtexcoords[4] = texcoords + vec2(0,-0.42857142)*kernelsize;
				vtexcoords[5] = texcoords + vec2(0,-0.28571428)*kernelsize;
				vtexcoords[6] = texcoords + vec2(0,-0.14285714)*kernelsize;
				vtexcoords[7] = texcoords;
				vtexcoords[8] = texcoords + vec2(0,0.14285714)*kernelsize;
				vtexcoords[9] = texcoords + vec2(0,0.28571428)*kernelsize;
				vtexcoords[10] = texcoords + vec2(0,0.42857142)*kernelsize;
				vtexcoords[11] = texcoords + vec2(0,0.57142857)*kernelsize;
				vtexcoords[12] = texcoords + vec2(0,0.71428571)*kernelsize;
				vtexcoords[13] = texcoords + vec2(0,0.85714285)*kernelsize;
				vtexcoords[14] = texcoords + vec2(0,1)*kernelsize;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords[15];
			out vec4 filtered;

			uniform sampler2D unfiltered;

			void main() {
				filtered += texture(unfiltered,vtexcoords[0])*0.00442991210551132;
				filtered += texture(unfiltered,vtexcoords[1])*0.00895781211794;
				filtered += texture(unfiltered,vtexcoords[2])*0.0215963866053;
				filtered += texture(unfiltered,vtexcoords[3])*0.0443683338718;
				filtered += texture(unfiltered,vtexcoords[4])*0.0776744219933;
				filtered += texture(unfiltered,vtexcoords[5])*0.115876621105;
				filtered += texture(unfiltered,vtexcoords[6])*0.147308056121;
				filtered += texture(unfiltered,vtexcoords[7])*0.159576912161;
				filtered += texture(unfiltered,vtexcoords[8])*0.147308056121;
				filtered += texture(unfiltered,vtexcoords[9])*0.115876621105;
				filtered += texture(unfiltered,vtexcoords[10])*0.0776744219933;
				filtered += texture(unfiltered,vtexcoords[11])*0.0443683338718;
				filtered += texture(unfiltered,vtexcoords[12])*0.0215963866053;
				filtered += texture(unfiltered,vtexcoords[13])*0.00895781211794;
				filtered += texture(unfiltered,vtexcoords[14])*0.00442991210551132;
			}
			"
		}
	}

	(pass) {
		id = "terrain.filterheight1.pass";
		shader = "terrain.filterheight1.shader";
		backgroundQuad = "true";
		(floats) {
			kernelsize = "terrain.heightvalues.kernelsize";
		}
		(textures) {
			unfiltered = "terrain.heightvalues.texture";
		}
		(output) {
			rendertarget = "terrain.heightvalues.target";
			filtered = "terrain.tempbuffer.texture";
		}
	}

	//separable gaussian filter removing high frequency artifacts
	//from terrain, horizontal blur
	(shader) {
		id = "terrain.filterheight2.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords[15];

			uniform float kernelsize;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords[0] = texcoords + vec2(-1,0)*kernelsize;
				vtexcoords[1] = texcoords + vec2(-0.85714285,0)*kernelsize;
				vtexcoords[2] = texcoords + vec2(-0.71428571,0)*kernelsize;
				vtexcoords[3] = texcoords + vec2(-0.57142857,0)*kernelsize;
				vtexcoords[4] = texcoords + vec2(-0.42857142,0)*kernelsize;
				vtexcoords[5] = texcoords + vec2(-0.28571428,0)*kernelsize;
				vtexcoords[6] = texcoords + vec2(-0.14285714,0)*kernelsize;
				vtexcoords[7] = texcoords;
				vtexcoords[8] = texcoords + vec2(0.14285714,0)*kernelsize;
				vtexcoords[9] = texcoords + vec2(0.28571428,0)*kernelsize;
				vtexcoords[10] = texcoords + vec2(0.42857142,0)*kernelsize;
				vtexcoords[11] = texcoords + vec2(0.57142857,0)*kernelsize;
				vtexcoords[12] = texcoords + vec2(0.71428571,0)*kernelsize;
				vtexcoords[13] = texcoords + vec2(0.85714285,0)*kernelsize;
				vtexcoords[14] = texcoords + vec2(1,0)*kernelsize;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords[15];
			out vec4 filtered;

			uniform sampler2D unfiltered;

			void main() {
				filtered += texture(unfiltered,vtexcoords[0])*0.00442991210551132;
				filtered += texture(unfiltered,vtexcoords[1])*0.00895781211794;
				filtered += texture(unfiltered,vtexcoords[2])*0.0215963866053;
				filtered += texture(unfiltered,vtexcoords[3])*0.0443683338718;
				filtered += texture(unfiltered,vtexcoords[4])*0.0776744219933;
				filtered += texture(unfiltered,vtexcoords[5])*0.115876621105;
				filtered += texture(unfiltered,vtexcoords[6])*0.147308056121;
				filtered += texture(unfiltered,vtexcoords[7])*0.159576912161;
				filtered += texture(unfiltered,vtexcoords[8])*0.147308056121;
				filtered += texture(unfiltered,vtexcoords[9])*0.115876621105;
				filtered += texture(unfiltered,vtexcoords[10])*0.0776744219933;
				filtered += texture(unfiltered,vtexcoords[11])*0.0443683338718;
				filtered += texture(unfiltered,vtexcoords[12])*0.0215963866053;
				filtered += texture(unfiltered,vtexcoords[13])*0.00895781211794;
				filtered += texture(unfiltered,vtexcoords[14])*0.00442991210551132;
			}
			"
		}
	}

	(pass) {
		id = "terrain.filterheight2.pass";
		shader = "terrain.filterheight2.shader";
		backgroundQuad = "true";
		(floats) {
			kernelsize = "terrain.heightvalues.kernelsize";
		}
		(textures) {
			unfiltered = "terrain.tempbuffer.texture";
		}
		(output) {
			rendertarget = "terrain.heightvalues.target";
			filtered = "terrain.heightvalues.texture";
		}
	}

	(pass) {
		id = "normalmap.heightvalues.pass";
		shader = "terrain.heightvalues.shader";
		(matrices) {
			transform = "normalmap.transform";
		}
		(objects) {
			"terrain.object1"
			"terrain.object2"
		}
		(output) {
			rendertarget = "terrain.normalmap.target";
			heightmap = "normalmap.tempbuffer.texture";
		}
	}

	(pass) {
		id = "normalmap.filterheight1.pass";
		shader = "terrain.filterheight1.shader";
		backgroundQuad = "true";
		(floats) {
			kernelsize = "terrain.heightvalues.kernelsize";
		}
		(textures) {
			unfiltered = "normalmap.tempbuffer.texture";
		}
		(output) {
			rendertarget = "terrain.normalmap.target";
			filtered = "terrain.normalmap.texture";
		}
	}

	(pass) {
		id = "normalmap.filterheight2.pass";
		shader = "terrain.filterheight2.shader";
		backgroundQuad = "true";
		(floats) {
			kernelsize = "terrain.heightvalues.kernelsize";
		}
		(textures) {
			unfiltered = "terrain.normalmap.texture";
		}
		(output) {
			rendertarget = "terrain.normalmap.target";
			filtered = "normalmap.tempbuffer.texture";
		}
	}

	//compute normalmap of terrain
	(shader) {
		id = "terrain.normalmap.shader";
		(vertex) {"
			#version 400 core

			in vec3 vertices;
			in vec2 texcoords;
			out vec2 coords;

			void main() {
				coords = texcoords;
				gl_Position = vec4(vertices,1);
			}
			"
		}
		(fragment) {"
			#version 400 core

			in vec2 coords;
			out vec4 normalmap;

			uniform sampler2D heightmap;
			uniform float pixelcount;

			float getDCoord(float terrainwidth, float delta, vec2 direction) {
				float diff = texture2D(heightmap,coords + direction * delta).x - texture2D(heightmap,coords - direction * delta).x;
				return diff / (2 * delta * terrainwidth);
			}

			void main() {
				//terrain's max height is supposed
				//to have height 1/10 of terrainwidth
				
				float dx = getDCoord(800.0, 1/pixelcount, vec2(1,0));
				float dy = getDCoord(800.0, 1/pixelcount, vec2(0,1));
				
				vec3 normal = cross( vec3(1,0,dx) , vec3(0,1,dy) );
				normal = normalize(normal);

				normalmap = vec4(normal,1);
			}
			"
		}
	}

	(pass) {
		id = "terrain.normalmap.pass";
		shader = "terrain.normalmap.shader";
		backgroundQuad = "true";
		(floats) {
			pixelcount = "normalmap.pixelcount";
		}
		(textures) {
			heightmap = "normalmap.tempbuffer.texture";
		}
		(output) {
			rendertarget = "terrain.normalmap.target";
			normalmap = "terrain.normalmap.texture";
		}
	}

	(effect) {
		id = "terrain.heightvalues.effect";
		(passes) {
			"terrain.heightvalues.pass"
			"terrain.filterheight1.pass"
			"terrain.filterheight2.pass"
			"normalmap.heightvalues.pass"
			"normalmap.filterheight1.pass"
			"normalmap.filterheight2.pass"
			"terrain.normalmap.pass"
		}
	}

	//Here the actual subdivision takes place!
	//The diamond-square subdivision is done
	//in the geometry shader, operating on triangles
	//forming the quads of the terrain. The outcome is stored in
	//another mesh using OpenGl's transform-feedback.
	//Textures are used to access points outside
	//the current primitive.
	(shader) {
		id = "terrain.subdivide.shader";
		(transformfeedback) {
			"out_vertices"
		}
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out vec3 v_vertices;

			void main() {
				v_vertices = vertices;
			}
			"
		}
		(geometry) {"
			#version 400 core

			layout(triangles) in;
			layout(triangle_strip,max_vertices = 6) out;

			in vec3 v_vertices[];
			out vec3 out_vertices;

			uniform sampler2D Xtex;
			uniform sampler2D heightmap;
			uniform sampler2D variancemap;
			uniform mat4 randomDisplacement;
			uniform mat4 heightvalues_tcoord;
			uniform mat4 variance_tcoord;
			uniform float n;
			uniform float sqrtVariance;
			uniform float H;
			uniform float maximumHeight;
			uniform float initialHeight;

			vec3 X(vec3 p, float steps) {
				vec2 Xcoord = (randomDisplacement * vec4(p,1)).xy;
				float Xval = texture(Xtex,Xcoord).r;

				//apply variance and H from the variancemap
				//at location p
				vec2 vcoord = ( variance_tcoord * vec4(p,1) ).xy;
				vec3 vfrag = texture(variancemap,vcoord).xyz;
				float sqrtLocalVariance = sqrt(vfrag.x);
				float localH = vfrag.y;
				float localInfluence = vfrag.z;

				//blend global and local displacement
				//to obtain the actual displacement
				float dH = H * (1-localInfluence) + localH;
				float dSqrtVariance = sqrtVariance * (1-localInfluence) + sqrtLocalVariance;
				float displacement = Xval * dSqrtVariance / pow(2.0,max(steps*dH,0));
				return vec3(0,0,displacement);
			}

			float height(vec2 position) {
				vec2 lookup = ( heightvalues_tcoord * vec4(position,0,1) ).xy;
				lookup = clamp(lookup,0.01,0.99);
				return texture(heightmap,lookup).r;
			}

			void main() {
				//read positions of three vertices of the quad on the
				//x-y plane
				vec2 p0_temp = v_vertices[0].xy;
				vec2 p1_temp = v_vertices[1].xy;
				vec2 p2_temp = v_vertices[2].xy;

				//get the height value of each vertex from the heightmap
				//taking some of the height values from the original geometry
				//would cause problems on the border of the terrain

				//calculate p3
				vec2 m_temp = (p0_temp + p2_temp) * 0.5;
				vec2 p1_m = m_temp - p1_temp;
				vec3 p3 = vec3(m_temp + p1_m,0);
				p3.z = height(p3.xy);
				//calculate p0
				vec3 p0 = vec3(p0_temp,0);
				p0.z = height(p0.xy);
				//calculate p1
				vec3 p1 = vec3(p1_temp,0);
				p1.z = height(p1.xy);
				//calculate p2
				vec3 p2 = vec3(p2_temp,0);
				p2.z = height(p2.xy);

				//calculate midpoint
				vec3 m = (p0 + p1 + p2 + p3) * 0.25;
		
				//function X calculates displacements X depending on p0 ... p2, m
				//and parameters n, H and sqrtVariance
				//p3 is located on another triangle, hence p3 is emitted separately
				out_vertices = p1 + X(p1,n-1);
				out_vertices.z = clamp(out_vertices.z,0,maximumHeight);
				EmitVertex();
				out_vertices = m + X(m,n);
				out_vertices.z = clamp(out_vertices.z,0,maximumHeight);
				EmitVertex();
				out_vertices = p0 + X(p0,n-1);
				out_vertices.z = clamp(out_vertices.z,0,maximumHeight);
				EmitVertex();
				EndPrimitive();

				out_vertices = p2 + X(p2,n-1);
				out_vertices.z = clamp(out_vertices.z,0,maximumHeight);
				EmitVertex();
				out_vertices = m + X(m,n);
				out_vertices.z = clamp(out_vertices.z,0,maximumHeight);
				EmitVertex();
				out_vertices = p1 + X(p1,n-1);
				out_vertices.z = clamp(out_vertices.z,0,maximumHeight);
				EmitVertex();
				EndPrimitive();
			}
			"
		}
	}

	(pass) {
		id = "terrain.subdivide.mesh1_to_mesh2";
		shader = "terrain.subdivide.shader";
		(textures) {
			Xtex = "gaussiandistribution.texture";
			heightmap = "terrain.heightvalues.texture";
			variancemap = "variancepaint.map.texture";
		}
		(floats) {
			n = "terrain.stepcount";
			sqrtVariance = "terrain.sqrtvariance";
			H = "terrain.H";
			maximumHeight = "terrain.maximumheight";
			initialHeight = "terrain.initialheight";
		}
		(matrices) {
			randomDisplacement = "gaussiandistribution.transform";
			heightvalues_tcoord = "terrain.heightvalues.texcoord";
			variance_tcoord = "variancepaint.map.texcoord";
		}
		(objects) {
			"terrain.object1"
		}
		geometryoutput = "terrain.mesh2";
	}

	(pass) {
		id = "terrain.subdivide.mesh2_to_mesh1";
		shader = "terrain.subdivide.shader";
		(textures) {
			Xtex = "gaussiandistribution.texture";
			heightmap = "terrain.heightvalues.texture";
			variancemap = "variancepaint.map.texture";
		}
		(floats) {
			n = "terrain.stepcount";
			sqrtVariance = "terrain.sqrtvariance";
			H = "terrain.H";
			maximumHeight = "terrain.maximumheight";
			initialHeight = "terrain.initialheight";
		}
		(matrices) {
			randomDisplacement = "gaussiandistribution.transform";
			heightvalues_tcoord = "terrain.heightvalues.texcoord";
			variance_tcoord = "variancepaint.map.texcoord";
		}
		(objects) {
			"terrain.object2"
		}
		geometryoutput = "terrain.mesh1";
	}

	//pass resetting mesh1

	(shader) {
		id = "terrain.reset.shader";
		(transformfeedback) {
			"out_vertices"
		}
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out vec3 out_vertices;

			uniform float initialHeight;

			void main() {
				out_vertices = vec3(vertices.xy,initialHeight);
			}
			"
		}
	}

	(pass) {
		id = "terrain.reset.pass";
		shader = "terrain.reset.shader";
		(floats) {
			initialHeight = "terrain.initialheight";
		}
		(objects) {
			"terrain.object"
		}
		geometryoutput = "terrain.mesh1";
	}

}