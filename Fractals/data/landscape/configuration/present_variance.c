/**
 * Showing the variance map.
 * (c) 2016 by Tristan Bauer
 */
(present) {

	(float) {
		id = "variance.ambient";
		value = 0.4;
	}

	(shader) {
		id = "present.variance.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out vec2 tcoord;
			out vec2 vcoord;

			uniform mat4 view;
			uniform mat4 projection;
			uniform mat4 heightvalues_tcoord;
			uniform mat4 variance_tcoord;

			void main() {
				gl_Position = projection * view * vec4(vertices,1);
				tcoord = ( heightvalues_tcoord * vec4(vertices,1) ).xy;
				vcoord = ( variance_tcoord * vec4(vertices,1) ).xy;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 tcoord;
			in vec2 vcoord;
			out vec4 fragment;

			uniform sampler2D normalmap;
			uniform sampler2D variancemap;
			uniform vec4 lightdir;
			uniform float ambient;

			void main() {
				if(tcoord.x < 0.01 || tcoord.x > 0.99 || tcoord.y < 0.01 || tcoord.y > 0.99) {
					discard;
				}

				//calculate color representing the value
				//of the variance map on this location
				vec4 varianceval = texture(variancemap,vcoord);
				//scale varianceval for better visual representation
				varianceval.x = varianceval.z * sqrt(varianceval.x) / 316.22777;
				varianceval.y = varianceval.z * varianceval.y;
				varianceval = varianceval * 0.7 + 0.2;

				//calculate light influence
				vec3 normal = normalize(texture(normalmap,tcoord).xyz);
				vec3 ldir = normalize(-lightdir.xyz);
				float factor = max( dot(ldir,normal) , ambient);
				fragment = varianceval * factor;
			}
			"
		}
	}

	(pass) {
		id = "present.variance.pass";
		shader = "present.variance.shader";
		(matrices) {
			view = "viewer.view.matrix";
			projection = "viewer.projection.matrix";
			heightvalues_tcoord = "normalmap.texcoord";
			variance_tcoord = "variancepaint.map.texcoord";
		}
		(vectors) {
			lightdir = "present.lightdir";
		}
		(floats) {
			ambient = "variance.ambient";
		}
		(textures) {
			normalmap = "terrain.normalmap.texture";
			variancemap = "variancepaint.map.texture";
		}
		(output) {
			rendertarget = "present.target";
		}
	}

	//show the variance paint tool

	(matrix) {
		id = "variancepaint.variancepainter.model";
	}

	(object) {
		id = "present.variancepainter.object";
		mesh = "present.painter.mesh";
		(matrices) {
			model = "variancepaint.variancepainter.model";
		}
	}

	(shader) {
		id = "present.variancepaint.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;

			uniform mat4 model;
			uniform mat4 view;
			uniform mat4 projection;

			void main() {
				gl_Position = projection * view * model * vec4(vertices,1);
			}
			"
		}
		(fragment) {"
			#version 400 core
			out vec4 fragment;

			void main() {
				fragment = vec4(1,1,0,0.2);
			}
			"
		}
	}

	(pass) {
		id = "present.variancepaint.pass";
		shader = "present.variancepaint.shader";
		(matrices) {
			view = "viewer.view.matrix";
			projection = "viewer.projection.matrix";
			heightvalues_tcoord = "normalmap.texcoord";
		}
		(objects) {
			"present.variancepainter.object"
		}
		(output) {
			clearColorBuffer = "false";
			depthTest = "accept_always";
			fragmentBlendFactor = "fragmentalpha";
			targetBlendFactor = "one_minus_fragmentalpha";
			rendertarget = "present.target";
		}
	}

	(effect) {
		id = "present.variance.effect";
		(passes) {
			"present.variance.pass"
			"present.variancepaint.pass"
		}
	}

}