/**
 * Resources for water rendering.
 * (c) 2016 by Tristan Bauer
 */

(water) {

	(matrix) {
		id = "water.transform";
	}

	(matrix) {
		id = "mainobj.water.world2tex";
		(value) {
			(scale) {"0.05 0.05 0.05"}
		}
	}

	(vector) {
		id = "wave1.vector";
	}

	(vector) {
		id = "wave2.vector";
	}

	(vector) {
		id = "water.oceancolor.vector";
		(value) {"0.013333 0.02822 0.07214"}
	}

	(mesh) {
		id = "water.mesh";
		path = "../data/landscape/meshes/sparcegrid.ply";
	}

	(texture) {
		id = "water.texture";
		path = "../data/landscape/textures/waves.jpg";
	}

	(shader) {
		id = "water.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out vec4 world;
			out vec4 refraction;
			out vec4 reflection;
			out vec2 wavecoords;

			uniform mat4 model;
			uniform mat4 view;
			uniform mat4 reflectedview;
			uniform mat4 projection;
			uniform mat4 world2tex;

			void main() {
				world = model * vec4(vertices,1);
				wavecoords = ( world2tex * world ).xy;
				reflection = projection * reflectedview * world;
				refraction = projection * view * world;
				gl_Position = refraction;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec4 world;
			in vec4 refraction;
			in vec4 reflection;
			in vec2 wavecoords;
			out vec4 fragment;

			uniform sampler2D waves;
			uniform vec4 wave1;
			uniform vec4 wave2;
			uniform vec4 oceancolor;
			uniform vec4 atmospherecolor;
			uniform float waterheight;
			uniform float abovewater;

			uniform vec4 position;

			uniform sampler2D reflectionmap;
			uniform sampler2D refractionmap;

			void main() {
				//calculate fragment normal
				vec3 w1 = texture(waves,wavecoords + wave1.xy).xyz;
				vec3 w2 = texture(waves,wavecoords + wave2.xy).xyz;
				w1 = (w1 - vec3(0.5)) * 2.0;
				w2 = (w2 - vec3(0.5)) * 2.0;
				vec3 normal = normalize(w1 + w2);
				
				//get fragment from reflection
				//and from refraction
				vec2 c_reflection = reflection.xy / reflection.w;
				c_reflection = (c_reflection + vec2(1,1)) * 0.5;
				vec2 c_refraction = refraction.xy / refraction.w;
				c_refraction = (c_refraction + vec2(1,1)) * 0.5;

				float d = distance(world.xyz,position.xyz);
				float waterdepth = texture(refractionmap,c_refraction).a - d;
				float verticaldepth = clamp(waterdepth/50.0,0,1);
				if(abovewater < 0.5) {
					verticaldepth = clamp((waterheight - position.z)/50.0,0,1);
				}
				float waveinfluence = clamp(10.0*waterdepth/d,0,1);
				vec4 f_reflection = texture(reflectionmap,c_reflection + normal.xy * 0.1 * waveinfluence);
				vec4 f_refraction = texture(refractionmap,c_refraction + normal.xy * 0.1 * waveinfluence) * (1-verticaldepth) + oceancolor * verticaldepth;

				//how the reflection and refraction fragments are blended
				//depends on the viewing direction, and on the normalvector
				//of the water surface
				vec3 viewdir = normalize(world.xyz - position.xyz);
				float refractionfactor = abs( dot(normal,viewdir) );

				float atmosphere = clamp((exp(d / 650.0) - 1)/2.71828182,0,1);
				fragment = (f_refraction * refractionfactor + f_reflection * (1 - refractionfactor))*(1-atmosphere) + atmospherecolor*atmosphere;
				fragment.a = d;
			}
			"
		}
	}

	(object) {
		id = "water.object";
		shader = "water.shader";
		mesh = "water.mesh";
		(textures) {
			waves = "water.texture";
		}
		(vectors) {
			wave1 = "wave1.vector";
			wave2 = "wave2.vector";
			oceancolor = "water.oceancolor.vector";
		}
		(matrices) {
			model = "water.transform";
			world2tex = "mainobj.water.world2tex";
		}
	}

	(pass) {
		id = "present.shaded.pass";
		(objects) {
			"water.object"
		}
	}

	(pass) {
		id = "present.underwater.pass";
		(objects) {
			"water.object"
		}
	}

}