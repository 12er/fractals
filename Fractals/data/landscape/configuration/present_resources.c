/**
 * Common resources for rendering
 * (c) 2016 by Tristan Bauer
 */
(present) {

	/**
	 * viewer data
	 */

	(vector) {
		id = "viewer.position.vector";
		(value) {"300 300 300"}
	}

	(vector) {
		id = "viewer.view.vector";
		(value) {"-1 -1 -1"}
	}

	(vector) {
		id = "viewer.up.vector";
		(value) {"0 0 1"}
	}

	(matrix) {
		id = "viewer.view.matrix";
		(value) {
			(view) {
				(position) {"400 400 400"}
				(view) {"-1 -1 -1"}
				(up) {"0 0 1"}
			}
		}
	}

	(matrix) {
		id = "viewer.projection.matrix";
		(value) {
			(perspective) {
				angle = 1.57;
				znear = 1;
				zfar = 1500;
				width = 800;
				height = 600;
			}
		}
	}

	(float) {
		id = "present.width";
		value = 800;
	}

	(float) {
		id = "present.height";
		value = 600;
	}

	(rendertarget) {
		id = "present.target";
		width = 800;
		height = 600;
		renderToDisplay = "true";
	}

	(mesh) {
		id = "present.painter.mesh";
		path = "../data/landscape/meshes/sphere.ply";
	}

}