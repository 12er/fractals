/**
 * Rendering terrain on height paint
 * (c) 2016 by Tristan Bauer
 */
(present) {

	(float) {
		id = "height.ambient";
		value = 0.4;
	}

	(shader) {
		id = "present.height.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out vec2 tcoord;

			uniform mat4 view;
			uniform mat4 projection;
			uniform mat4 heightvalues_tcoord;

			void main() {
				gl_Position = projection * view * vec4(vertices,1);
				tcoord = ( heightvalues_tcoord * vec4(vertices,1) ).xy;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 tcoord;
			out vec4 fragment;

			uniform sampler2D normalmap;
			uniform vec4 lightdir;
			uniform float ambient;

			void main() {
				if(tcoord.x < 0.01 || tcoord.x > 0.99 || tcoord.y < 0.01 || tcoord.y > 0.99) {
					discard;
				}

				vec3 normal = normalize(texture(normalmap,tcoord).xyz);
				vec3 ldir = normalize(-lightdir.xyz);
				float factor = max( dot(ldir,normal) , ambient);
				fragment = vec4(factor,factor,factor,1);
			}
			"
		}
	}

	(pass) {
		id = "present.height.pass";
		shader = "present.height.shader";
		(matrices) {
			view = "viewer.view.matrix";
			projection = "viewer.projection.matrix";
			heightvalues_tcoord = "normalmap.texcoord";
		}
		(vectors) {
			lightdir = "present.lightdir";
		}
		(floats) {
			ambient = "height.ambient";
		}
		(textures) {
			normalmap = "terrain.normalmap.texture";
		}
		(output) {
			rendertarget = "present.target";
		}
	}

	//show the variance paint tool

	(matrix) {
		id = "heightpaint.heightpainter.model";
	}

	(object) {
		id = "present.heightpainter.object";
		mesh = "present.painter.mesh";
		(matrices) {
			model = "heightpaint.heightpainter.model";
		}
	}

	(object) {
		id = "present.waterplane.object";
		mesh = "terrain.mesh";
		(matrices) {
			model = "present.waterplane.matrix";
		}
	}

	(shader) {
		id = "present.planes.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;

			uniform mat4 model;
			uniform mat4 view;
			uniform mat4 projection;

			void main() {
				gl_Position = projection * view * model * vec4(vertices,1);
			}
			"
		}
		(fragment) {"
			#version 400 core
			out vec4 fragment;

			void main() {
				fragment = vec4(0,0,1,0.2);
			}
			"
		}
	}

	(pass) {
		id = "present.planes.pass";
		shader = "present.planes.shader";
		(matrices) {
			view = "viewer.view.matrix";
			projection = "viewer.projection.matrix";
		}
		(objects) {
			"present.waterplane.object"
		}
		(output) {
			clearColorBuffer = "false";
			clearDepthBuffer = "false";
			fragmentBlendFactor = "fragmentalpha";
			targetBlendFactor = "one_minus_fragmentalpha";
			rendertarget = "present.target";
		}
	}

	(shader) {
		id = "present.heightpaint.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;

			uniform mat4 model;
			uniform mat4 view;
			uniform mat4 projection;

			void main() {
				gl_Position = projection * view * model * vec4(vertices,1);
			}
			"
		}
		(fragment) {"
			#version 400 core
			out vec4 fragment;

			void main() {
				fragment = vec4(1,1,0,0.2);
			}
			"
		}
	}

	(pass) {
		id = "present.heightpaint.pass";
		shader = "present.heightpaint.shader";
		(matrices) {
			view = "viewer.view.matrix";
			projection = "viewer.projection.matrix";
		}
		(objects) {
			"present.heightpainter.object"
		}
		(output) {
			clearColorBuffer = "false";
			depthTest = "accept_always";
			fragmentBlendFactor = "fragmentalpha";
			targetBlendFactor = "one_minus_fragmentalpha";
			rendertarget = "present.target";
		}
	}

	(effect) {
		id = "present.height.effect";
		(passes) {
			"present.height.pass"
			"present.planes.pass"
			"present.heightpaint.pass"
		}
	}

}