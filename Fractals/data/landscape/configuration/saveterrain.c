/**
 * Resources for transforming the terrain before saving it.
 * (c) 2016 by Tristan Bauer
 */
(saveterrain) {

	(texture) {
		id = "saveterrain.heightmap.texture";
		width = 2;
		height = 2;
		format = "float";
	}

	(texture) {
		id = "saveterrain.normalmap.texture";
		width = 2;
		height = 2;
		format = "float";
	}

	(shader) {
		id = "saveterrain.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords = texcoords;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			out vec4 heightmap;
			out vec4 normalmap;

			uniform sampler2D originalHeightmap;
			uniform sampler2D originalNormalmap;
			uniform float maximumHeight;

			void main() {
				//scale heightvalues from [0,maximumHeight] to [0,1]
				float h = texture(originalHeightmap,vtexcoords).r;
				h = h / maximumHeight;
				
				//transform normal into a positive vector
				vec3 n = texture(originalNormalmap,vtexcoords).rgb;
				n = normalize(n);
				n = (n + vec3(1))*0.5;

				heightmap = vec4(h,0,0,1);
				normalmap = vec4(n,1);
			}
			"
		}
	}

	(pass) {
		id = "saveterrain.pass";
		shader = "saveterrain.shader";
		backgroundQuad = "true";
		(floats) {
			maximumHeight = "terrain.maximumheight";
		}
		(textures) {
			originalHeightmap = "terrain.heightvalues.texture";
			originalNormalmap = "terrain.normalmap.texture";
		}
		(output) {
			rendertarget = "terrain.heightvalues.target";
			heightmap = "saveterrain.heightmap.texture";
			normalmap = "saveterrain.normalmap.texture";
		}
	}

}