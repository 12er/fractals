/**
 * Resources for rendering the sky.
 * (c) 2016 by Tristan Bauer
 */
(sky) {

	(texture) {
		id = "sky.skymap";
		path = "../data/landscape/textures/skymap.bmp";
	}

	(matrix) {
		id = "sky.position.matrix";
		(value) {
			(translate) {"400 400 400"}
			(scale) {"1400 1400 1400"}
		}
	}

	(matrix) {
		id = "sky.reflectedposition.matrix";
	}

	(shader) {
		id = "sky.shader";
		(vertex) {"
			#version 400 core

			in vec3 vertices;
			out vec4 world;
			out vec2 coords;

			uniform mat4 model;
			uniform mat4 view;
			uniform mat4 projection;

			void main() {
				coords = vertices.xy;
				world = model * vec4(vertices,1);
				gl_Position = projection * view * world;
			}
			"
		}
		(fragment) {"
			#version 400 core

			in vec2 coords;
			in vec4 world;
			out vec4 fragment;

			uniform vec4 lightdir;
			uniform float isRefractionscene;
			uniform float isReflectionscene;
			uniform float waterheight;
			uniform float abovewater;
			uniform vec4 position;
			uniform float daytime;
			uniform sampler2D skymap;

			void discardControl() {
				if(isRefractionscene > 0.5 || isReflectionscene > 0.5) {
					bool cancelFrag = (world.z > waterheight);
					if(abovewater < 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(isReflectionscene > 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(cancelFrag) {
						discard;
					}
				}
			}

			void main() {
				discardControl();
				vec3 fragdir = normalize(world.xyz - position.xyz);
				float height = max(0,dot(fragdir,vec3(0,0,1)) * 0.75);
				fragment = texture2D(skymap,vec2(daytime,height));
				fragment.a = distance(world.xyz, position.xyz);
			}
			"
		}
	}

	(mesh) {
		id = "sky.mesh";
		path = "../data/landscape/meshes/sparcesphere.ply";
	}

	(object) {
		id = "sky.object";
		mesh = "sky.mesh";
		shader = "sky.shader";
		(textures) {
			skymap = "sky.skymap";
		}
		(vectors) {
			position = "viewer.position.vector";
		}
		(matrices) {
			model = "sky.position.matrix";
		}
	}

	(object) {
		id = "sky.reflected.object";
		mesh = "sky.mesh";
		shader = "sky.shader";
		(textures) {
			skymap = "sky.skymap";
		}
		(vectors) {
			position = "reflected.position.vector";
		}
		(matrices) {
			model = "sky.reflectedposition.matrix";
		}
	}

	//add sky to passes

	(pass) {
		id = "present.reflection.pass";
		(objects) {
			"sky.reflected.object"
		}
	}

	(pass) {
		id = "present.underwaterreflection.pass";
		(objects) {
			"sky.reflected.object"
		}
	}

	(pass) {
		id = "present.refraction.pass";
		(objects) {
			"sky.object"
		}
	}

	(pass) {
		id = "present.shaded.pass";
		(objects) {
			"sky.object"
		}
	}

	(pass) {
		id = "present.underwater.pass";
		(objects) {
			"sky.object"
		}
	}

}