/**
 * Resources
 * (c) 2016 by Tristan Bauer
 */

(present) {

	(float) {
		id = "present.width";
		value = 800;
	}

	(float) {
		id = "present.height";
		value = 600;
	}

	(rendertarget) {
		id = "present.target";
		width = 800;
		height = 600;
		renderToDisplay = "true";
	}

	(rendertarget) {
		id = "present.offline.target";
		width = 800;
		height = 600;
	}

	(texture) {
		id = "present.levelcolors.texture";
		path = "../data/mandelbrot/textures/Levelcolors.png";
	}

	(mesh) {
		id = "fractal.mesh";
		faceSize = 3;
		(attribute) {
			name = "vertices";
			attributeSize = 3;
			(data) {"
				-1 -1 0
				1 -1 0
				1 1 0
				-1 -1 0
				1 1 0
				-1 1 0
				"
			}
		}
		(attribute) {
			name = "texcoords";
			attributeSize = 2;
			(data) {"
				0 0
				1 0
				1 1
				0 0
				1 1
				0 1
				"
			}
		}
	}

	(matrix) {
		id = "present.mandelbrot.transform";
		(value) {
			(scale) {"1 1 1"}
		}
	}

	(object) {
		id = "mandelbrot.object";
		mesh = "fractal.mesh";
		(matrices) {
			transform = "present.mandelbrot.transform";
		}
		(textures) {
			fractaltex = "mandelbrot.texture";
		}
		(passes) {
			"present.pass"
		}
	}

	(matrix) {
		id = "present.julia.transform";
		(value) {
			(translate) {"0.75 -0.75 -0.1"}
			(scale) {"0.25 0.25 1"}
		}
	}

	(object) {
		id = "julia.object";
		mesh = "fractal.mesh";
		(matrices) {
			transform = "present.julia.transform";
		}
		(textures) {
			fractaltex = "julia.texture";
		}
		(passes) {
			"present.pass"
		}
	}

	(shader) {
		id = "presentfractal.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords;

			uniform mat4 transform;

			void main() {
				gl_Position = transform * vec4(vertices,1);
				vtexcoords = texcoords;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			out vec4 fragment;

			uniform sampler2D fractaltex;

			void main() {
				fragment = texture(fractaltex,vtexcoords);
			}
			"
		}
	}

	(pass) {
		id = "present.pass";
		shader = "presentfractal.shader";
		(output) {
			rendertarget = "present.target";
		}
	}

}