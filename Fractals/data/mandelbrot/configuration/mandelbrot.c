/**
 * Mandelbrot fractal resources
 * (c) 2016 by Tristan Bauer
 */

(mandelbrot) {

	(float) {
		id = "maxcount.float";
		value = 200;
	}
	
	(double) {
		id = "mandelbrot.imaginaryDelta.float";
		value = 2;
	}

	(dvector) {
		id = "mandelbrot.imagecenter.vector";
		(value) {"0 0"}
	}

	(dmatrix) {
		id = "mandelbrot.transform";
		(value) {
			(scale) {"1.3333 1 1"}
		}
	}

	(texture) {
		id = "mandelbrot.texture";
		width = 800;
		height = 600;
		format = "float";
	}

	/**
	 * Here the actual calculations for the Mandelbrot fractal 
	 * are happening, 32 bits are used for the floating point computations.
	 * For each fragment iterations are done until
	 * it's proven that the fragment does not belong
	 * to the mandelbrot set (the trajectory leads outside
	 * a circle of radius two around the centerpoint).
	 * For non-mandelbrot fragments a smooth colorcoding is
	 * used to visualize the number of iterations.
	 */
	(shader) {
		id = "mandelbrot.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out vec2 imaginary;

			uniform dmat4 transform;

			void main() {
				gl_Position = vec4(vertices,1);
				imaginary = (mat4(transform) * vec4(vertices,1)).xy;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 imaginary;
			out vec4 mandelbrot;

			uniform float maxCount;
			uniform sampler2D levelcolors;

			//mult. of imaginary numbers
			vec2 imgMult(vec2 z1, vec2 z2) {
				return vec2( 
					z1.x * z2.x - z1.y * z2.y , 
					z1.x * z2.y + z1.y * z2.x
					);
			}

			//euclidean norm of imaginary number
			float imgNorm(vec2 z) {
				return sqrt(z.x*z.x + z.y*z.y);
			}

			void main() {
				float i=0;
				vec2 z = vec2(0);

				//The fragment is assumed to belong
				//to the mandelbrot set as long as proven otherwise.
				//That's done by following the trajectory of the centerpoint.
				bool isMandelbrot = true;
				for( ; i<maxCount ; i++) {
					float inorm = imgNorm(z);
					//Divergence detected, hence not a mandelbrot point!
					if(inorm > 2.0) {
						isMandelbrot = false;
					}
					if(inorm > 12.0) {
						isMandelbrot = false;
						break;
					}
					//Calculate the next position on the trajectory
					//by squaring the last point, and adding the image position to it.
					z = imgMult(z,z) + imaginary;
				}
				//Colorcode number of iterations, use norm of last trajectory point
				//to get a smooth colorcoding.
				float level = i + 1.0 - log2(log2(pow(imgNorm(z),2.0))*0.5);
				level = sqrt(level);
				mandelbrot = texture(levelcolors,vec2(level,0));
				//Mandelbrot points are colored black.
				if(isMandelbrot) {
					mandelbrot = vec4(0,0,0,1);
				}
			}
			"
		}
	}

	(pass) {
		id = "mandelbrot.pass";
		shader = "mandelbrot.shader";
		backgroundQuad = "true";
		(floats) {
			maxCount = "maxcount.float";
		}
		(dmatrices) {
			transform = "mandelbrot.transform";
		}
		(textures) {
			levelcolors = "present.levelcolors.texture";
		}
		(output) {
			rendertarget = "present.offline.target";
			mandelbrot = "mandelbrot.texture";
		}
	}

	/**
	 * Here the actual calculations for the Mandelbrot fractal 
	 * are happening, 64 bits are used for the floating point computations.
	 * For each fragment iterations are done until
	 * it's proven that the fragment does not belong
	 * to the mandelbrot set (the trajectory leads outside
	 * a circle of radius two around the centerpoint).
	 * For non-mandelbrot fragments a smooth colorcoding is
	 * used to visualize the number of iterations.
	 */
	(shader) {
		id = "mandelbrot64.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out vec2 imag;

			void main() {
				gl_Position = vec4(vertices,1);
				imag = vertices.xy;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 imag;
			out vec4 mandelbrot;

			uniform float maxCount;
			uniform dmat4 transform;
			uniform sampler2D levelcolors;

			//mult. of imaginary numbers
			dvec2 imgMult(dvec2 z1, dvec2 z2) {
				return dvec2( 
					z1.x * z2.x - z1.y * z2.y , 
					z1.x * z2.y + z1.y * z2.x
					);
			}

			//euclidean norm of imaginary number
			double imgNorm(dvec2 z) {
				return sqrt(z.x*z.x + z.y*z.y);
			}

			void main() {
				dvec2 imaginary = (transform * dvec4(imag,0,1)).xy;
				float i=0;
				dvec2 z = dvec2(0);
				//The fragment is assumed to belong
				//to the mandelbrot set as long as proven otherwise.
				//That's done by following the trajectory of the centerpoint.
				bool isMandelbrot = true;
				for( ; i<maxCount ; i++) {
					double inorm = imgNorm(z);
					//Divergence detected, hence not a mandelbrot point!
					if(inorm > 2.0) {
						isMandelbrot = false;
					}
					if(inorm > 12.0) {
						isMandelbrot = false;
						break;
					}
					//Calculate the next position on the trajectory
					//by squaring the last point, and adding the image position to it.
					z = imgMult(z,z) + imaginary;
				}
				//Colorcode number of iterations, use norm of last trajectory point
				//to get a smooth colorcoding.
				float level = i + 1.0 - log2(log2(pow(float(imgNorm(z)),2.0))*0.5);
				level = sqrt(level);
				//Mandelbrot points are colored black.
				mandelbrot = texture(levelcolors,vec2(level,0));
				if(isMandelbrot) {
					mandelbrot = vec4(0,0,0,1);
				}
			}
			"
		}
	}

	(pass) {
		id = "mandelbrot64.pass";
		shader = "mandelbrot64.shader";
		backgroundQuad = "true";
		(floats) {
			maxCount = "maxcount.float";
		}
		(dmatrices) {
			transform = "mandelbrot.transform";
		}
		(textures) {
			levelcolors = "present.levelcolors.texture";
		}
		(output) {
			rendertarget = "present.offline.target";
			mandelbrot = "mandelbrot.texture";
		}
	}

}