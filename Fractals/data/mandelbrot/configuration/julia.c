/**
 * Julia fractal resources
 * (c) 2016 by Tristan Bauer
 */

(julia) {

	(double) {
		id = "julia.imaginaryDelta.float";
		value = 2;
	}

	(dvector) {
		id = "julia.imagecenter.vector";
		(value) {"0 0"}
	}

	(dmatrix) {
		id = "julia.transform";
		(value) {
			(scale) {"1.3333 1 1"}
		}
	}

	(dvector) {
		id = "julia.c";
		(value) {"0 0 0"}
	}

	(texture) {
		id = "julia.texture";
		width = 800;
		height = 600;
		format = "float";
	}

	/**
	 * Here the actual calculations for the Julia fractal 
	 * are happening, 32 bits are used for the floating point computations.
	 * For each fragment iterations are done until
	 * it's proven that the fragment does not belong
	 * to the julia set (the trajectory leads outside
	 * a circle of radius two around the centerpoint).
	 * For non-julia fragments a smooth colorcoding is
	 * used to visualize the number of iterations.
	 */
	(shader) {
		id = "julia.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out vec2 imaginary;

			uniform dmat4 transform;

			void main() {
				gl_Position = vec4(vertices,1);
				imaginary = (mat4(transform) * vec4(vertices,1)).xy;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 imaginary;
			out vec4 julia;

			uniform float maxCount;
			uniform dvec4 c;
			uniform sampler2D levelcolors;

			//mult. of imaginary numbers
			vec2 imgMult(vec2 z1, vec2 z2) {
				return vec2( 
					z1.x * z2.x - z1.y * z2.y , 
					z1.x * z2.y + z1.y * z2.x
					);
			}

			//euclidean norm of imaginary number
			float imgNorm(vec2 z) {
				return sqrt(z.x*z.x + z.y*z.y);
			}

			void main() {
				float i=0;
				vec2 fc = vec2(c.xy);
				vec2 z = imaginary;
				//The fragment is assumed to belong
				//to the julia set as long as proven otherwise.
				//That's done by following the trajectory starting
				//at the current imagepoint.
				bool isJulia = true;
				for( ; i<maxCount ; i++) {
					float inorm = imgNorm(z);
					//Divergence detected, hence not a julia point!
					if(inorm > 2.0) {
						isJulia = false;
					}
					if(inorm > 12.0) {
						isJulia = false;
						break;
					}
					//Calculate the next position on the trajectory
					//by squaring the last point, and adding constant fc to it,
					//which is characteristic for this particular julia set.
					z = imgMult(z,z) + fc;
				}
				//Colorcode number of iterations, use norm of last trajectory point
				//to get a smooth colorcoding.
				float level = i + 1.0 - log2(log2(pow(imgNorm(z),2.0))*0.5);
				level = sqrt(level);
				julia = texture(levelcolors,vec2(level,0));
				//Julia points are colored black.
				if(isJulia) {
					julia = vec4(0,0,0,1);
				}
			}
			"
		}
	}

	(pass) {
		id = "julia.pass";
		shader = "julia.shader";
		backgroundQuad = "true";
		(floats) {
			maxCount = "maxcount.float";
		}
		(dvectors) {
			c = "julia.c";
		}
		(dmatrices) {
			transform = "julia.transform";
		}
		(textures) {
			levelcolors = "present.levelcolors.texture";
		}
		(output) {
			rendertarget = "present.offline.target";
			julia = "julia.texture";
		}
	}

	/**
	 * Here the actual calculations for the Julia fractal 
	 * are happening, 64 bits are used for the floating point computations.
	 * For each fragment iterations are done until
	 * it's proven that the fragment does not belong
	 * to the julia set (the trajectory leads outside
	 * a circle of radius two around the centerpoint).
	 * For non-julia fragments a smooth colorcoding is
	 * used to visualize the number of iterations.
	 */
	(shader) {
		id = "julia64.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out vec2 imag;

			void main() {
				gl_Position = vec4(vertices,1);
				imag = vertices.xy;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 imag;
			out vec4 julia;

			uniform float maxCount;
			uniform dmat4 transform;
			uniform dvec4 c;
			uniform sampler2D levelcolors;

			dvec2 imgMult(dvec2 z1, dvec2 z2) {
				return dvec2( 
					z1.x * z2.x - z1.y * z2.y , 
					z1.x * z2.y + z1.y * z2.x
					);
			}

			double imgNorm(dvec2 z) {
				return sqrt(z.x*z.x + z.y*z.y);
			}

			void main() {
				dvec2 imaginary = (transform * dvec4(imag,0,1)).xy;
				float i=0;
				dvec2 z = imaginary;
				//The fragment is assumed to belong
				//to the julia set as long as proven otherwise.
				//That's done by following the trajectory starting
				//at the current imagepoint.
				bool isJulia = true;
				for( ; i<maxCount ; i++) {
					double inorm = imgNorm(z);
					//Divergence detected, hence not a julia point!
					if(inorm > 2.0) {
						isJulia = false;
					}
					if(inorm > 12.0) {
						isJulia = false;
						break;
					}
					//Calculate the next position on the trajectory
					//by squaring the last point, and adding constant fc to it,
					//which is characteristic for this particular julia set.
					z = imgMult(z,z) + c.xy;
				}
				//Colorcode number of iterations, use norm of last trajectory point
				//to get a smooth colorcoding.
				float level = i + 1.0 - log2(log2(pow(float(imgNorm(z)),2.0))*0.5);
				level = sqrt(level);
				julia = texture(levelcolors,vec2(level,0));
				//Julia points are colored black.
				if(isJulia) {
					julia = vec4(0,0,0,1);
				}
			}
			"
		}
	}

	(pass) {
		id = "julia64.pass";
		shader = "julia64.shader";
		backgroundQuad = "true";
		(floats) {
			maxCount = "maxcount.float";
		}
		(dvectors) {
			c = "julia.c";
		}
		(dmatrices) {
			transform = "julia.transform";
		}
		(textures) {
			levelcolors = "present.levelcolors.texture";
		}
		(output) {
			rendertarget = "present.offline.target";
			julia = "julia.texture";
		}
	}

}