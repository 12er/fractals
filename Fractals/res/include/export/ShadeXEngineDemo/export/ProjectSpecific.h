#ifndef _EXPORT_PROJECTSPECIFIC_H_
#define _EXPORT_PROJECTSPECIFIC_H_

/**
 * project specific export settings
 * (c) 2012 by Tristan Bauer
 */

/**
 * This define entails that the current
 * project is shadeXEngineDemo
 */
#define _PROJECT_SHADEXENGINEDEMO_

#endif