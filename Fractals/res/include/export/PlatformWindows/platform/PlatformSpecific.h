#ifndef _EXPORT_PLATFORMSPECIFIC_H_
#define _EXPORT_PLATFORMSPECIFIC_H_

/**
 * project specific export settings
 * (c) 2012 by Tristan Bauer
 */

/**
 * This define entails that the current
 * platform is Windows
 */
#define _PLATFORM_WINDOWS_

#endif