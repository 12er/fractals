#ifndef _SX_SXCORE_H_
#define _SX_SXCORE_H_

/**
 * ShadeX Engine core functionality
 * (c) 2012 by Tristan Bauer
 */
#include <AL/al.h>
#include <AL/alc.h>
#include <export/Export.h>
#include <vector>
#include <sx/SXMath.h>
#include <sx/SXParser.h>
#include <map>
#include <boost/timer.hpp>
#include <boost/unordered_map.hpp>
using namespace std;
using namespace boost;

namespace sx {

	/**
	 * a ressource (e texture, shader)
	 */
	class SXResource {
	protected:

		/**
		 * identifyer
		 */
		string id;

	public:

		/**
		 * deconstructor
		 * , compiler requires this method to be
		 * implemented in order to be virtual
		 */
		EX virtual ~SXResource();

		/**
		 * returns the identifier of the resource
		 */
		EX const string &getID() const;

		/**
		 * true iff the resource is loaded
		 */
		virtual bool isLoaded() const = 0;

		/**
		 * Tries to load or reload the resource.
		 */
		virtual void load() = 0;

	};

	/**
	 * Object processed by the render
	 * method of class Effect
	 */
	class EffectObject {

	public:

		/**
		 * Called by the Effect class with some input
		 * parameters each time the render method of Effect
		 * is invoked. The method is processed by those
		 * Effects, to which this has been added with
		 * method getObjects.
		 */
		virtual void execute(vector<string> &input) = 0;

	};

	/**
	 * types of shaders
	 */
	enum ShaderType {
		VERTEX,
		TESSELLATION_CONTROL,
		TESSELLATION_EVALUATION,
		GEOMETRY,
		FRAGMENT
	};

	/**
	 * a shader program
	 */
	class Shader: public SXResource {
	private:

		/**
		 * true iff the program is loaded
		 */
		bool loaded;

		/**
		 * Used to distribute texture slots of a
		 * shader among multiple textures, which
		 * are rendered at the same time. The
		 * current value is used by the next
		 * texture slot request as a texture slot.
		 */
		unsigned int newTextureSlot;

		/**
		 * Minimum texture slot, which is not locked.
		 * Locked texture slots (those from zero to freeTextureSlot - 1)
		 * are not freed by the freeTextureSlots command.
		 */
		unsigned int freeTextureSlot;

		/**
		 * Every time texture slots are locked, the first
		 * free texture slot is pushed onto this stack.
		 * The next time a texture slot unlock operation is performed,
		 * exactly all slots including slot t on top of the stack, and
		 * all slots with a higher number are unlocked.
		 */
		vector<unsigned int> lockedSlotsStack;

		//resources not loaded yet

		/**
		 * code of the shaders which
		 * have not been loaded yet
		 */
		vector<string> newShaders;

		/**
		 * identifiers of the transform feedback buffers
		 * used after the load operation
		 */
		vector<string> newTransformFeedbackBuffers;

		/**
		 * true iff the next program loaded
		 * will contain a fragment shader
		 */
		bool newFragmentShade;

		/**
		 * true iff the next program loaded
		 * will contain a tessellation shader
		 */
		bool newTessellate;

		/**
		 * types of the shaders newShaders
		 */
		vector<ShaderType> newShaderTypes;

		/**
		 * specifies, if an entry of newShader
		 * is code or a path to a file
		 */
		vector<bool> newShaderIsFile;

		//loaded resources

		/**
		 * id of the shader program
		 */
		unsigned int program;

		/**
		 * shaders linked in shader program
		 */
		vector<unsigned int> shaders;

		/**
		 * Identifiers of the transform feedback buffers.
		 * This class is a transform feedback shader iff
		 * transformFeedbackBuffers has one or more entries.
		 */
		vector<string> transformFeedbackBuffers;

		/**
		 * true iff the program has been loaded with
		 * a fragment shader
		 */
		bool fragmentShade;

		/**
		 * true iff the program has been loaded with
		 * tessellation shaders
		 */
		bool tessellate;

		/**
		 * locations of uniform variables
		 */
		unordered_map<string,int> uniformLocations;

		/**
		 * locations of fragment output targets
		 */
		unordered_map<string,int> targetLocations;

		/**
		 * locations of the vertex attributes
		 */
		unordered_map<string,int> attribLocations;

		/**
		 * error messages generated turing the execution
		 * of load() are posted in errorLog
		 */
		vector<string> errorLog;

		/**
		 * copy constructor, not used
		 */
		Shader(const Shader &);

		/**
		 * assignment operator, not used
		 */
		Shader &operator = (const Shader &);

	public:

		/**
		 * default constructor
		 */
		EX Shader(const string &id);

		/**
		 * deconstructor
		 */
		EX ~Shader();

		/**
		 * Unloads the current program,
		 * and compiles and links all
		 * the shaders in newShaders
		 *
		 * @see SXResource::load()
		 */
		EX void load();

		/**
		 * @see SXResource::isLoaded()
		 */
		EX bool isLoaded() const;

		/**
		 * returns compilation/link errors generated
		 * turing the execution of load()
		 */
		EX vector<string> getErrorLog() const;

		/**
		 * adds new shadercode to newShaders, which will be compiled
		 * by load
		 */
		EX void addShader(const string &code, ShaderType type);

		/**
		 * adds path to new shadercode to newShaders, which will be
		 * loaded and compiled
		 */
		EX void addShaderFile(const string &path, ShaderType type);

		/**
		 * Adds the identifier of a transform feedback buffer.
		 * After executing load(), this shader is a transform feedback shader,
		 * which should only be used between the calls beginCapture and endCapture
		 * in the class BufferedMesh.
		 */
		EX void addTransformFeedbackBuffer(const string &identifier);

		/**
		 * Turns the shader into the current shader. If the shader
		 * isn't loaded, the method has no effect. Releases all
		 * unlocked texture slots.
		 */
		EX void use();

		/**
		 * returns true iff the program was successfully loaded with
		 * a tessellation shader
		 */
		EX bool isTessellating() const;

		/**
		 * returns true iff 
		 */
		EX bool isTransformFeedback() const;

		/**
		 * returns true iff the program has a fragment shader
		 */
		EX bool isFragmentShading() const;

		/**
		 * Returns the program ID, which is internally used
		 * to access the shader program. If the shader is
		 * not initialized, the return value is not specified.
		 */
		EX int getProgramID() const;

		/**
		 * returns the identifiers of the transform feedback buffers
		 */
		EX const vector<string> &getTransformFeedbackBuffers() const;

		/**
		 * Returns the location of the transform feedback buffer
		 * with ID identifyer. If no such transform feedback buffer
		 * exists, a negative number is returned.
		 */
		EX int getTransformFeedbackLocation(const string &identifyer);

		/**
		 * Returns the uniform location of an identifyer.
		 * If the shader does not have a uniform named identifyer,
		 * a negative number is returned.
		 */
		EX int getUniformLocation(const string &identifyer);

		/**
		 * Returns the location of an identifyer of a fragment
		 * output target. If the shader does not have a fragment output
		 * target named identifyer, a negative number is returned.
		 */
		EX int getTargetLocation(const string &identifyer);

		/**
		 * Returns the location of an identifyer of a vertex attribute.
		 * If the shader does not have an attribute named identifyer,
		 * a negative number is returned.
		 */
		EX int Shader::getAttribLocation(const std::string &identifyer);

		/**
		 * Releases all texture slots from being
		 * used by shaders, which are not locked.
		 */
		EX void freeTextureSlots();

		/**
		 * Returns the next free texture slot which
		 * can be used by a texture to be used with this
		 * shader.
		 */
		EX unsigned int createTextureSlot();

		/**
		 * Locks all the texture slots which 
		 * are used currently. (from zero to newTextureSlot - 1)
		 * The locked texture slots are not affected
		 * by an invocation of freeTextureSlots() or use()
		 */
		EX void lockTextureSlots();

		/**
		 * Undos the last lockTextureSlots operation, which
		 * has not been undone.
		 */
		EX void unlockTextureSlots();

	};

	/**
	 * A mesh. Every mesh is a set of vertex attributes, which are
	 * available at the same time during rendering.
	 */
	class Mesh: public SXResource {
	protected:

		/**
		 * the number of vertices per face of the next mesh
		 * after the load operation
		 */
		unsigned int newFaceSize;
	
		/**
		 * The number of vertices per face of the loaded mesh,
		 * can only have values one, two or three. If
		 * the load operation could not be executed successfully,
		 * the value is undefined.
		 */
		unsigned int faceSize;

		/**
		 * number of vertices, divisible by faceSize
		 */
		unsigned int vertexCount;

	public:

		/**
		 * deconstructor
		 */
		EX ~Mesh();

		/**
		 * setter
		 */
		EX void setFaceSize(unsigned int faceSize);

		/**
		 * getter
		 */
		EX unsigned int getFaceSize() const;

		/**
		 * getter
		 */
		EX unsigned int getVertexCount() const;

		/**
		 * renders all attribute buffers currently loaded
		 * simultaneously using the shader specified
		 */
		virtual void render(Shader &shader) = 0;
	};

	/**
	 * a VBO
	 */
	struct VertexBuffer {

		/**
		 * name of the vertex attribute
		 * used by shaders
		 */
		string ID;

		/**
		 * name of the vertex attribute
		 * used by shader vertex output
		 */
		string outputID;
		
		/**
		 * id in VRam of the vertex buffer object
		 */
		unsigned int bufferID;

		/**
		 * location of attribute buffer,
		 * must be unique in the set of all
		 * attribute buffers used simultaneously
		 */
		unsigned int location;

		/**
		 * number of components per vertexattribute
		 */
		unsigned int attributeSize;

	public:

		/**
		 * Returns a pointer to
		 * the vertex buffer. Writing and
		 * reading from the returned array is possible.
		 * After usage, lock() must be invoked.
		 */
		EX float *unlock();

		/**
		 * Returns a pointer to
		 * the vertex buffer. Only reading
		 * from the returned array is possible.
		 * After usage, lock() must be invoked.
		 */
		EX const float *unlockRead();
		
		/**
		 * Returns a pointer to
		 * the vertex buffer. Only writing
		 * data to the returned array is possible.
		 * After usage, lock() must be invoked.
		 */
		EX float *unlockWrite();

		/**
		 * The invocation of lock () invalidates
		 * the returned pointers from unlock(),
		 * unlockRead() and unlockWrite(), on
		 * the client side (CPU, but the dataset
		 * still exists on the GPU after the lock()
		 * operation). lock() must be called as soon
		 * as possible after the invocation of
		 * unlock(), unlockRead() or unlockWrite().
		 */
		EX void lock();

	};

	class Uniform;
	class UniformFloat;
	class UniformVector;
	class UniformMatrix;

	/**
	 * A Mesh stored in VRam
	 */
	class BufferedMesh: public Mesh, public EffectObject {
	private:

		/**
		 * true iff the last invocation
		 * of load could load all buffers
		 * into VRam successfully
		 */
		bool loaded;

		//resources not loaded yet

		/**
		 * array of new buffers
		 */
		vector<vector<float>> newBuffers;

		/**
		 * identifiers of the elements in newBuffers
		 */
		vector<string> newAttributeIDs;

		/**
		 * maps the identifier of a buffer to its output id
		 */
		map<string,string> newOutputIDs;

		/**
		 * attribute sizes of the new buffers
		 */
		vector<unsigned int> newAttributeSizes;

		/**
		 * File paths of the new buffers. Each
		 * element points to the filepath of a mesh.
		 * The attribute buffers of each mesh will be
		 * loaded.
		 */
		vector<string> newMeshes;

		/**
		 * new maximum number of vertices,
		 * which can be written to the mesh
		 */
		unsigned int newMaxVertexCount;

		/**
		 * Each element specifies, if the standard buffers
		 * of the corresponding mesh in newMeshes should be
		 * ignored or not. The standard attribute buffers are
		 * buffers with one of the names
		 * vertices, normals, texcoords, colors
		 */
		vector<bool> newDiscardStandard;

		//loaded resources

		/**
		 * Map of VBOs. The key is the identifyer of the VBO,
		 * and the value points to a VertexBuffer.
		 */
		map<string,VertexBuffer *> vbos;

		/**
		 * Map of VBOs. The key is the output identifyer of the VBO,
		 * and the value points to a VertexBuffer. The valueset of
		 * outputVBOs is equal to the valueset of vbos.
		 */
		map<string,VertexBuffer *> outputVBOs;

		/**
		 * All the vbos are part of a vertex array object.
		 * They can be invoced by binding the vertex array object.
		 */
		unsigned int vao;

		/**
		 *
		 */
		map<string,UniformFloat **> uniformFloatSets;
		map<string,UniformVector **> uniformVectorSets;
		map<string,UniformMatrix **> uniformMatrixSets;

		/**
		 * query counting the primitives written
		 * to the mesh
		 * in between the calls
		 * beginCapture and endCapture
		 */
		unsigned int query_primitives_written;

		/**
		 * query counting the primitives generated
		 * in between the calls
		 * beginCapture and endCapture
		 */
		unsigned int query_primitives_generated;

		/**
		 * maximum number of vertices, which
		 * can be written to the BufferedMesh
		 */
		unsigned int maxVertexCount;

		/**
		 * copy constructor, not used
		 */
		BufferedMesh(const BufferedMesh &);

		/**
		 * root bones of the meshe's skeleton
		 */
		vector<Bone> bones;

		/**
		 * transformation from the space containing
		 * the rootbones into the world space
		 */
		Matrix skeletonMatrix;

		/**
		 * assignment operator, not used
		 */
		BufferedMesh &operator = (const BufferedMesh &);

	public:

		/**
		 * constructor constructing a BufferedMesh with
		 * identifier id
		 */
		EX BufferedMesh(const string &id);

		/**
		 * deconstructor
		 */
		EX ~BufferedMesh();

		/**
		 * deconstructs the previously loaded attribute buffers,
		 * if something has been loaded,
		 * and leaves this unloaded
		 */
		EX void unload();

		/**
		 * Deconstructs the previously loaded attribute buffers,
		 * and loads the new attribute buffers. All files, which
		 * are used as a source of some buffers, must be parsed successfully,
		 * and each of the loaded buffers must have the same number of
		 * vertices. Otherwise the load method will leave
		 * this in an uninitialized state.
		 */
		EX void load();

		/**
		 * true iff the last invocation of load
		 * loaded all buffers
		 */
		EX bool isLoaded() const;

		/**
		 * Saves the mesh in a file. Only the first vertexCount vertices are saved.
		 * Files with extension .dae are assumed to be in Collada format.
		 * Files with extension .c are assumed to be in SX resource file format (see doc/SX reference cards/SX language/SX language.pdf ).
		 * An Exception is thrown, if the file can't be saved.
		 */
		EX void save(const string &filename);

		/**
		 * Starts recording the vertices generated
		 * by the vertex, tessellation and geometry
		 * shader in shader into the mesh, until maxVertexCount
		 * vertices are created. The mesh dataset is deleted
		 * before anything is written to the mesh.
		 */
		EX void beginCapture(Shader &shader);

		/**
		 * Stops recording vertices.
		 */
		EX void endCapture(Shader &shader);

		/**
		 * Returns the number of vertices created in
		 * between the call of beginCapture and endCapture.
		 * The number of created vertices doesn't equal
		 * the number of vertices written to the mesh,
		 * if more than maxVertexCount vertices are created.
		 */
		EX unsigned int countCreatedVertices() const;

		/**
		 * Sets the maximum number of vertices, which
		 * can be written to the mesh, of the next BufferedMesh
		 * loaded by load(). If buffers with
		 * vc vertices assigned to each buffer are added
		 * by calling addBuffer, and maxVertexCount is less
		 * than vc, vc is assigned to maxVertexCount.
		 */
		EX void setMaxVertexCount(unsigned int count);

		/**
		 * getter
		 */
		EX unsigned int getMaxVertexCount() const;

		/**
		 * Includes the first count vertices of the buffers
		 * into the mesh (, which from then on are considered for
		 * rendering and being saved through method save).
		 * vertexCount will be the biggest number smaller or equal
		 * to count, smaller or equal to maxVertexCount and
		 * a number divisible by faceSize. The method has immediate effect.
		 * If this is not loaded, the method has no effect.
		 */
		EX void setVertexCount(unsigned int count);

		/**
		 * renders the attribute buffers, if the last invocation of
		 * load loaded all buffers successfully
		 */
		EX void render(Shader &shader);

		/**
		 * Renders the attribute buffers, for each vertex in instanceBuffers. The command
		 * has only an effect, if the last invocation of load loaded all buffers successfully.
		 * Let n be the minimum vertex count out of all loaded meshes in instanceBuffers, then
		 * the buffers of this are rendered repeatedly for n times. For the k. time the buffers
		 * of this are rendered, the k. vertex of each buffer of each loaded mesh in instanceBuffers
		 * can be accessed in the Shaderprogram shader. Meshes in instanceBuffers, which
		 * haven't been loaded yet are not considered.
		 */
		EX void renderInstanced(Shader &shader, vector<BufferedMesh *> &instanceBuffers);

		/**
		 * Adds a new attribute buffer. The vertex attributes can be read by using
		 * variablename identifier in the shaders. For writing vertex attributes, the
		 * variablename outputIdentifier is used. Each attributeSize elements in buffer
		 * in sequence form a vertex attribute.
		 */
		EX void addBuffer(const string &identifier, const string &outputIdentifier, vector<float> &buffer, unsigned int attributeSize);

		/**
		 * Adds a new attribute buffer. The vertex attributes can be accessed by the
		 * variablename identifier in the shaders. Each attributeSize elements in buffer
		 * in sequence form a vertex attribute.
		 */
		EX void addBuffer(const string &identifier, vector<float> &buffer, unsigned int attributeSize);

		/**
		 * Adds a new file to be loaded. Files with extension ply in their filename
		 * are assumed to be in PLY (polygon file format of the stanford file format),
		 * and files with extension dae are assumed to be in Collada format.
		 *
		 * *.ply (polygon file format) files:
		 *		Properties x, y, z, w in element vertex are stored in a buffer with
		 *		identifier "vertices". The attributeSize is equal
		 *		to the number of properties called x, y, z, w declared
		 *		in element vertex.
		 *		Properties nx, ny, nz, nw in element vertex are stored in a buffer with
		 *		identifier "normals". The attributeSize is equal
		 *		to the number of properties called nx, ny, nz, nw declared
		 *		in element vertex.
		 *		Properties s, t, p, q in element vertex are stored in a buffer with
		 *		identifier "texcoords". The attributeSize is equal
		 *		to the number of properties called s, t, p, q declared
		 *		in element vertex.
		 *		Each other property with a name n in element vertex is
		 *		stored in a buffer called n with attributeSize 1.
		 * *.dae (Collada) files:
		 *		The mesh of the first geometry in library_geometries is
		 *		loaded into the buffers. Each float_array referenced
		 *		in polylist is loaded into a buffer having a name equal
		 *		to property semantic. Buffers with semantic "VERTEX", 
		 *		"NORMAL" or "TEXCOORDS" are treated differently:
		 *		The buffers with those semantics are called "vertices",
		 *		"normals" and "texcoords". If element input has semantic
		 *		"NORMAL", or property normalize="true", and references
		 *		a float array with dimension 3 or 4, the vectors in the
		 *		float array are normalized.
		 *		Let t1 be the transformationmatrix specified in the
		 *		node in visual_scene in library_visual_scenes, which
		 *		has the name of the geometry. And let t2 be the identity
		 *		matrix, or the bind_shape_matrix in the controller in
		 *		library_controllers referenced by the node, if a skeleton
		 *		is attached to the mesh.
		 *		Then transformation t2 * t1 is applied to each three or
		 *		fourdimensional buffer, which does not have semantic "NORMAL",
		 *		and does not have property normal="true" in element input.
		 *		The normalmatrix of t2 * t1 is applied to each three or
		 *		fourdimensional buffer, which has semantic "NORMAL", or
		 *		property normal="true" in element input.
		 *		The vertexwheights of the bones are loaded as buffers with
		 *		attributeSize 1, such that each buffer with vertexwheights
		 *		has the name of the associated bone.
		 */
		EX void addBuffers(const string &path);

		/**
		 * Adds a new file to be loaded without the standard buffers.
		 * A standard buffer is a buffer, which has one of the following identifyers:
		 * vertices, normals, texcoords, colors
		 */
		EX void addBuffers(const string &path, bool discardStandardBuffers);

		/**
		 * Specifies the outputIdentifier of the buffer identifier after the execution of load().
		 * If no such buffer has been added since the last load / unload call, this method
		 * has no effect.
		 */
		EX void setOutputIdentifier(const string &identifier, const string &outputIdentifier);

		/**
		 * Returns the VBO having identifier identifier. If no such
		 * VBO exists, 0 is returned.
		 */
		EX VertexBuffer *getBuffer(const string &identifier);

		/**
		 * returns the set of all VBOs having their identifiers as
		 * keys
		 */
		EX map<string,VertexBuffer *> &getBuffers();

		/**
		 * Assigns the Uniforms in uniforms to the location. vertex of each VertexBuffer with an ID in bufferIDs.
		 * Calling loadBuffersToUniforms() will update the uniforms with the values of the location. vertex,
		 * and calling loadUniformsToBuffers() will update the location. vertex with the values of the uniforms.
		 * Let A be the k. element of uniforms, and B be the VertexBuffer with the id equal to the k. element of bufferIDs,
		 * then the operations loadBuffersToUniforms() and loadUniformsToBuffers() will exchange data between A and B.
		 * If a set of uniforms was already assigned to location, the assignment is undone and replaced by the uniform set
		 * in the parameters.
		 * The method has no effect and returns false iff this is not loaded, or one of the uniforms is a Texture or
		 * a Volume, or location is greater or equal to maxVertexCount, or uniforms and bufferIDs have different sizes,
		 * or bufferIDs contains a string, which is not equal to any id of a VertexBuffer, or the dimension of a uniform
		 * assigned to a VertexBuffer is not equal to the attributeSize of the VertexBuffer. Furthermore the method is
		 * only guaranteed to work if the ids in bufferIDs are mutually different, and the objects in uniforms are mutually
		 * different.
		 */
		EX bool setUniforms(const vector<Uniform *> &uniforms, const vector<string> &bufferIDs, unsigned int location);

		/**
		 * Undoes the association of the uniforms with
		 * the k. vertex created by setUniforms.
		 * The method has no effect and
		 * returns false iff this is not loaded, or no
		 * such assignment exists.
		 */
		EX bool removeUniforms(unsigned int location);

		/**
		 * undoes all associations created by setUniforms
		 */
		EX void removeUniforms();

		/**
		 * undoes all associations created by setUniforms,
		 * and releases the memory allocated for holding
		 * this associations
		 */
		EX void resetUniforms();

		/**
		 * Loads the content of the VertexBuffers
		 * to the Uniforms associated with the VertexBuffers
		 * by calling setUniforms. Let A be a uniform associated
		 * to vertex B in a certain buffer, then loadBuffersToUniforms
		 * assigns the value of B to A.
		 */
		EX void loadBuffersToUniforms();

		/**
		 * Loads the content of the VertexBuffers
		 * to the Uniforms associated with the VertexBuffers
		 * by calling setUniforms with a location greater or equal to firstLocation,
		 * and smaller or equal to lastLocation. Let A be a uniform associated
		 * to vertex B in a certain buffer, then loadBuffersToUniforms
		 * assigns the value of B to A.
		 */
		EX void loadBuffersToUniforms(unsigned int firstLocation, unsigned int lastLocation);

		/**
		 * Loads the content of the VertexBuffer with id bufferID
		 * to the Uniforms associated with the VertexBuffer
		 * by calling setUniforms. Let A be a uniform associated
		 * to vertex B in VertexBuffer with id bufferID, then loadBuffersToUniforms
		 * assigns the value of B to A.
		 */
		EX void loadBufferToUniforms(const string &bufferID);

		/**
		 * Loads the content of the VertexBuffer with id bufferID
		 * to the Uniforms associated with the VertexBuffer
		 * by calling setUniforms with a location greater or equal to firstLocation,
		 * and smaller or equal to lastLocation. Let A be a uniform associated
		 * to vertex B in VertexBuffer with id bufferID, then loadBuffersToUniforms
		 * assigns the value of B to A.
		 */
		EX void loadBufferToUniforms(const string &bufferID, unsigned int firstLocation, unsigned int lastLocation);

		/**
		 * Loads the content of the Uniforms associated with the
		 * VertexBuffers by calling setUniforms to the VertexBuffers.
		 * Let A be a uniform associated to vertex B in a certain buffer,
		 * then loadBuffersToUniforms the value of A to B.
		 */
		EX void loadUniformsToBuffers();

		/**
		 * Loads the content of the Uniforms associated with the
		 * VertexBuffers by calling setUniforms to the VertexBuffers with a
		 * location greater or equal to firstLocation, and smaller or equal than lastLocation.
		 * Let A be a uniform associated to vertex B in a certain buffer,
		 * then loadBuffersToUniforms the value of A to B.
		 */
		EX void loadUniformsToBuffers(unsigned int firstLocation, unsigned int lastLocation);

		/**
		 * Loads the content of the Uniforms associated with the
		 * VertexBuffer with id bufferID by calling setUniforms to the VertexBuffer.
		 * Let A be a uniform associated to vertex B in the VertexBuffer with id bufferID,
		 * then loadBuffersToUniforms the value of A to B.
		 */
		EX void loadUniformsToBuffer(const string &bufferID);

		/**
		 * Loads the content of the Uniforms associated with the
		 * VertexBuffer with id bufferID by calling setUniforms to the VertexBuffer with a
		 * location greater or equal to firstLocation, and smaller or equal than lastLocation.
		 * Let A be a uniform associated to vertex B in the VertexBuffer with id bufferID,
		 * then loadBuffersToUniforms the value of A to B.
		 */
		EX void loadUniformsToBuffer(const string &bufferID, unsigned int firstLocation, unsigned int lastLocation);

		/**
		 * returns the root bones of the mesh's skeleton
		 */
		EX vector<Bone> getSkeleton() const;

		/**
		 * returns the transformation from the space
		 * containing the root bones into the model space
		 */
		EX Matrix getSkeletonMatrix() const;

		/**
		 * Setter. Has no effect, if the boneIDs
		 * are not unique.
		 */
		EX void setSkeleton(const vector<Bone> &bones);

		/**
		 * setter
		 */
		EX void setSkeletonMatrix(const Matrix &skeletonMatrix);

		/**
		 * Strings called "readUniforms" and "writeUniforms" in input are considered
		 * in order:
		 * for each "readUniforms" in input method loadUniformsToBuffers() is called
		 * for each "writeUniforms" in input method loadBuffersToUniforms() is callsed
		 */
		EX void execute(vector<string> &input);

	};

	/**
	 * a uniform variable
	 */
	class Uniform: public SXResource {
	protected:

		/**
		 * last token of id with '.' being the the
		 * delimiter of the tokens in id
		 */
		string idToken;

		/**
		 * mapping of the identifiers associating them
		 * with the names used by a certain resource
		 */
		unordered_map<string,string> uniformNames;

		/**
		 * initializing this with identifier
		 * id, and uniformNames
		 */
		EX Uniform(const string &id);

	public:

		/**
		 * deconstructor
		 */
		EX virtual ~Uniform();

		/**
		 * sets the name of the uniform variable used in the
		 * shader, when the resource with identifyer id uses
		 * the uniform variable
		 */
		EX void setUniformName(const string &name, const string &id);

		/**
		 * Returns the uniform name of the variable associated with id.
		 * If this association has not be specified yet with setUniformName,
		 * idToken is returned
		 */
		EX const string &getUniformName(const string &id) const;

		/**
		 * Submits the uniform variable to the specified shader.
		 * The uniform variable is accessible in the shader with
		 * the name set with setUniformName. If setUniformName
		 * has not been used, idToken is used as the uniform variable name.
		 */
		virtual void use(Shader &shader, const string &id) = 0;

	};

	class UniformFloat: public Uniform {
	private:

		/**
		 * copy constructor, not used
		 */
		UniformFloat(const UniformFloat &);

		/**
		 * assignment operator, not used
		 */
		UniformFloat &operator = (const UniformFloat &);

	public:

		/**
		 * value of the uniform float
		 */
		float value;

		/**
		 * constructor initializing this with
		 * identifier id
		 */
		EX UniformFloat(const string &id);

		/**
		 * copies float v, and returns a reference to this
		 */
		EX UniformFloat &operator = (float v);

		/**
		 * copies float v, and returns a reference to this
		 */
		EX UniformFloat &operator << (float v);

		/**
		 * copies itself to float v, returns a reference to this
		 */
		EX const UniformFloat &operator >> (float &v) const;

		/**
		 * as this is always loaded,
		 * this method has no effect on this
		 */
		EX void load();

		/**
		 * as this is always loaded, the return
		 * value is always true
		 */
		EX bool isLoaded() const;

		/**
		 * @see Uniform::use(Shader &shader, const string &id)
		 */
		EX void use(Shader &shader, const string &id);

	};

	class UniformDouble: public Uniform {
	private:

		/**
		 * copy constructor, not used
		 */
		UniformDouble(const UniformDouble &);

		/**
		 * assignment operator, not used
		 */
		UniformDouble &operator = (const UniformDouble &);

	public:

		/**
		 * value of the uniform float
		 */
		double value;

		/**
		 * constructor initializing this with
		 * identifier id
		 */
		EX UniformDouble(const string &id);

		/**
		 * copies double v, and returns a reference to this
		 */
		EX UniformDouble &operator = (double v);

		/**
		 * copies float v, and returns a reference to this
		 */
		EX UniformDouble &operator << (double v);

		/**
		 * copies itself to float v, returns a reference to this
		 */
		EX const UniformDouble &operator >> (double &v) const;

		/**
		 * as this is always loaded,
		 * this method has no effect on this
		 */
		EX void load();

		/**
		 * as this is always loaded, the return
		 * value is always true
		 */
		EX bool isLoaded() const;

		/**
		 * @see Uniform::use(Shader &shader, const string &id)
		 */
		EX void use(Shader &shader, const string &id);

	};

	class UniformVector: public Uniform, public Vector {
	private:

		/**
		 * copy constructor, not used
		 */
		UniformVector(const UniformVector &);

		/**
		 * assignment operator, only copies content of vector
		 */
		UniformVector &operator = (const UniformVector &);

	public:

		/**
		 * constructor initializing this with
		 * identifier id
		 */
		EX UniformVector(const string &id);

		/**
		 * assignment operator
		 */
		EX Vector &operator = (const Vector &v);

		/**
		 * as this is always loaded,
		 * this method has no effect on this
		 */
		EX void load();

		/**
		 * as this is always loaded, the return
		 * value is always true
		 */
		EX bool isLoaded() const;

		/**
		 * @see Uniform::use(Shader &shader, const string &id)
		 */
		EX void use(Shader &shader, const string &id);

	};

	class UniformDVector: public Uniform, public DVector {
	private:

		/**
		 * copy constructor, not used
		 */
		UniformDVector(const UniformDVector &);

		/**
		 * assignment operator, only copies content of vector
		 */
		UniformDVector &operator = (const UniformDVector &);

	public:

		/**
		 * constructor initializing this with
		 * identifier id
		 */
		EX UniformDVector(const string &id);

		/**
		 * assignment operator
		 */
		EX DVector &operator = (const DVector &v);

		/**
		 * as this is always loaded,
		 * this method has no effect on this
		 */
		EX void load();

		/**
		 * as this is always loaded, the return
		 * value is always true
		 */
		EX bool isLoaded() const;

		/**
		 * @see Uniform::use(Shader &shader, const string &id)
		 */
		EX void use(Shader &shader, const string &id);

	};

	class UniformMatrix: public Uniform, public Matrix {
	private:

		/**
		 * copy constructor, not used
		 */
		UniformMatrix(const UniformMatrix &);

		/**
		 * assignment operator, not used
		 */
		UniformMatrix &operator = (const UniformMatrix &);

	public:

		/**
		 * constructor initializing this with
		 * identifier id
		 */
		EX UniformMatrix(const string &id);

		/**
		 * assignment operator
		 */
		EX Matrix &operator = (const Matrix &m);

		/**
		 * as this is always loaded,
		 * this method has no effect on this
		 */
		EX void load();

		/**
		 * as this is always loaded, the return
		 * value is always true
		 */
		EX bool isLoaded() const;

		/**
		 * @see Uniform::use(Shader &shader, const string &id)
		 */
		EX void use(Shader &shader, const string &id);

	};

	class UniformDMatrix: public Uniform, public DMatrix {
	private:

		/**
		 * copy constructor, not used
		 */
		UniformDMatrix(const UniformDMatrix &);

		/**
		 * assignment operator, not used
		 */
		UniformDMatrix &operator = (const UniformDMatrix &);

	public:

		/**
		 * constructor initializing this with
		 * identifier id
		 */
		EX UniformDMatrix(const string &id);

		/**
		 * assignment operator
		 */
		EX DMatrix &operator = (const DMatrix &m);

		/**
		 * as this is always loaded,
		 * this method has no effect on this
		 */
		EX void load();

		/**
		 * as this is always loaded, the return
		 * value is always true
		 */
		EX bool isLoaded() const;

		/**
		 * @see Uniform::use(Shader &shader, const string &id)
		 */
		EX void use(Shader &shader, const string &id);

	};

	/**
	 * format of a pixel of a texture
	 */
	enum PixelFormat {
		BYTE_RGBA,
		FLOAT16_RGBA,
		FLOAT_RGBA
	};

	class Texture: public Uniform {
	private:

		/**
		 * true if the texture could be loaded
		 * successfully
		 */
		bool loaded;

		//resources not loaded yet

		/**
		 * Path to the new texture to be
		 * loaded. If newPath is assigned
		 * with a string different from the
		 * empty string, load() tries to
		 * load an image from the file,
		 * otherwise only the other new resources
		 * are taken into consideration.
		 */
		string newPath;

		/**
		 * width of the new image, ignored
		 * if newPath is nonempty
		 */
		int newWidth;

		/**
		 * height of the new image, ignored
		 * if newPath is nonempty
		 */
		int newHeight;

		/**
		 * format of the pixels, ignored
		 * if newPath is nonempty
		 */
		PixelFormat newPixelFormat;

		/**
		 * Dataset of the new texture. Only taken
		 * into consideration if newPath is nonempty
		 * and newPixelFormat equals BYTE_RGBA. If
		 * newByteBuffer's size does not have
		 * newHeight * newWidth * 4 elements,
		 * the texture is loaded without this buffer.
		 */
		vector<unsigned char> newByteBuffer;

		/**
		 * Dataset of the new texture. Only taken
		 * into consideration if newPath is nonempty
		 * and newPixelFormat equals FLOAT_RGBA. If
		 * newByteBuffer's size does not have
		 * newHeight * newWidth * 4 elements,
		 * the texture is loaded without this buffer.
		 */
		vector<float> newFloatBuffer;

		//loaded resources
		
		/**
		 * id of the texture in VRam
		 */
		unsigned int texture;

		/**
		 * width of the loaded texture
		 */
		int width;

		/**
		 * height of the loaded texture
		 */
		int height;

		/**
		 * format of the pixels of the texture
		 */
		PixelFormat pixelFormat;

		/**
		 * copy constructor, not used
		 */
		Texture(const Texture &);

		/**
		 * assignment operator, not used
		 */
		Texture &operator = (const Texture &);

	public:

		/**
		 * constructor initializing this with
		 * identifier id
		 */
		EX Texture(const string &id);

		/**
		 * deconstructor
		 */
		EX ~Texture();

		/**
		 * if a texture has been loaded, it's
		 * unloaded by this operation, leaving
		 * this unloaded
		 */
		EX void unload();

		/**
		 * Deletes the last texture, and loads
		 * a new texture from the specified path,
		 * or from a buffer with the specified width,
		 * height and pixelFormat, or with specified width,
		 * height and pixelFormat, but without a buffer.
		 * If nothing has been specified, the method
		 * has no effect.
		 */
		EX void load();

		/**
		 * returns true if a texture is loaded
		 */
		EX bool isLoaded() const;

		/**
		 * Saves the texture in a file.
		 * Currently bmp, jpg and png files are supported.
		 *
		 * If the texture is saved in a bmp or jpg file, the
		 * saved image will have 24 bits per pixel regardless of the
		 * pixelFormat of the texture.
		 * If the texture is saved in a png file, and the pixelFormat
		 * is equal to BYTE_RGBA, the saved image will have 32 bits per pixel,
		 * and if the pixelFormat is equal to FLOAT16_RGBA or FLOAT_RGBA,
		 * the saved image will have 64 bits per pixel.
		 *
		 * An Exception is thrown, if the texture can't be saved.
		 */
		EX void save(const string &filename);

		/**
		 * Binds the current texture to the next free
		 * texture slot of the shader. If the texture is
		 * not loaded, the method has no effect.
		 *
		 * @see Uniform::use(Shader &shader, const string &id)
		 */
		EX void use(Shader &shader, const string &id);

		/**
		 * Returns the texture ID, which is internally used
		 * to access the shader program. If the shader is
		 * not initialized, the return value is not specified.
		 */
		EX unsigned int getTextureID() const;

		/**
		 * Sets a path, from which the texture will be
		 * loaded by load(). If path is set, the setters
		 * for width, height, pixelFormat, byteBuffer and floatBuffer
		 * are ignored.
		 *
		 * Currently bmp, jpg and png files are supported.
		 * If path points to file with 24 or 32 bits per pixel,
		 * method load() will initialize a BYTE_RGBA texture.
		 * If path points to a png file with 64 bits per pixel,
		 * method load() will initialize a FLOAT16_RGBA texture.
		 */
		EX void setPath(const string &path);

		/**
		 * Specifies the width of the next texture loaded by load().
		 * If a path was set, this parameter is ignored.
		 */
		EX void setWidth(int width);

		/**
		 * getter of the loaded texture, if no
		 * texture is loaded, the return value isn't specified
		 */
		EX int getWidth() const;

		/**
		 * Specifies the height of the next texture loaded by load().
		 * If a path was set, this parameter is ignored.
		 */
		EX void setHeight(int height);

		/**
		 * getter of the loaded texture, if no
		 * texture is loaded, the return value isn't specified
		 */
		EX int getHeight() const;

		/**
		 * Specifies the pixel format of the next texture loaded by load().
		 * If a path was set, this parameter is ignored.
		 */
		EX void setPixelFormat(PixelFormat pixelFormat);

		/**
		 * getter of the loaded texture, if no
		 * texture is loaded, the return value isn't specified
		 */
		EX PixelFormat getPixelFormat() const;

		/**
		 * Specifies what the texture will look like after the load() operation.
		 * Changes the pixelFormat of the next texture to BYTE_RGBA.
		 * If a path was set, or the buffer doesn't have size width * height * 4,
		 * the buffer is ignored.
		 */
		EX void setByteBuffer(const vector<unsigned char> &buffer);

		/**
		 * Specifies what the texture will look like after the load() operation.
		 * Changes the pixelFormat of the next texture to FLOAT_RGBA.
		 * If a path was set, or the buffer doesn't have size width * height * 4,
		 * the buffer is ignored.
		 */
		EX void setFloatBuffer(const vector<float> &buffer);

	};

	/**
	 * A 3D texture. Additionally to dimensions width and height it
	 * also has dimension depth.
	 */
	class Volume: public Uniform {
	private:

		/**
		 * true if the texture could be loaded
		 * successfully
		 */
		bool loaded;

		//resources not loaded yet

		/**
		 * Paths to the volume's layers to be
		 * loaded. The paths specify the layers in ascending order.
		 * width and height of the volume are equal to width and height
		 * of the first image, the other images are scaled to those dimensions.
		 * If newPathy is assigned
		 * with a string different from the
		 * empty string, load() tries to
		 * load an image from the file,
		 * otherwise only the other new resources
		 * are taken into consideration.
		 * The images in the files specified by newPaths
		 * must be valid images.
		 */
		vector<string> newPaths;

		/**
		 * width of the new texture, ignored
		 * if newPath is nonempty
		 */
		int newWidth;

		/**
		 * height of the new texture, ignored
		 * if newPath is nonempty
		 */
		int newHeight;

		/**
		 * depth of the new texture, ignored
		 * if newPath is nonempty
		 */
		int newDepth;

		/**
		 * format of the pixels, ignored
		 * if newPath is nonempty
		 */
		PixelFormat newPixelFormat;

		/**
		 * Dataset of the new texture. Only taken
		 * into consideration if newPath is nonempty
		 * and newPixelFormat equals BYTE_RGBA. If
		 * newByteBuffer's size does not have
		 * newHeight * newWidth * newDepth * 4 elements,
		 * the texture is loaded without this buffer.
		 */
		vector<unsigned char> newByteBuffer;

		/**
		 * Dataset of the new texture. Only taken
		 * into consideration if newPath is nonempty
		 * and newPixelFormat equals FLOAT_RGBA. If
		 * newByteBuffer's size does not have
		 * newHeight * newWidth * newDepth * 4 elements,
		 * the texture is loaded without this buffer.
		 */
		vector<float> newFloatBuffer;

		//loaded resources
		
		/**
		 * id of the texture in VRam
		 */
		unsigned int texture;

		/**
		 * width of the loaded texture
		 */
		int width;

		/**
		 * height of the loaded texture
		 */
		int height;

		/**
		 * depth of the loaded texture,
		 * equals the number of layers of
		 * images with dimensions width x height
		 * in the texture
		 */
		int depth;

		/**
		 * format of the pixels of the texture
		 */
		PixelFormat pixelFormat;

		/**
		 * copy constructor, not used
		 */
		Volume(const Volume &);

		/**
		 * assignment operator, not used
		 */
		Volume &operator = (const Volume &);

	public:

		/**
		 * constructor initializing this with
		 * identifier id
		 */
		EX Volume(const string &id);

		/**
		 * deconstructor
		 */
		EX ~Volume();

		/**
		 * if a texture has been loaded, it's
		 * unloaded by this operation, leaving
		 * this unloaded
		 */
		EX void unload();

		/**
		 * Deletes the last texture, and loads
		 * new layers from the specified paths,
		 * or from a buffer with the specified width,
		 * height, depth and pixelFormat, or with specified width,
		 * height, depth and pixelFormat, but without a buffer.
		 * If the layers are loaded from the added paths,
		 * width and height of the volume are equal to width and height
		 * of the first image, and the other images are scaled to those dimensions.
		 * If nothing has been specified, the method
		 * has no effect.
		 */
		EX void load();

		/**
		 * returns true if a texture is loaded
		 */
		EX bool isLoaded() const;

		/**
		 * Saves the texture in a set of files. Each layer
		 * of the volume is stored in a separate file. The
		 * name of each file representing the i. layer is
		 * generated by appending number i to the filename
		 * in filenamepart.
		 * Currently bmp, jpg and png files are supported.
		 *
		 * If the volume is saved in a bmp or jpg file, the
		 * saved images will have 24 bits per pixel regardless of the
		 * pixelFormat of the texture.
		 * If the texture is saved in png files, and the pixelFormat
		 * is equal to BYTE_RGBA, the saved images will have 32 bits per pixel,
		 * and if the pixelFormat is equal to FLOAT16_RGBA or FLOAT_RGBA,
		 * the saved images will have 64 bits per pixel.
		 *
		 * An Exception is thrown, if the volume can't be saved.
		 */
		EX void save(const string &filenamepart);

		/**
		 * Binds the current texture to the next free
		 * texture slot of the shader. If the texture is
		 * not loaded, the method has no effect.
		 *
		 * @see Uniform::use(Shader &shader, const string &id)
		 */
		EX void use(Shader &shader, const string &id);

		/**
		 * Returns the texture ID, which is internally used
		 * to access the shader program. If the shader is
		 * not initialized, the return value is not specified.
		 */
		EX unsigned int getTextureID() const;

		/**
		 * Adds a path, from which the n. layer will be
		 * loaded by load(), if n-1 layer's paths have been added already.
		 * If path is set, the setters
		 * for width, height, pixelFormat, byteBuffer and floatBuffer
		 * are ignored. width and height of the volume are equal to width and height
		 * of the first image, the other images are scaled to those dimensions.
		 *
		 * Currently bmp, jpg and png files are supported.
		 * If path points to file with 24 or 32 bits per pixel,
		 * method load() will initialize a BYTE_RGBA volume.
		 * If path points to a png file with 64 bits per pixel,
		 * method load() will initialize a FLOAT16_RGBA volume.
		 */
		EX void addPath(const string &path);

		/**
		 * Sets the paths, from which the layers will be
		 * loaded by load(). The path's specify the layers in ascending order.
		 * If path is set, the setters
		 * for width, height, pixelFormat, byteBuffer and floatBuffer
		 * are ignored. width and height of the volume are equal to width and height
		 * of the first image, the other images are scaled to those dimensions.
		 */
		EX void setPaths(const vector<string> &paths);

		/**
		 * Specifies the width of the next texture loaded by load().
		 * If a path was set, this parameter is ignored.
		 */
		EX void setWidth(int width);

		/**
		 * getter of the loaded texture, if no
		 * texture is loaded, the return value isn't specified
		 */
		EX int getWidth() const;

		/**
		 * Specifies the height of the next texture loaded by load().
		 * If a path was set, this parameter is ignored.
		 */
		EX void setHeight(int height);

		/**
		 * getter of the loaded texture, if no
		 * texture is loaded, the return value isn't specified
		 */
		EX int getHeight() const;

		/**
		 * Specifies the depth of the next texture loaded by load().
		 * If a path was set, this parameter is ignored.
		 */
		EX void setDepth(int depth);

		/**
		 * getter of the loaded texture, if no
		 * texture is loaded, the return value isn't specified
		 */
		EX int getDepth();

		/**
		 * Specifies the pixel format of the next texture loaded by load().
		 * If a path was set, this parameter is ignored.
		 */
		EX void setPixelFormat(PixelFormat pixelFormat);

		/**
		 * getter of the loaded texture, if no
		 * texture is loaded, the return value isn't specified
		 */
		EX PixelFormat getPixelFormat() const;

		/**
		 * Specifies what the texture will look like after the load() operation.
		 * Changes the pixelFormat of the next texture to BYTE_RGBA.
		 * If a path was set, or the buffer doesn't have size width * height * depth * 4,
		 * the buffer is ignored.
		 */
		EX void setByteBuffer(const vector<unsigned char> &buffer);

		/**
		 * Specifies what the texture will look like after the load() operation.
		 * Changes the pixelFormat of the next texture to FLOAT_RGBA.
		 * If a path was set, or the buffer doesn't have size width * height * depth * 4,
		 * the buffer is ignored.
		 */
		EX void setFloatBuffer(const vector<float> &buffer);

	};

	/**
	 * Description of a bone's movement with id boneID.
	 * Matrix input describes a transformation of
	 * the bone in its joint space. Matrix output
	 * is the transformation of the vertices bound to
	 * the bone from model space coordinates in bind pose
	 * into model space coordinates after the movement of
	 * the bone and its parent bones.
	 * class Skeleton takes the state of the input matrices
	 * into consideration to calculate the output matrices.
	 */
	struct BoneTransform {
		
		/**
		 * identifier of the bone
		 */
		string boneID;

		/**
		 * Describes the movement of the bone
		 * as a tranformation in joint space.
		 * input may be 0. In this case,
		 * input is assumed to be the identity matrix.
		 */
		Matrix *input;

		/**
		 * Describes the movement of the vertices
		 * bound to this bone from the bind pose
		 * into the pose after the movement
		 * in model space coordinates.
		 * output may be 0. In this case,
		 * output is simply ignored by a Skeleton
		 * calculating the output matrix.
		 */
		Matrix *output;

	public:

		/**
		 * initializes input and output
		 * with NULL pointers
		 */
		EX BoneTransform();

	};

	/**
	 * Collection of bone hierachies with sets of BoneTransforms
	 * attached to it. Each set of BoneTransforms has a unique id,
	 * and repesents an instance of the skeleton in movement.
	 * Method updateBoneTransforms uses the input matrices of
	 * the BoneTransforms to move the bones, and copies the matrix
	 * describing the movement of the vertices associated to the bone of 
	 * a BoneTransform in model space coordinates into the output matrix
	 * of the BoneTransform.
	 */
	class Skeleton: public SXResource, public EffectObject {
	private:

		/**
		 * true if the skeleton could be loaded
		 * successfully
		 */
		bool loaded;

		//resources not loaded yet

		/**
		 * collection of paths pointing
		 * at files containing skeletons,
		 * which will be merged with this skeleton
		 */
		vector<string> newPaths;

		
		/**
		 * collection of meshes containting
		 * skeletons, which will be merged with
		 * this skeleton
		 */
		vector<BufferedMesh *> newMeshes;

		/**
		 * collection of bone hierachies, which
		 * will be merged with this skeleton
		 */
		vector<Bone> newBones;

		//loaded resources

		/**
		 * transformation from the space containing
		 * the rootbones into the world space
		 */
		Matrix skeletonMatrix;

		/**
		 * Root bones of the skeleton. Caution:
		 * changes of this vector invalidate the boneMap!
		 */
		vector<Bone> bones;

		/**
		 * max depth of the bone trees
		 */
		unsigned int skeletonDepth;

		/**
		 * map containing all bones indexed
		 * by their IDs.
		 * Caution: changes of vector bones
		 * invalidate the boneMap!
		 */
		map<string, Bone *> boneMap;

		/**
		 * Mapping of the bone's IDs to their position
		 * in an inorder traversal. The first bone
		 * in the inorder traversal has position 0.
		 */
		map<string,unsigned int> boneTransformsIndices;

		/**
		 * Map containing pointers to the BoneTransforms
		 * for optimized traversal. The identifier of the
		 * set is mapped to the vector of BoneTransforms
		 * of the set. The k. BoneTransform in a vector
		 * has the identifier of the k. Bone in an inorder traversal.
		 * If there is no BoneTransform associated with
		 * the k. bone in an inorder traversal, the NULL
		 * pointer can be found at the k. position of the vector.
		 * BoneTransforms, which are not associated with
		 * any Bone are not part of the vector.
		 */
		map<int,vector<BoneTransform *> > boneTransforms;

		/**
		 * Map containing sets of BoneTransforms. The key
		 * attribute of the map is the identifier of the set.
		 * The key attribute of a map of a set is the identifier
		 * of the BoneTransform.
		 * BoneTransforms with identifiers, which do not have
		 * a bone in the skeleton associated with it won't be
		 * influenced by method updateBoneTransforms.
		 */
		map<int,map<string,BoneTransform *> > boneTransformMap;

		/**
		 * List of all bonenames in BoneTransforms, which are
		 * not contained in the skeleton. If the skeleton
		 * is not loaded, it's empty.
		 */
		vector<string> unmatchedBones;

		/**
		 * Fills the bonemap with pointers to 
		 * the bones. The calculated boneMap
		 * might become incorrect if vector bones is changed.
		 */
		void initBoneMap();

		/**
		 * Calculates the inverseBindPoseMatrices of
		 * the bones, and initializes map boneTransforms
		 * such that the BoneTransforms of each set in boneTransforms
		 * have the order of the bones in an inorder traversal.
		 */
		void initBoneTransorms();

		/**
		 * Initializes map boneTransforms of a set of BoneTransforms
		 * described by Iterator setIter
		 * such that the BoneTransforms of the set in map boneTransforms
		 * have the order of the bones in an inorder traversal.
		 */
		void initBoneTransformSet(map<int,map<string,BoneTransform *> >::iterator setIter);

		/**
		 * copy constructor, not used
		 */
		Skeleton(const Skeleton &);

		/**
		 * assignment operator, not used
		 */
		Skeleton &operator = (const Skeleton &);

	public:

		/**
		 * constructor
		 */
		EX Skeleton(const string &id);

		/**
		 * destructor
		 */
		EX ~Skeleton();

		/**
		 * Adds the path of a file containing
		 * a skeleton, which will be loaded by
		 * method load. Skeletons in Collada format
		 * are supported currently.
		 */
		EX void addBones(const string &path);

		/**
		 * Adds a mesh, from which bones will be loaded by method load.
		 * The load operation of Skeleton calls the load
		 * operation of the mesh, if the mesh is not loaded yet.
		 */
		EX void addBones(BufferedMesh &mesh);

		/**
		 * setter
		 */
		EX void setSkeletonMatrix(const Matrix &transform);

		/**
		 * getter
		 */
		EX const Matrix &getSkeletonMatrix() const;

		/**
		 * Adds a root bone, which will be loaded
		 * by method load.
		 */
		EX void addRootBone(Bone &bone);

		/**
		 * Returns a loaded bone with identifier id.
		 * If no such bone exists, an exception is thrown.
		 */
		EX const Bone &getBone(const string &id) const;

		/**
		 * returns the loaded root bones
		 */
		EX vector<const Bone> getRootBones() const;

		/**
		 * Deletes the previously loaded bones, and
		 * loads bones added with the addBone and addRootBone
		 * methods. Each bone is required to have a unique
		 * identifier. If that uniquenes constraint does not
		 * hold, or the bones can't be loaded from the
		 * added media, the method deletes all bones,
		 * and leaves this in an uninitialized state.
		 */
		EX void load();

		/**
		 * deletes all loaded bones, if something
		 * has been loaded, and leaves this unloaded
		 */
		EX void unload();

		/**
		 * true iff the last invocation of load
		 * loaded all the bones
		 */
		EX bool isLoaded() const;

		/**
		 * The purpose of this method is to check for
		 * boneIDs of BoneTransforms, which don't match
		 * to any bones.
		 * If this is loaded, the method returns a list
		 * of all BoneTransforms' boneIDs, which aren't equal
		 * to any bone's ID.
		 * Otherwise, if this is not loaded, the returned
		 * vector is empty.
		 */
		EX vector<string> getUnmatchedBones() const;

		/**
		 * Adds a bone transform to a set with identifier setID.
		 * The identifier of the transform is only required to be unique
		 * in the set of BoneTransforms with identifier setID.
		 * A set of BoneTransforms represents an instance of the skeleton.
		 * The BoneTransform itself represents movement of the skeleton.
		 * The input matrix of the BoneTransform describes the movement
		 * of the bone with the same identifier as the BoneTransform
		 * in joint space from the bind pose into the moved state.
		 * The updateBoneTransforms method takes all input matrices
		 * of a set into consideration, applies the movements specified
		 * by the input matrices, and calculates the output matrix
		 * of the BoneTransform, which transforms the vertices associated
		 * to the bone with the identifier of the BoneTransform from
		 * the bind pose into the moved pose in model space coordinates.
		 * If the BoneTransform has no bone associated with it, the
		 * updateBoneTransforms method simply ignores it.
		 */
		EX void addBoneTransform(int setID, const BoneTransform &transform);

		/**
		 * Returns the BoneTransform with identifier boneID in the set
		 * of BoneTransforms with identifier setID. If no such BoneTransform
		 * exists, an exception is thrown.
		 */
		EX BoneTransform &getBoneTransform(int setID, const string &boneID);

		/**
		 * Removes the BoneTransform with identifier boneID in the set
		 * of BoneTransforms with identifier setID.
		 */
		EX void removeBoneTransform(int setID, const string &boneID);

		/**
		 * removes the set of BoneTransforms with identifier setID
		 */
		EX void removeBoneTransforms(int setID);

		/**
		 * removes all BoneTransforms
		 */
		EX void removeBoneTransforms();

		/**
		 * Calculates the movement of the vertices from bind pose
		 * into the moved position in model space coordinates.
		 * The method calculates the state of the moved position
		 * by taking the input matrices of the BoneTransforms
		 * into consideration, and copies the transformations
		 * of the vertices into the moved position into the
		 * output matrices of the BoneTransforms.
		 * Each set of BoneTransforms is treated as an instance
		 * of the Skeleton. The input matrix of a certain
		 * BoneTransform is interpreted as the movement
		 * of the bone with the same identifier from the bind
		 * pose into the moved state in the space of the bone's joint.
		 * If the input matrix is NULL, it's assumed to
		 * be the identity matrix. The transformation from
		 * the bind pose into the moved state in model space
		 * coordinates is copied into the output matrix.
		 * If the BoneTransform's output matrix is NULL,
		 * the output matrix remains unchanged. BoneTransforms,
		 * which are not associated with any bone are ignored.
		 */
		EX void updateBoneTransforms();

		/**
		 * calls method updateBoneTransforms()
		 */
		EX void execute(vector<string> &input);
		
	};

	/**
	 * A blendfactor is used to specify, how a fragment is blended
	 * with its background, when it's drawn. When a fragment f is
	 * rendered, a blendfactor fragmentblendfactor is multiplied with the fragment,
	 * and another bendfactor targetblendfactor is multiplied with the fragment
	 * fbo in the framebuffer located on the position of fragment f,
	 * and the sum of both values replaces the former value fbo in the framebuffer.
	 * Hence the new value new_fbo of the framebufferfragment
	 * depending on the original value fbo of the framebufferfragment fbo, the value f
	 * of the fragment about to be rendered, and the blendfactors
	 * fragmentblendfactor, targetblendfactor is computed by evaluating the formula
	 *
	 * new_fbo = fbo * targetblendfactor + f * fragmentblendfactor
	 *
	 * the initial values of targetblendfactor and fragmentblendfactor are
	 * targetblendfactor = ZERO
	 * fragmentblendfactor = ONE
	 *
	 * Example 1: Suppose a transparent primitive should be rendered, such that fragments
	 *     of the primitive with alphavalue = 1 are covering the background completely,
	 *     and fragments with alphavalue = 0 let the background shine through without
	 *     being visible. This can be achieved by using the blendfactors
	 *     fragmentblendfactor = FRAGMENTALPHA, targetblendfactor = ONE_MINUS_FRAGMENTALPHA
	 * Example 2: Suppose a non-transparent primitive should be rendered, such that
	 *     the background is completely covered by the primitive. This can be achieved
	 *     by using the blendfactors
	 *     fragmentblendfactor = ONE, targetblendfactor = ZERO
	 */
	enum BlendFactor {
		ZERO,						//Blendfactor = 0
		ONE,						//Bendfactor = 1
		FRAGMENTCOLOR,				//Blendfactor = color of the fragment, which will be rendered
		FRAMEBUFFERCOLOR,			//Blendfactor = color of framebufferfragment
		FRAGMENTALPHA,				//Blendfactor = alphavalue of the fragment, which will be rendered
		FRAMEBUFFERALPHA,			//Blendfactor = alphavalue of the framebufferfragment
		ONE_MINUS_FRAGMENTCOLOR,	//Blendfactor = 1 - color of the fragment, which will be rendered
		ONE_MINUS_FRAMEBUFFERCOLOR,	//Blendfactor = 1 - color of the framebufferfragment
		ONE_MINUS_FRAGMENTALPHA,	//Blendfactor = 1 - alphavalue of the fragment, which will be rendered
		ONE_MINUS_FRAMEBUFFERALPHA	//Blendfactor = 1 - alphavalue of the framebufferfragment
	};

	/**
	 * A depth test is used to determine wether a pixel is covered
	 * by the framebuffercontent or not depending on its distance from
	 * the camera. The distances of the fragments in the framebuffer
	 * are stored in a depthbuffer. The depth test compares the depthvalue
	 * of the fragment about to be rendered with the depthvalue of the framebuffer
	 * , and discards it iff the depth test fails.
	 * The default depth test is ACCEPT_LESS.
	 */
	enum DepthTest {
		ACCEPT_NEVER,			//fails always
		ACCEPT_ALWAYS,			//succeeds always
		ACCEPT_LESS,			//succeeds, if the fragment, which is about to be rendered, has a smaller depthvalue than the depthvalue in the framebuffer
		ACCEPT_LESS_EQUAL,		//succeeds, if the fragment, which is about to be rendered, has a smaller depthvalue than, or a depthvalue equal to the depthvalue in the framebuffer
		ACCEPT_GREATER,			//succeeds, if the fragment, which is about to be rendered, has a bigger depthvalue than the depthvalue in th framebuffer
		ACCEPT_GREATER_EQUAL,	//succeeds, if the fragment, which is about to be rendered, has a bigger depthvalue than, or a depthvalue equal to the depthvalue in the framebuffer
		ACCEPT_EQUAL,			//succeeds, if the fragment, which is about to be rendered, has a depthvalue equal to the depthvalue in the framebuffer
		ACCEPT_NOT_EQUAL		//succeeds, if the fragment, which is about to be rendered, has a depthvalue different from the depthvalue in the framebuffer
	};

	/**
	 * framebuffer, used to render into textures
	 */
	class RenderTarget: public SXResource {
	private:

		/**
		 * last token of id
		 */
		string idToken;

		/**
		 * true if the target is loaded
		 */
		bool loaded;

		//resources not loaded yet

		/**
		 * width of the target after the load operation,
		 * positive if loaded
		 */
		int newWidth;

		/**
		 * height of the target after the load operation,
		 * positive if loaded
		 */
		int newHeight;

		/**
		 * true if the data rendered to the RenderTarget
		 * is displayed, and not copied to an offscreen buffer
		 * after the load operation
		 */
		bool newRenderToDisplay;

		//loaded resources

		/**
		 * width of the target,
		 * value not specified, if not loaded
		 */
		int width;

		/**
		 * height of the target,
		 * value not specified, if not loaded
		 */
		int height;

		/**
		 * id on vram of the target
		 */
		unsigned int target;

		/**
		 * renderbuffer for depth values
		 */
		unsigned int renderbuffer;

		/**
		 * true if the data rendered to the RenderTarget
		 * is displayed, and not copied to an offscreen buffer
		 */
		bool renderToDisplay;

		/**
		 * maps a depth test to its corresponding OpenGL depthfunc
		 */
		static unordered_map<int,int> depthTest2GL;

		/**
		 * maps a blendfactor to its corresponding OpenGL blendfactor
		 */
		static unordered_map<int,int> blendFactor2GL;

		/**
		 * init depthTest2GL
		 */
		static unordered_map<int,int> initDepthTest2GL();

		/**
		 * init blendFactor2GL
		 */
		static unordered_map<int,int> initBlendFactor2GL();

		/**
		 * copy constructor, not used
		 */
		RenderTarget(const RenderTarget &);

		/**
		 * assignment operator, not used
		 */
		RenderTarget &operator = (const RenderTarget &);

	public:

		/**
		 * constructor
		 */
		EX RenderTarget(const string &id);
		
		/**
		 * destructor
		 */
		EX ~RenderTarget();

		/**
		 * deletes the color and depth buffer of
		 * the currently set rendertarget
		 */
		EX static void clearTarget();

		/**
		 * deletes the depth buffer of
		 * the currently set rendertarget
		 */
		EX static void clearDepthBuffer();

		/**
		 * deletes the color buffer of
		 * the currently set rendertarget
		 */
		EX static void clearColorBuffer();

		/**
		 * Determines, which kind of depthtest is executed.
		 * For a detailed description of test depthtest see
		 * @see sx::DepthTest
		 */
		EX static void applyDepthTest(DepthTest test);

		/**
		 * Determines, if rendering into the framebuffer
		 * has an effect on the depthbuffer or not.
		 * @param write true iff rendering into the framebuffer alters the depthbuffer
		 */
		EX static void applyWriteDepthBuffer(bool write);

		/**
		 * Assigns values to the blendfactors.
		 * For a detailed description of the blendfactors see
		 * @see sx::BlendFactor
		 */
		EX static void applyBlendFactors(BlendFactor fragmentBlendFactor, BlendFactor targetBlendFactor);

		/**
		 * sets the viewport such that its lower left corner is at (x,y),
		 * and it has dimensions (width,height)
		 */
		EX static void setViewport(int x, int y, int width, int height);

		/**
		 * covers the whole rendertarget with the viewport, if this is loaded
		 */
		EX void setViewport();

		/**
		 * sets the viewport such that its lower left corner is located at (width * xFactor, height * yFactor),
		 * and it has dimensions (width * widthFactor, height * heightFactor), if this is loaded
		 */
		EX void setViewport(float xFactor, float yFactor, float widthFactor, float heightFactor);

		/**
		 * deletes the last framebuffer, if loaded,
		 * and leaves the framebuffer unloaded
		 */
		EX void unload();

		/**
		 * Deletes the last framebuffer, and loads
		 * a new one with the specified width,
		 * height and pixelFormat.
		 * If not enough information has been specified, the method
		 * has no effect.
		 */
		EX void load();

		/**
		 * returns true if a texture is loaded
		 */
		EX bool isLoaded() const;

		/**
		 * After this call, the next draw commands render
		 * into the textures from the parameter list. The
		 * framebuffer must be loaded, otherwise the method
		 * has no effect. The method only affects those textures
		 * which have the same width, height and pixelFormat.
		 */
		EX void beginRender(Shader &shader, vector<Texture *> &textures, const string &resourceID);

		/**
		 * After this call, the next draw commands render
		 * into the layer of number layer in the 3D textures from the
		 * parameter list. The framebuffer must be loaded, otherwise
		 * the method has no effect. The method only affects those textures
		 * which have the same width, height and pixelFormat.
		 */
		EX void beginRender(Shader &shader, vector<Volume *> &textures, unsigned int layer, const string &resourceID);

		/**
		 * after this call, the next draw commands have
		 * no effect on the textures specified as rendertargets with beginRender
		 */
		EX void endRender();

		/**
		 * Specifies if the content rendered to the target is displayed
		 * on the screen, and not rendered to textures after the load operation. If renderToDisplay is true,
		 * the target is loaded, and setWidth/Height have direct effect on
		 * getWidth/Height.
		 */
		EX void setRenderToDisplay(bool renderToDisplay);

		/**
		 * true iff target renders to display
		 */
		EX bool isRenderingToDisplay() const;

		/**
		 * Specifies the width of the next texture loaded by load().
		 * If renderToDisplay is true, getWidth returns the value
		 * set by this method immediately.
		 */
		EX void setWidth(int width);

		/**
		 * getter of the loaded texture, if no
		 * texture is loaded, the return value isn't specified
		 */
		EX int getWidth() const;

		/**
		 * Specifies the height of the next texture loaded by load().
		 * If renderToDisplay is true, getHeight returns the value
		 * set by this method immediately.
		 */
		EX void setHeight(int height);

		/**
		 * getter of the loaded texture, if no
		 * texture is loaded, the return value isn't specified
		 */
		EX int getHeight() const;

	};

	class RenderObject: public SXResource {
	private:

		/**
		 * true iff the RenderObject can be rendered
		 */
		bool visible;

		/**
		 * A mesh. If no mesh is specified,
		 * the RenderObject can't be rendered.
		 */
		Mesh *mesh;

		/**
		 * A shader program. If no shader is
		 * specified, the shader from the pass
		 * rendering the RenderObject is used.
		 */
		Shader *shader;

		/**
		 * set of the RenderObject's uniform variables
		 * associated with their names
		 */
		unordered_map<string,Uniform *> uniforms;

		/**
		 * Contains data for instanced rendering,
		 * which can be set/unset with
		 * setInstancedBuffers/removeInstancedBuffers
		 */
		vector<BufferedMesh *> instanceBuffers;

		/**
		 * Sets of uniform variables. For each set the
		 * RenderObject is rendered once.
		 */
		unordered_map<int,unordered_map<string,Uniform *>> uniformSets;

		/**
		 * copy constructor, not used
		 */
		RenderObject(const RenderObject &);

		/**
		 * assignment operator, not used
		 */
		RenderObject &operator = (const RenderObject &);

	public:

		/**
		 * constructor
		 */
		EX RenderObject(const string &id);

		/**
		 * deconstructor
		 */
		EX ~RenderObject();

		/**
		 * as this is always loaded,
		 * this method has no effect on this
		 */
		EX void load();

		/**
		 * as this is always loaded, the return
		 * value is always true
		 */
		EX bool isLoaded() const;

		/**
		 * setter
		 */
		EX void setVisible(bool visible);

		/**
		 * getter
		 */
		EX bool isVisible() const;

		/**
		 * setter
		 */
		EX void setMesh(Mesh &mesh);

		/**
		 * getter
		 */
		EX Mesh *getMesh();

		/**
		 * setter
		 */
		EX void setShader(Shader *shader);

		/**
		 * getter
		 */
		EX Shader *getShader();

		/**
		 * Adds a uniform variable to the RenderObject.
		 * The uniform's name must be unique in the RenderObject,
		 * if another variable with the same name exists, it's replaced
		 * by uniform.
		 */
		EX void addUniform(Uniform &uniform);

		/**
		 * Removes the uniform named name. If such a uniform variable
		 * doesn't exist, the method does nothing.
		 */
		EX void removeUniform(const string &name);

		/**
		 * removes all uniforms
		 */
		EX void removeUniforms();

		/**
		 * Assigns a set BufferedMeshes to this. this is rendered once for
		 * each vertex in instanceBuffers. When this is rendered the k. time
		 * the k. vertex of each VertexBuffer can be accessed in the shader
		 * as a vertex attribute.
		 * If mesh is not a BufferedMesh, this method does not affect the
		 * behaviour of method render().
		 */
		EX void setInstanceBuffers(const vector<BufferedMesh *> &instanceBuffers);

		/**
		 * undoes the changes of setInstanceBuffers
		 */
		EX void removeInstanceBuffers();

		/**
		 * Returns the set of uniform variables associated with an integer key. If such
		 * a set hasn't existed yet, its created. For each uniform set the mesh with the RenderObject's shader and
		 * uniform variables in uniforms and the uniform set is rendered once.
		 *
		 * @param id unique integer associated with the uniform set returned
		 * @return uniform set associated with key name
		 */
		EX unordered_map<string, Uniform *> &getUniformSet(const int id);

		/**
		 * Removes the uniform set associated to id. If such a uniform set
		 * doesn't exist, the method does nothing.
		 */
		EX void removeUniformSet(const int id);

		/**
		 * removes all uniform sets
		 */
		EX void removeUniformSets();

		/**
		 * Renders the RenderObject. If no shader is attached to
		 * the RenderObject, the shader passShader in the parameter is used.
		 * If uniformSets is nonempty, the mesh is rendered once for each set in uniformSets.
		 * If instanceBuffers is nonempty, and mesh is a BufferedMesh, the mesh is rendered
		 * once for the first k vertices in instanceBuffers, if k is the amount of vertices
		 * of the BufferedMesh with the minimum amount of vertices in instanceBuffers.
		 * If visible is false, render doesn't do anything.
		 * Iff both shaders are null, or mesh is null, render doesn't do anything.
		 *
		 * @param passShader Shader provided by the pass rendering the RenderObject, may be null. If a shader is attached to the RenderObject, its used instead of passShader.
		 */
		EX void render(Shader *passShader);

	};

	class Pass: public SXResource, public EffectObject {
	private:

		/**
		 * true iff the Pass can be rendered
		 */
		bool visible;

		/**
		 * Shader of the pass. If all RenderObjects have
		 * their own shaders, shader doesn't need to be set.
		 */
		Shader *shader;

		/**
		 * RenderObjects associated with their ids. The RenderObjects
		 * are rendered in an arbitrary order.
		 */
		unordered_map<string,RenderObject *> renderObjects;

		/**
		 * RenderObjects only rendered for one frame. Once all RenderObjects are rendered,
		 * the RenderObjects are removed from tempRenderObjects. The RenderObjects
		 * are rendered from the lowest to the highest index.
		 */
		vector<vector<RenderObject *>> tempRenderObjects;

		/**
		 * The backgroundQuad is made of two triangles with two buffers:
		 * vertices(attributeSize = 3):
		 *		triangle nr1:
		 *			-1 -1 0
		 *			1 -1 0
		 *			1 1 0
		 *		triangle nr2:
		 *			-1 -1 0
		 *			1 1 0
		 *			-1 1 0
		 * texcoords(attributeSize = 2):
		 *		triangle nr1:
		 *			0 0
		 *			1 0
		 *			1 1
		 *		triangle nr2:
		 *			0 0
		 *			1 1
		 *			0 1
		 */
		BufferedMesh *backgroundQuadMesh;
		RenderObject *backgroundQuad;

		/**
		 * set of the RenderObject's uniform variables
		 * associated with their names
		 */
		unordered_map<string,Uniform *> uniforms;

		/**
		 * true iff the color buffer is deleted before rendering
		 */
		bool clearColorbuffer;

		/**
		 * true iff the depth buffer is deleted before rendering
		 */
		bool clearDepthbuffer;

		/**
		 * true iff the objects are rendered in wireframe
		 */
		bool wireframe;

		BlendFactor fragmentBlendFactor;
		BlendFactor targetBlendFactor;
		DepthTest depthTest;
		bool writeDepthBuffer;

		/**
		 * rendertarget, must be set, otherwise method
		 * render doesn't render anything
		 */
		RenderTarget *renderTarget;

		/**
		 * The textures which receive the output of the pass.
		 * A pass can only render into targets or volumeTargets
		 * exclusively at the same time.
		 */
		vector<Texture *> targets;

		/**
		 * The volumes which receive the output of the pass.
		 * A pass can only render into targets or volumeTargets
		 * exclusively at the same time.
		 */
		vector<Volume *> volumeTargets;

		/**
		 * The BufferedMesh which receives the geometry of the pass.
		 * A geometryTarget can be used simultaneously with a texture
		 * or volume target.
		 */
		BufferedMesh *geometryTarget;

		/**
		 * Uniform float used during rendering into 3D textures.
		 * The uniform variable provides the shader with the depth
		 * value of the layer, which is currently used for rendering.
		 * The layer with the lowest number has coordinate 0, and
		 * the layer with the highest number has coordinate 1.
		 * If the pass is rendering into a 2D texture, its value is zero.
		 */
		UniformFloat *layerCoordinate;

		/**
		 * Addresses the first layer which is used for rendering into 3D textures.
		 * Pass always renders into an interval of layers of a 3D texture, if
		 * a 3D texture is used as a rendertarget. The first layer in the interval
		 * is addressed by the value of floor(*startLayer). If startLayer points to NULL,
		 * pass starts rendering with the first layer in the 3D texture.
		 */
		UniformFloat *startLayer;
		
		/**
		 * Addresses the last layer which is used for rendering into 3D textures.
		 * Pass always renders into an interval of layers of a 3D texture, if
		 * a 3D texture is used as a rendertarget. The last layer in the interval
		 * is addressed by the value of floor(*endLayer). If endLayer points to NULL,
		 * pass renders into layers until the last layer of the 3D texture is reached.
		 */
		UniformFloat *endLayer;

		/**
		 * copy constructor, not used
		 */
		Pass(const Pass &);

		/**
		 * assignment operator, not used
		 */
		Pass &operator = (const Pass &);

	public:

		/**
		 * If wireframe is true, the RenderObjects
		 * rendered after the call of this function are
		 * rendered in wireframe. Otherwise the inside
		 * of the polygons is filled with fragments.
		 */
		EX static void applyWireframe(bool wireframe);

		/**
		 * constructor
		 */
		EX Pass(const string &id);

		/**
		 * deconstructor
		 */
		EX ~Pass();

		/**
		 * as this is always loaded,
		 * this method has no effect on this
		 */
		EX void load();

		/**
		 * as this is always loaded, the return
		 * value is always true
		 */
		EX bool isLoaded() const;

		/**
		 * setter
		 */
		EX void setVisible(bool visible);

		/**
		 * getter
		 */
		EX bool isVisible() const;

		/**
		 * setter
		 */
		EX void setShader(Shader *shader);

		/**
		 * getter
		 */
		EX Shader *getShader();

		/**
		 * Adds a RenderObject object to the Pass. Object
		 * will be rendered every time the render method of the
		 * pass is invoced.
		 */
		EX void addRenderObject(RenderObject &object);

		/**
		 * Removes the RenderObject with identifyer id. The RenderObject
		 * won't be rendered again. If no RenderObject with identifyer id
		 * was inserted into the Pass, the method doesn't do anything.
		 */
		EX void removeRenderObject(const string &id);

		/**
		 * Removes all RenderObjects, which were inserted with addRenderObject. Those
		 * RenderObjects won't be rendered again.
		 */
		EX void removeRenderObjects();

		/**
		 * Determines if a background quad is rendered.
		 * If use equals true, a call of render() will render the RenderObject
		 * backgroundQuad. If use equals false, the backgroundQuad will not be
		 * rendered by a call of render().
		 * If another RenderObject with id "sx.engine.core.Pass.cpp.backgroundQuad"
		 * was added to the Pass with method addRenderObject, useBackgroundQuad
		 * has no effect.
		 * The backgroundQuad is made of two triangles with two buffers:
		 * vertices(attributeSize = 3):
		 *		triangle nr1:
		 *			-1 -1 0
		 *			1 -1 0
		 *			1 1 0
		 *		triangle nr2:
		 *			-1 -1 0
		 *			1 1 0
		 *			-1 1 0
		 * texcoords(attributeSize = 2):
		 *		triangle nr1:
		 *			0 0
		 *			1 0
		 *			1 1
		 *		triangle nr2:
		 *			0 0
		 *			1 1
		 *			0 1
		 */
		EX void useBackgroundQuad(bool use);

		/**
		 * Adds RenderObjects, which will be only rendered the next time
		 * method render in Pass is invoced. Those RenderObjects will
		 * be rendered in the order given by temp.
		 */
		EX void addTempRenderObjects(const vector<RenderObject *> &temp);

		/**
		 * Returns a reference to an empty vector. The RenderObjects
		 * inserted into this reference will be rendered the next time
		 * method render in Pass is invoced. Those RenderObjects will be rendered
		 * in the order they are inserted into the vector.
		 */
		EX vector<RenderObject *> &getTempRenderObjects();

		/**
		 * Adds a uniform variable to the Pass.
		 * The uniform's name must be unique in the Pass,
		 * otherwise an exception is thrown.
		 *
		 * @param uniform a uniform variable
		 * @exception if the name of the uniform variable isn't unique in the set of uniforms, an exception Exception of type EX_AMBIGUOUS is thrown
		 */
		EX void addUniform(Uniform &uniform);

		/**
		 * Removes the uniform named name. If such a uniform variable
		 * doesn't exist, an exception is thrown.
		 *
		 * @param name name of the uniform variable
		 * @exception if a uniform variable named name doesn't exist, an Exception of type EX_NODATA is thrown
		 */
		EX void removeUniform(const string &name);

		/**
		 * removes all uniforms
		 */
		EX void removeUniforms();

		/**
		 * setter
		 */
		EX void setWireframe(bool wireframe);

		/**
		 * getter
		 */
		EX bool isWireframe() const;

		/**
		 * setter
		 */
		EX void setClearColorBuffer(bool clear);

		/**
		 * getter
		 */
		EX bool isClearingColorBuffer() const;

		/**
		 * setter
		 */
		EX void setClearDepthBuffer(bool clear);

		/**
		 * getter
		 */
		EX bool isClearingDepthBuffer() const;

		/**
		 * setter
		 */
		EX void setFragmentBlendFactor(BlendFactor factor);

		/**
		 * getter
		 */
		EX BlendFactor getFragmentBlendFactor() const;

		/**
		 * setter
		 */
		EX void setTargetBlendFactor(BlendFactor factor);

		/**
		 * getter
		 */
		EX BlendFactor getTargetBlendFactor() const;

		/**
		 * setter
		 */
		EX void setDepthTest(DepthTest test);

		/**
		 * getter
		 */
		EX DepthTest getDepthTest();

		/**
		 * Determines, if rendering into the rendertarget
		 * has an effect on the depthbuffer or not.
		 * @param write true iff rendering into the rendertarget should alter the depthbuffer
		 */
		EX void setWriteDepthBuffer(bool write);

		/**
		 * Returns wether rendering into the rendertarget
		 * has an effect on the depthbuffer or not.
		 * @return true iff rendering into the rendertarget alters the depthbuffer
		 */
		EX bool isWritingDepthBuffer();

		/**
		 * Assigns a RenderTarget to Pass. The output of pass is rendered
		 * to the textures in targets. If RenderTarget target is the display,
		 * the textures in targets remain unchanged. If no RenderTarget
		 * is set, method render doesn't do anything.
		 */
		EX void setOutput(RenderTarget &target, vector<Texture *> targets);

		/**
		 * Assigns a RenderTarget to Pass. The output of pass is rendered
		 * to the textures in targets. If RenderTarget target is the display,
		 * the textures in targets remain unchanged. If no RenderTarget
		 * is set, method render doesn't do anything.
		 */
		EX void setOutput(RenderTarget &target, vector<Volume *> targets);

		/**
		 * Assigns a geometry target to Pass. The geometry generated by method render()
		 * is stored in the BufferedMesh target. The method will only render geometry
		 * to those VertexBuffers, which have identifiers added to the shader of Pass
		 * with method addTransformFeedbackBuffer(). If target is 0, no geometry target
		 * is used.
		 */
		EX void setOutput(BufferedMesh *target);

		/**
		 * setter
		 */
		EX void setLayerCoordinate(UniformFloat *layerCoordinate);

		/**
		 * setter
		 */
		EX void setStartLayer(UniformFloat *startLayer);

		/**
		 * setter
		 */
		EX void setEndLayer(UniformFloat *endLayer);

		/**
		 * Renders all RenderObjects to the rendertarget, and the textures, if a rendertarget
		 * with textures is set.
		 * Renders all RenderObjects to the rendertarget, and the volumes, if a rendertarget
		 * with volumes is set.
		 * Stores the geometry generated with RenderObjects in geometryTarget, if
		 * geometryTarget is set.
		 * If no shader is attached to a RenderObject, the shader of the pass is used.
		 * If no rendertarget has been specified, the method doesn't do anything.
		 * If visible is false, the method doesn't do anything.
		 * Iff both shaders are null, or mesh is null, render doesn't do anything.
		 */
		EX void render();

		/**
		 * invokes method render
		 */
		EX void execute(vector<string> &input);

	};

	/**
	 * association of an EffectObject with
	 * a vector of input strings
	 */
	struct EffectCommand {
		vector<string> input;
		EffectObject *object;
	};

	class Effect: public SXResource {
	private:

		/**
		 * True iff method render
		 * renders something. If visible is false,
		 * all textures and rendertargets including the
		 * display stay unaffected.
		 */
		bool visible;

		/**
		 * Objects rendered for every invocation of render in
		 * Effect. The objects are rendered in the order
		 * given by vector passes.
		 */
		vector<EffectCommand> objects;

		/**
		 * copy constructor, not used
		 */
		Effect(const Effect &);

		/**
		 * assignment operator, not used
		 */
		Effect &operator = (const Effect &);

	public:

		/**
		 * constructor
		 */
		EX Effect(const string &id);

		/**
		 * deconstructor
		 */
		EX ~Effect();

		/**
		 * as this is always loaded,
		 * this method has no effect on this
		 */
		EX void load();

		/**
		 * as this is always loaded, the return
		 * value is always true
		 */
		EX bool isLoaded() const;

		/**
		 * setter
		 */
		EX void setVisible(bool visible);

		/**
		 * getter
		 */
		EX bool isVisible() const;

		/**
		 * Returns a vector containing all EffectObjects rendered
		 * by method render. The passes are rendered in the
		 * order given by the returned vector.
		 */
		EX vector<EffectCommand> &getObjects();

		/**
		 * Calls execute in all EffectObjects in the order given by vector objects,
		 * and provides them with the input associated with them through struct EffectCommand
		 * through the parameter of method execute.
		 * If visible is false, the method doesn't do anything.
		 */
		EX void render();

	};

	/**
	 * Audio Buffer class. This class holds the dataset
	 * specifying sound. To play the content stored in
	 * an AudioBuffer, an AudioObject must be used.
	 * Multiple AudioObjects can use and playback the
	 * content of one buffer simultaneously.
	 */
	class AudioBuffer: public SXResource {
	private:

		//resource requests

		/**
		 * path pointing to the file
		 * containing the content, which
		 * will be loaded into the AudioBuffer
		 * by the next call of load
		 */
		string newPath;

		//loaded resources

		/**
		 * true iff the last invocation of load
		 * was successfull
		 */
		bool loaded;

		/**
		 * OpenAL id of the buffer
		 */
		ALuint buffer;

		/**
		 * copy constructor, not used
		 */
		AudioBuffer(const AudioBuffer &);

		/**
		 * assignment operator, not used
		 */
		AudioBuffer &operator = (const AudioBuffer &);

	public:

		/**
		 * constructor
		 */
		EX AudioBuffer(const string &id);

		/**
		 * deconstructor
		 */
		EX ~AudioBuffer();

		/**
		 * If setPath was called with a valid url pointing
		 * to a file, load tries to load audio content to this.
		 * This is only loaded, iff the invocation of load
		 * succeeded in loading from the path specified by setPath.
		 * If setPath has not been called after the last successful
		 * invocation of load, the next invocation of load has no effect.
		 */
		EX void load();

		/**
		 * deconstructs the currently loaded resources,
		 * and leaves this unloaded
		 */
		EX void unload();

		/**
		 * returns true iff this is loaded
		 */
		EX bool isLoaded() const;

		/**
		 * Specifies the path of a file from which
		 * the audio content of this will be loaded.
		 * Currently only wav files are supported.
		 */
		EX void setPath(const string &path);

		/**
		 * Returns the OpenAL buffer id. If this
		 * is not loaded, 0 is returned.
		 */
		EX unsigned int getBufferID() const;

	};

	/**
	 * Class used to playback the content of an
	 * AudioBuffer. Several parameters like
	 * position, speed, volume, pitch, or
	 * how the pitch changes depending on the
	 * distance to the listener can be specified.
	 * Additionally this class is responsible
	 * for initializing the layer communicating
	 * with the sound device.
	 */
	class AudioObject: public SXResource {
	private:

		/**
		 * true iff OpenAl is initialized
		 */
		static bool initialized;

		/**
		 * OpenAL device
		 */
		static ALCdevice *device;

		/**
		 * OpenAL context
		 */
		static ALCcontext *context;

		//loaded resources

		/**
		 * true iff this is loaded
		 */
		bool loaded;

		/**
		 * OpenAl id of the source
		 * used to implement the functionality
		 * of AudioObject
		 */
		ALuint audio;

		/**
		 * true iff during the last invocation
		 * of  start, isPlaying or update
		 * this was playing audio content
		 */
		bool playing;

		//state

		/**
		 * audio content, which is used to playback sound
		 */
		AudioBuffer *buffer;

		/**
		 * Position of the source. If position is NULL,
		 * this is assumed to be at (0,0,0).
		 */
		const UniformVector *position;

		/**
		 * If the listener is within the referenceRadius
		 * around position of this, the volume
		 * of this is not influenced by the distance
		 * between listener and this.
		 * If referenceRadius, maxRadius or velocity
		 * is NULL, the pitch is not influenced
		 * by the distance between listener and this.
		 */
		const UniformFloat *referenceRadius;

		/**
		 * If the listener is beyond referenceRadius
		 * around position of this, and within the
		 * maxRadius, the pitch of this is multiplied
		 * by
		 * ((position - listener.position) / referenceRadius)^(-distanceExponent)
		 * If referenceRadius, maxRadius or velocity
		 * is NULL, the pitch is not influenced
		 * by the distance between listener and this.
		 */
		const UniformFloat *maxRadius;

		/**
		 * If the listener is beyond maxRadius
		 * around position of this, the pitch
		 * is multiplied by
		 * (maxRadius / referenceRadius)^(-distanceExponent)
		 * If referenceRadius, maxRadius or velocity
		 * is NULL, the pitch is not influenced
		 * by the distance between listener and this.
		 */
		const UniformFloat *distanceExponent;

		/**
		 * Velocity of this. If velocity is NULL,
		 * velocity is assumed to be (0,0,0).
		 */
		const UniformVector *velocity;

		/**
		 * Volume of this. If volume is NULL,
		 * volume is assumed to be 1.
		 */
		const UniformFloat *volume;

		/**
		 * Pitch of this. The original pitch
		 * of the audio content is 1. If
		 * pitch is NULL, pitch is assumed to be 1.
		 */
		const UniformFloat *pitch;

		/**
		 * True iff this starts playing again from
		 * the beginning of the current AudioBuffer
		 * , if during playback the end
		 * of the AudioBuffer is reached.
		 */
		bool looping;

		/**
		 * true if the invocation of start,
		 * stop or pause should not have any
		 * effect
		 */
		bool locked;

		/**
		 * If lockTime seconds passed after the
		 * last invocation of lock, locked becomes
		 * true with the next invocation of
		 * start, stop or pause.
		 */
		double lockTime;

		/**
		 * timer measuring the seconds after
		 * the last invocation of lock
		 */
		timer timedLock;

		/**
		 * copy constructor, not used
		 */
		AudioObject(const AudioObject &);

		/**
		 * assignment operator, not used
		 */
		AudioObject &operator = (const AudioObject &);

	public:

		/**
		 * Initializes the sound device
		 * communicating with AudioBuffer,
		 * AudioObject and AudioListener.
		 * A call of initAudio is not necessary,
		 * as AudioBuffer, AudioObject and AudioListener
		 * call initAudio if necessary. Method
		 * closeAudio will be called automatically
		 * when the application terminates.
		 */
		EX static void initAudio();

		/**
		 * Closes the sounddevice, if it
		 * was initialized by initAudio.
		 */
		EX static void closeAudio();

		/**
		 * Returns true iff the sound device
		 * communicating with AudioBuffer, AudioObject
		 * and AudioListener is initialized.
		 */
		EX static bool isInitialized();

		/**
		 * constructor
		 */
		EX AudioObject(const string &id);

		/**
		 * deconstructor
		 */
		EX ~AudioObject();

		/**
		 * loads the resources needed
		 * to playback the AudioBuffer
		 * specified with setAudioBuffer
		 */
		EX void load();

		/**
		 * unloads the resources of this
		 */
		EX void unload();

		/**
		 * returns true iff this is loaded
		 */
		EX bool isLoaded() const;

		/**
		 * setter
		 */
		EX void setAudioBuffer(AudioBuffer &buffer);

		/**
		 * setter
		 */
		EX void setPosition(const UniformVector *pos);

		/**
		 * setter
		 */
		EX void setReferenceRadius(const UniformFloat *radius);

		/**
		 * setter
		 */
		EX void setMaxRadius(const UniformFloat *maxRadius);

		/**
		 * setter
		 */
		EX void setDistanceExponent(const UniformFloat *exponent);

		/**
		 * setter
		 */
		EX void setVelocity(const UniformVector *vel);

		/**
		 * setter
		 */
		EX void setVolume(const UniformFloat *v);

		/**
		 * setter
		 */
		EX void setPitch(const UniformFloat *pitch);

		/**
		 * setter
		 */
		EX void setLooping(bool looping);

		/**
		 * If this is not paused, the method
		 * start playback at second 0.
		 * If lock(time) was called
		 * after t seconds, and t <= time
		 * holds, start has no effect.
		 */
		EX void start();

		/**
		 * Starts playback at second time.
		 * If lock(time) was called
		 * after t seconds, and t <= time
		 * holds, start has no effect.
		 */
		EX void start(double time);

		/**
		 * Stops playback. The next invocation
		 * of play() will start at second 0.
		 * If lock(time) was called
		 * after t seconds, and t <= time
		 * holds, stop has no effect.
		 */
		EX void stop();

		/**
		 * Pauses playback. The next invocation
		 * of play() will start at the point of
		 * time of the audio content, at which
		 * pause was called.
		 * If lock(time) was called
		 * after t seconds, and t <= time
		 * holds, pause has no effect.
		 */
		EX void pause();

		/**
		 * Returns true iff this is currently
		 * playing the content of this.
		 */
		EX bool isPlaying();

		/**
		 * returns the length of the audio content
		 * in seconds
		 */
		EX double getLength() const;

		/**
		 * Returns the point of time in seconds,
		 * at which the sound content is played.
		 */
		EX double getPlaybackPosition() const;

		/**
		 * Locks methods start, stop and pause
		 * for the next time seconds:
		 * For the next time seconds the invocation
		 * of start, stop and pause won't have
		 * any effect.
		 */
		EX void lock(double time);

		/**
		 * Unlocks methods start stop and pause:
		 * The next invocation of start, stop and pause
		 * is not influenced by the last invocation
		 * of lock, such that those operations
		 * have an effect on playback again.
		 */
		EX void unlock();

		/**
		 * Updates this copying the content from
		 * position, velocity, referenceRadius,
		 * maxRadius, distanceExponent, volume
		 * and pitch. In between two update calls,
		 * the change of those parameters won't influence
		 * the sound! Hence update must be called
		 * periodically!
		 */
		EX void update();

	};

	/**
	 * Class specifying the state of the 
	 * listener listening to the SoundObjects.
	 * At each particular moment of time,
	 * only one listener can exist. Sound
	 * is always perceived from the state
	 * of the AudioListener object, which
	 * was used the last time to invoke update.
	 */
	class AudioListener: public SXResource {
	private:

		/**
		 * Position of the listener. If
		 * position is NULL, position is
		 * assumed to be (0,0,0).
		 */
		const UniformVector *position;

		/**
		 * Viewing direction of the listener
		 * specifying the orientation of
		 * the listener together with vector up.
		 * If view is NULL, view is assumed
		 * to be (1,0,0).
		 */
		const UniformVector *view;

		/**
		 * Up vector of the listener
		 * specifying the orientation
		 * of the listener together with
		 * view. If up is NULL, up is assumed
		 * to be (0,0,1).
		 */
		const UniformVector *up;

		/**
		 * Velocity of the listener.
		 * If velocity is NULL, velocity
		 * is assumed to be (0,0,0).
		 */
		const UniformVector *velocity;

		/**
		 * Volume multiplied to each
		 * AudioObject's volume. If
		 * volume is NULL, volume is assumed
		 * to be 1.
		 */
		const UniformFloat *volume;

		/**
		 * copy constructor
		 */
		AudioListener(const AudioListener &);

		/**
		 * assigment operator
		 */
		AudioListener &operator = (const AudioListener &);

	public:

		/**
		 * constructor
		 */
		EX AudioListener(const string &id);

		/**
		 * deconstructor
		 */
		EX ~AudioListener();

		/**
		 * Loads the resources needed to update
		 * the listener's state.
		 */
		EX void load();

		/**
		 * unloads the resources loaded
		 * by load, and leaves this unloaded
		 */
		EX void unload();

		/**
		 * returns true iff this is loaded
		 */
		EX bool isLoaded() const;

		/**
		 * setter
		 */
		EX void setPosition(const UniformVector *pos);

		/**
		 * setter
		 */
		EX void setView(const UniformVector *view);

		/**
		 * setter
		 */
		EX void setUp(const UniformVector *up);

		/**
		 * setter
		 */
		EX void setVelocity(const UniformVector *vel);

		/**
		 * setter
		 */
		EX void setVolume(const UniformFloat *v);

		/**
		 * Updates this copying the content from
		 * position, velocity, view, up and volume.
		 * In between two update calls,
		 * the change of those parameters won't influence
		 * the sound! Hence update must be called
		 * periodically!
		 */
		EX void update();

	};

	/**
	 * A collection of AudioObjects with an AudioListener.
	 * The update method updates the sound sources and the listener.
	 */
	class AudioPass: public SXResource {
	private :

		/**
		 * Listener of the scene. If listener
		 * is NULL, listener is assumed to
		 * be at position (0,0,0), viewing in
		 * direction (1,0,0), having up vector (0,0,1).
		 */
		AudioListener *listener;

		/**
		 * AudioObjects contributing to the
		 * sound of the scene.
		 */
		unordered_map<string,AudioObject *> objects;

		/**
		 * Objects paused by the last invocation
		 * of setPause. Objects paused before
		 * must not be listed.
		 */
		vector<AudioObject *> pausedObjects;

		/**
		 * Objects not paused by the last invocation
		 * of setPause. Objects paused by
		 * setPause must not be listed.
		 */
		vector<AudioObject *> unpausedObjects;

		/**
		 * True if all AudioObjects in objects
		 * were paused by setPause.
		 */
		bool paused;

		/**
		 * copy constructor, not used
		 */
		AudioPass(const AudioPass &);

		/**
		 * assignment operator, not used
		 */
		AudioPass &operator = (const AudioPass &);
	public:

		/**
		 * constructor
		 */
		EX AudioPass(const string &id);

		/**
		 * deconstructor
		 */
		EX ~AudioPass();

		/**
		 * Pauses all AudioObjects in objects, if
		 * pause is true. Continues playing all
		 * AudioObjects paused by the last invocation
		 * of setPause(true), if pause is false. Objects
		 * in the pause state, not paused by setPause, are still
		 * in the pause state after the invocation of setPause(false).
		 */
		EX void setPause(bool pause);

		/**
		 * Returns true iff setPause was invoked the
		 * last time with pause == true.
		 */
		EX bool isPaused() const;

		/**
		 * as this is always loaded,
		 * this method has no effect on this
		 */
		EX void load();

		/**
		 * as this is always loaded,
		 * this method has no effect on this
		 */
		EX void unload();

		/**
		 * Returns true iff this is loaded. As this
		 * is always loaded, true is returned.
		 */
		EX bool isLoaded() const;

		/**
		 * setter
		 */
		EX void setAudioListener(AudioListener &listener);

		/**
		 * Adds an AudioObject to objects. If another
		 * AudioObject with the same id was added before,
		 * it's replaced by object.
		 */
		EX void addAudioObject(AudioObject &object);

		/**
		 * Removes the AudioObject with id. If no such
		 * AudioObject exists, the method has no effect.
		 */
		EX void removeAudioObject(const string &id);

		/**
		 * Removes all AudioObjects.
		 */
		EX void removeAudioObjects();

		/**
		 * Updates the AudioObjects in objects and the
		 * AudioListener listener. In between two update calls,
		 * the change of the listener's and objects' parameters won't influence
		 * the sound! Hence update must be called
		 * periodically!
		 */
		EX void update();

	};

}

#endif