#ifndef _LANDSCAPE_CPP_
#define _LANDSCAPE_CPP_

/**
 * Fractal Landscape generator
 * (c) 2016 by Tristan Bauer
 */
#include <Landscape.h>
#include <QApplication>
#include <QSplashScreen>
#include <QPixmap>
#include <QIcon>
#include <sx/Log4SX.h>
#include <sx/Exception.h>

int main(int argc, char **argv) {
	//The actual fractal computations are all happening in the shaders!
	//The shaders can be found in the configuration file terrain.c
	//in folder ./data/landscape/configuration .

	try {
		sx::Logger::addLogger("landscape", new sx::FileLogger("Landscape.html"));
		sx::Logger::setDefaultLogger("landscape");
	} catch(sx::Exception &e) {
		sx::Logger::get() << sx::Level(sx::L_FATAL_ERROR) << e.getMessage();
		return 1;
	}

	QApplication app(argc,argv);
	QSplashScreen splashScreen(QPixmap("../data/landscape/textures/Splashscreen.png"));
	splashScreen.show();
	app.processEvents();

	landscape::MainWidget widget;
	widget.setMinimumSize(1075,500);
	widget.setWindowTitle("Fractal Landscape generator");
	widget.setWindowIcon(QIcon("../data/landscape/icons/Landscape.png"));
	widget.show();
	splashScreen.finish(&widget);

	return app.exec();
}

#endif