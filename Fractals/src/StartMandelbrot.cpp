#ifndef _STARTMANDELBROT_CPP_
#define _STARTMANDELBROT_CPP_

/**
 * Mandelbrot fractal generator
 * (c) 2016 by Tristan Bauer
 */
#include <Mandelbrot.h>
#include <QApplication>
#include <QSplashScreen>
#include <QPixmap>
#include <QIcon>
#include <QCheckBox>
#include <sx/Log4SX.h>
#include <sx/Exception.h>

int main(int argc, char **argv) {
	//The actual fractal computations are all happening in the shaders!
	//The shaders can be found in the configuration files mandelbrot.c and julia.c
	//in folder ./data/mandelbrot/configuration .

	try {
		sx::Logger::addLogger("mandelbrot", new sx::FileLogger("Mandelbrot.html"));
		sx::Logger::setDefaultLogger("mandelbrot");
	} catch(sx::Exception &e) {
		sx::Logger::get() << sx::Level(sx::L_FATAL_ERROR) << e.getMessage();
		return 1;
	}

	QApplication app(argc,argv);

	mandelbrot::MainWidget widget;
	widget.setMinimumSize(1075,500);
	widget.setWindowTitle("Mandelbrot fractal generator");
	widget.setWindowIcon(QIcon("../data/mandelbrot/icons/Mandelbrot.png"));
	widget.show();

	return app.exec();
}

#endif