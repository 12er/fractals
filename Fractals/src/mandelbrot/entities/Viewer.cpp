#ifndef _MANDELBROT_ENTITIES_VIEWER_CPP_
#define _MANDELBROT_ENTITIES_VIEWER_CPP_

/**
 * Viewer class
 * (c) 2016 by Tristan Bauer
 */
#include <Mandelbrot.h>

namespace mandelbrot {

	Viewer::Viewer() {
		mouseWheelChange = 0;
		useJulia = true;
		showJulia = false;
		changeShowTime = 0;
	}

	Viewer::~Viewer() {
	}

	void Viewer::wheelRotates(float degrees) {
		mouseWheelChange += degrees;
	}

	void Viewer::setUseJulia(bool useJulia) {
		this->useJulia = useJulia;
		RenderObject &julia = Renderer::shadeX.getRenderObject("julia.object");
		if(!useJulia) {
			Matrix &juliaTransform = Renderer::shadeX.getUniformMatrix("present.julia.transform");
			juliaTransform = Matrix().translate(Vector(0.75f,-0.75f,-0.1f)) * Matrix().scale(Vector(0.25f,0.25f,1));
			Entity::getJulia().setVerticalLength(2);
			Entity::getJulia().setCenter(DVector());
			showJulia = false;
			changeShowTime = 0;
			julia.setVisible(false);
		} else {
			julia.setVisible(true);
		}
	}

	void Viewer::setShowJulia(bool showJulia) {
		this->showJulia = showJulia;
		changeShowTime = 1;
	}

	bool Viewer::isShowingJulia() const {
		return showJulia;
	}

	void Viewer::reshape(SXRenderArea &area) {
	}

	void Viewer::update(SXRenderArea &area) {
		if(changeShowTime == 0) {
			if(useJulia && area.hasMouseKey(MOUSE_RIGHT)) {
				setShowJulia(!showJulia);
				return;
			}
			DVector mousePosition = Vector((double)area.getMouseX(),(double)area.getMouseY());
			DVector guiPositionChange = mousePosition * -1.0 + oldMousePosition;
			oldMousePosition = mousePosition;

			if(!showJulia) {
				if(area.hasMouseKey(MOUSE_LEFT) || area.hasMouseKey(MOUSE_RIGHT)) {
					if(guiPositionChange[0] == 0 && guiPositionChange[1] == 0) {
						return;
					}
					DMatrix &transform = Renderer::shadeX.getUniformDMatrix("mandelbrot.transform");
					DVector center = Entity::getMandelbrot().getCenter();				
					DVector positionChange = 
						DMatrix(transform).submatrix() *
						DMatrix().scale(Vector((double)2.0/area.getWidth() , (double)2.0/area.getHeight() ,1)) *
						guiPositionChange
						;
					center = center + positionChange;
					Entity::getMandelbrot().setCenter(center);
				}
				
				if(guiPositionChange[0] != 0 || guiPositionChange[1] != 0) {
					DMatrix &transform = Renderer::shadeX.getUniformDMatrix("mandelbrot.transform");	
					DVector position = 
						transform *
						DMatrix().translate(Vector(-1,-1,0)) *
						DMatrix().scale(Vector((double)2.0/area.getWidth() , (double)2.0/area.getHeight() ,1)) *
						mousePosition
						;
					Entity::getJulia().setC(position);
				}

				if(mouseWheelChange != 0) {
					double verticalLengthFactor = max(1 - mouseWheelChange / 360.0 , 0.1);
					mouseWheelChange = 0;
					DMatrix &transform = Renderer::shadeX.getUniformDMatrix("mandelbrot.transform");
					DVector guiMousePos = DVector((double)area.getMouseX() , (double)area.getMouseY());
					DVector mousePos = 
						DMatrix().scale(DVector(2.0/(double)area.getWidth(),2.0/(double)area.getHeight(),1)) *
						DMatrix().translate(DVector((double)area.getWidth(),(double)area.getHeight()) * -0.5) *
						guiMousePos
						;
					DVector newCenter = transform * ( mousePos + mousePos * -verticalLengthFactor );

					double verticalLength = Entity::getMandelbrot().getVerticalLength();
					verticalLength *= verticalLengthFactor;
					Entity::getMandelbrot().setVerticalLength(verticalLength);
					Entity::getMandelbrot().setCenter(newCenter);
				}
			} else {
				if(area.hasMouseKey(MOUSE_LEFT) || area.hasMouseKey(MOUSE_RIGHT)) {
					if(guiPositionChange[0] == 0 && guiPositionChange[1] == 0) {
						return;
					}
					DMatrix &transform = Renderer::shadeX.getUniformDMatrix("julia.transform");
					DVector center = Entity::getJulia().getCenter();				
					DVector positionChange = 
						DMatrix(transform).submatrix() *
						DMatrix().scale(DVector((double)2.0/area.getWidth() , (double)2.0/area.getHeight() ,1)) *
						guiPositionChange
						;
					center = center + positionChange;
					Entity::getJulia().setCenter(center);
				}

				if(mouseWheelChange != 0) {
					double verticalLengthFactor = max(1 - mouseWheelChange / 360.0 , 0.1);
					mouseWheelChange = 0;
					DMatrix &transform = Renderer::shadeX.getUniformDMatrix("julia.transform");
					DVector guiMousePos = DVector((double)area.getMouseX() , (double)area.getMouseY());
					DVector mousePos = 
						DMatrix().scale(DVector(2.0/(double)area.getWidth(),2.0/(double)area.getHeight(),1)) *
						DMatrix().translate(DVector((double)area.getWidth(),(double)area.getHeight()) * -0.5) *
						guiMousePos
						;
					DVector newCenter = transform * ( mousePos + mousePos * -verticalLengthFactor );

					double verticalLength = Entity::getJulia().getVerticalLength();
					verticalLength *= verticalLengthFactor;
					Entity::getJulia().setVerticalLength(verticalLength);
					Entity::getJulia().setCenter(newCenter);
				}
			}
		} else {
			float timeDelta = area.getDeltaTime();
			changeShowTime = max(0 , changeShowTime - timeDelta);
			Matrix &juliaTransform = Renderer::shadeX.getUniformMatrix("present.julia.transform");
			if(showJulia) {
				juliaTransform = Matrix().translate(Vector(0,0,-0.1f)) * (1 - changeShowTime) + Matrix().translate(Vector(0.75f,-0.75f,-0.1f)) * Matrix().scale(Vector(0.25f,0.25f,1)) * changeShowTime;
			} else {
				juliaTransform = Matrix().translate(Vector(0,0,-0.1f)) * changeShowTime + Matrix().translate(Vector(0.75f,-0.75f,-0.1f)) * Matrix().scale(Vector(0.25f,0.25f,1)) * (1 - changeShowTime);
				if(changeShowTime == 0) {
					Entity::getJulia().setVerticalLength(2);
					Entity::getJulia().setCenter(Vector());
				}
			}
		}
	}

}

#endif