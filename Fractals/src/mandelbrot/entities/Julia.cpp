#ifndef _MANDELBROT_ENTITIES_JULIA_CPP_
#define _MANDELBROT_ENTITIES_JULIA_CPP_

/**
 * Julia class
 * (c) 2016 by Tristan Bauer
 */
#include <Mandelbrot.h>

namespace mandelbrot {

	Julia::Julia() {
		updateNecessary = true;
		precision = FRACTAL_32_BIT;
	}

	Julia::~Julia() {
	}

	void Julia::setVerticalLength(double length) {
		UniformDouble &verticalLength = Renderer::shadeX.getUniformDouble("julia.imaginaryDelta.float");
		verticalLength.value = length;
		updateNecessary = true;
	}

	double Julia::getVerticalLength() {
		UniformDouble &verticalLength = Renderer::shadeX.getUniformDouble("julia.imaginaryDelta.float");
		return verticalLength.value;
	}

	void Julia::setCenter(const DVector &center) {
		DVector &centerPosition = Renderer::shadeX.getUniformDVector("julia.imagecenter.vector");
		centerPosition = center;
		updateNecessary = true;
	}

	DVector Julia::getCenter() {
		DVector &centerPosition = Renderer::shadeX.getUniformDVector("julia.imagecenter.vector");
		return centerPosition;
	}

	void Julia::setC(const DVector &c) {
		DVector &cPosition = Renderer::shadeX.getUniformDVector("julia.c");
		cPosition = c;
		updateNecessary = true;
	}

	DVector Julia::getC() {
		DVector &c = Renderer::shadeX.getUniformDVector("julia.c");
		return c;
	}

	void Julia::setPrecision(FractalPrecision precision) {
		this->precision = precision;
		updateNecessary = true;
	}

	void Julia::saveAsImage(const string &path) {
		Texture &julia = Renderer::shadeX.getTexture("julia.texture");
		julia.save(path);
	}

	void Julia::setUpdateNecessary() {
		updateNecessary = true;
	}

	void Julia::reshape(SXRenderArea &area) {
		Texture &juliaTexture = Renderer::shadeX.getTexture("julia.texture");
		juliaTexture.setWidth(area.getWidth());
		juliaTexture.setHeight(area.getHeight());
		juliaTexture.setPixelFormat(FLOAT_RGBA);
		juliaTexture.load();
		updateNecessary = true;
	}

	void Julia::update(SXRenderArea &area) {
		if(updateNecessary) {
			double width = (float) area.getWidth();
			double height = (float) area.getHeight();
			if(width == 0 || height == 0) {
				return;
			}
			double verticalLength = getVerticalLength();
			DVector center = getCenter();
			DMatrix &juliaTransform = Renderer::shadeX.getUniformDMatrix("julia.transform");
			juliaTransform = 
				DMatrix().translate(center)
				* DMatrix().scale(DVector(verticalLength/2.0 * (width/height) , (verticalLength/2.0) ,1))
				;

			if(precision == FRACTAL_32_BIT) {
				Pass &mandelbrot = Renderer::shadeX.getPass("julia.pass");
				mandelbrot.render();
			} else {
				Pass &mandelbrot = Renderer::shadeX.getPass("julia64.pass");
				mandelbrot.render();
			}
			updateNecessary = false;
		}
	}

}

#endif