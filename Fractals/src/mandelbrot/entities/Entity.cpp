#ifndef _LANDSCAPE_ENTITIES_ENTITY_CPP_
#define _LANDSCAPE_ENTITIES_ENTITY_CPP_

/**
 * Entity class
 * (c) 2016 by Tristan Bauer
 */
#include <Mandelbrot.h>

namespace mandelbrot {

	vector<Entity *> Entity::entities;
	Viewer *Entity::viewer = 0;
	Mandelbrot *Entity::mandelbrot = 0;
	Julia *Entity::julia = 0;

	void Entity::initEntities() {
		viewer = new Viewer();
		entities.push_back(viewer);

		mandelbrot = new Mandelbrot();
		entities.push_back(mandelbrot);

		julia = new Julia();
		entities.push_back(julia);
	}

	void Entity::reshapeEntities(SXRenderArea &area) {
		for(vector<Entity *>::iterator iter = entities.begin() ; iter != entities.end() ; iter++) {
			Entity *entity = (*iter);
			entity->reshape(area);
		}
	}

	void Entity::updateEntities(SXRenderArea &area) {
		for(vector<Entity *>::iterator iter = entities.begin() ; iter != entities.end() ; iter++) {
			Entity *entity = (*iter);
			entity->update(area);
		}
	}

	void Entity::destroyEntities() {
		for(vector<Entity *>::iterator iter = entities.begin() ; iter != entities.end() ; iter++) {
			delete (*iter);
		}
		entities.clear();
	}

	Viewer &Entity::getViewer() {
		return *viewer;
	}

	Mandelbrot &Entity::getMandelbrot() {
		return *mandelbrot;
	}

	Julia &Entity::getJulia() {
		return *julia;
	}

}

#endif