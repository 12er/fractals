#ifndef _MANDELBROT_ENTITIES_MANDELBROT_CPP_
#define _MANDELBROT_ENTITIES_MANDELBROT_CPP_

/**
 * Mandelbrot class
 * (c) 2016 by Tristan Bauer
 */
#include <Mandelbrot.h>

namespace mandelbrot {

	Mandelbrot::Mandelbrot() {
		updateNecessary = true;
		precision = FRACTAL_32_BIT;
	}

	Mandelbrot::~Mandelbrot() {
	}

	void Mandelbrot::setVerticalLength(double length) {
		UniformDouble &verticalLength = Renderer::shadeX.getUniformDouble("mandelbrot.imaginaryDelta.float");
		verticalLength.value = length;
		updateNecessary = true;
	}

	double Mandelbrot::getVerticalLength() {
		UniformDouble &verticalLength = Renderer::shadeX.getUniformDouble("mandelbrot.imaginaryDelta.float");
		return verticalLength.value;
	}

	void Mandelbrot::setCenter(const DVector &center) {
		DVector &centerPosition = Renderer::shadeX.getUniformDVector("mandelbrot.imagecenter.vector");
		centerPosition = center;
		updateNecessary = true;
	}

	DVector Mandelbrot::getCenter() {
		DVector &centerPosition = Renderer::shadeX.getUniformDVector("mandelbrot.imagecenter.vector");
		return centerPosition;
	}

	void Mandelbrot::setPrecision(FractalPrecision precision) {
		this->precision = precision;
		updateNecessary = true;
	}

	void Mandelbrot::saveAsImage(const string &path) {
		Texture &mandelbrot = Renderer::shadeX.getTexture("mandelbrot.texture");
		mandelbrot.save(path);
	}

	void Mandelbrot::setUpdateNecessary() {
		updateNecessary = true;
	}

	void Mandelbrot::reshape(SXRenderArea &area) {
		Texture &mandelbrotTexture = Renderer::shadeX.getTexture("mandelbrot.texture");
		mandelbrotTexture.setWidth(area.getWidth());
		mandelbrotTexture.setHeight(area.getHeight());
		mandelbrotTexture.setPixelFormat(FLOAT_RGBA);
		mandelbrotTexture.load();
		updateNecessary = true;
	}

	void Mandelbrot::update(SXRenderArea &area) {
		if(updateNecessary) {
			double width = (float) area.getWidth();
			double height = (float) area.getHeight();
			if(width == 0 || height == 0) {
				return;
			}
			double verticalLength = getVerticalLength();
			DVector center = getCenter();
			DMatrix &mandelbrotTransform = Renderer::shadeX.getUniformDMatrix("mandelbrot.transform");
			mandelbrotTransform = 
				DMatrix().translate(center)
				* DMatrix().scale(DVector(verticalLength/2.0 * (width/height) , (verticalLength/2.0) ,1))
				;

			if(precision == FRACTAL_32_BIT) {
				Pass &mandelbrot = Renderer::shadeX.getPass("mandelbrot.pass");
				mandelbrot.render();
			} else {
				Pass &mandelbrot = Renderer::shadeX.getPass("mandelbrot64.pass");
				mandelbrot.render();
			}
			updateNecessary = false;
		}
	}

}

#endif