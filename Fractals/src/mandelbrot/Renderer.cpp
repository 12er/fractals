#ifndef _MANDELBROT_RENDERER_CPP_
#define _MANDELBROT_RENDERER_CPP_

/**
 * Fractal Landscape objects
 * (c) 2016 by Tristan Bauer
 */
#include <Mandelbrot.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
using namespace sx;

namespace mandelbrot {

	ShadeX Renderer::shadeX;

	Renderer::Renderer(UpdateArea &updateArea) {
		this->updateArea = &updateArea;
		oldShowJulia = false;
	}

	Renderer::~Renderer() {
	}

	void Renderer::refreshUpdateArea(SXRenderArea &area) {
		DMatrix &mandelbrotTransform = shadeX.getUniformDMatrix("mandelbrot.transform");
		DMatrix &juliaTransform = shadeX.getUniformDMatrix("julia.transform");
		DVector mousepos(area.getMouseX(),area.getMouseY(),0);

		if(!mandelbrotTransform.equals(oldMandelbrotTransform)) {
			DVector minPos = mandelbrotTransform * DVector(-1,-1,0);
			DVector maxPos = mandelbrotTransform * DVector(1,1,0);
			updateArea->updateMandelbrotArea(Vector(minPos),Vector(maxPos));
			updateArea->updateJuliaSet(Vector(Entity::getJulia().getC()));
		}
		if(!juliaTransform.equals(oldJuliaTransform)) {
			DVector minPos = juliaTransform * DVector(-1,-1,0);
			DVector maxPos = juliaTransform * DVector(1,1,0);
			updateArea->updateJuliaArea(Vector(minPos),Vector(maxPos));
		}
		if(!mousepos.equals(oldMousepos) || oldShowJulia != Entity::getViewer().isShowingJulia()) {
			double w = area.getWidth();
			double h = area.getHeight();
			DMatrix fractalTransform;
			if(Entity::getViewer().isShowingJulia()) {
				fractalTransform = juliaTransform;
			} else {
				fractalTransform = mandelbrotTransform;
			}
			Vector mpos = 
				fractalTransform *
				DMatrix().translate(Vector(-1,-1,0)) *
				DMatrix().scale(Vector(2.0/w, 2.0/h ,1)) *
				mousepos;
			updateArea->updateMousepos(Vector(mpos));
			updateArea->updateJuliaSet(Vector(Entity::getJulia().getC()));
		}
		
		oldMandelbrotTransform = mandelbrotTransform;
		oldJuliaTransform = juliaTransform;
		oldMousepos = mousepos;
		oldShowJulia = Entity::getViewer().isShowingJulia();
	}

	void Renderer::create(SXRenderArea &area) {
		try {
			//Load shaders, geometry, textures and other resources
			//The actual fractal computations are all happening in the shaders!
			//The shaders can be found in the configuration files mandelbrot.c and julia.c
			shadeX.addResources("../data/mandelbrot/configuration/mandelbrot.c");
			shadeX.addResources("../data/mandelbrot/configuration/julia.c");
			shadeX.addResources("../data/mandelbrot/configuration/present.c");
		} catch(Exception &e) {
			Logger::get() << Level(L_ERROR) << e.getMessage();
			area.stopRendering();
			return;
		}

		try {
			shadeX.load();
		} catch(Exception &e) {
			Logger::get() << Level(L_ERROR) << e.getMessage();
			area.stopRendering();
			return;
		}
		Entity::initEntities();
	}

	void Renderer::reshape(SXRenderArea &area) {

		RenderTarget &screen = shadeX.getRenderTarget("present.target");
		RenderTarget &offline = shadeX.getRenderTarget("present.offline.target");

		UniformFloat &width = shadeX.getUniformFloat("present.width");
		UniformFloat &height = shadeX.getUniformFloat("present.height");

		screen.setWidth(area.getWidth());
		screen.setHeight(area.getHeight());
		screen.load();
		offline.setWidth(area.getWidth());
		offline.setHeight(area.getHeight());
		offline.setRenderToDisplay(false);
		offline.load();
		width.value = area.getWidth();
		height.value = area.getHeight();

		Entity::reshapeEntities(area);
	}

	void Renderer::render(SXRenderArea &area) {
		Entity::updateEntities(area);
		refreshUpdateArea(area);
		Pass &present = shadeX.getPass("present.pass");
		present.render();
	}

	void Renderer::stop(SXRenderArea &area) {
		Entity::destroyEntities();
		shadeX.clear();
	}

}

#endif