#ifndef _MANDELBROT_GUI_MAINWIDGET_CPP_
#define _MANDELBROT_GUI_MAINWIDGET_CPP_

/**
 * Mandelbrot fractal generator
 * (c) 2016 by Tristan Bauer
 */
#include <Mandelbrot.h>
#include <QGridLayout>
#include <QDockWidget>
#include <QTabWidget>
#include <QAction>
#include <QIcon>
#include <QMenu>
#include <QMenuBar>
#include <QSplashScreen>
#include <QFileDialog>
#include <QCheckBox>
#include <QPushButton>
#include <sx/SXWidget.h>
#include <sx/Exception.h>
#include <sstream>
using namespace sx;
using namespace std;

namespace mandelbrot {

	void MainWidget::wheelRotates(float degrees) {
		Entity::getViewer().wheelRotates(degrees);
	}

	void MainWidget::updateMaxCount() {
		UniformFloat &mCount = Renderer::shadeX.getUniformFloat("maxcount.float");
		mCount.value = (float)maxCount->getValue();
		Entity::getMandelbrot().setUpdateNecessary();
		Entity::getJulia().setUpdateNecessary();
	}

	void MainWidget::use64bit(int checkboxState) {
		if(checkboxState == Qt::Checked) {
			Entity::getJulia().setPrecision(FRACTAL_64_BIT);
			Entity::getMandelbrot().setPrecision(FRACTAL_64_BIT);
		} else {
			Entity::getJulia().setPrecision(FRACTAL_32_BIT);
			Entity::getMandelbrot().setPrecision(FRACTAL_32_BIT);
		}
	}

	void MainWidget::useJulia(int checkboxState) {
		if(checkboxState == Qt::Checked) {
			Entity::getViewer().setUseJulia(true);
		} else {
			Entity::getViewer().setUseJulia(false);
		}
	}

	void MainWidget::updateMandelbrotArea(const Vector &minPos, const Vector &maxPos) {
		stringstream realStr;
		stringstream imgStr;
		realStr << "Mandelbrot real interval: [" << minPos[0] << " , " << maxPos[0] << "]";
		imgStr << "Mandelbrot imaginary interval: [" << minPos[1] << " , " << maxPos[1] << "]";
		realMandelbrotInterval->setText(tr(realStr.str().c_str()));
		imaginaryMandelbrotInterval->setText(tr(imgStr.str().c_str()));
	}

	void MainWidget::updateJuliaArea(const Vector &minPos, const Vector &maxPos) {
		stringstream realStr;
		stringstream imgStr;
		realStr << "Julia real interval: [" << minPos[0] << " , " << maxPos[0] << "]";
		imgStr << "Julia imaginary interval: [" << minPos[1] << " , " << maxPos[1] << "]";
		realJuliaInterval->setText(tr(realStr.str().c_str()));
		imaginaryJuliaInterval->setText(tr(imgStr.str().c_str()));
	}

	void MainWidget::updateJuliaSet(const Vector &c) {
		stringstream juliastr;
		juliastr << "Julia set: " << c[0] << " + i * " << c[1] << endl;
		juliaSet->setText(tr(juliastr.str().c_str()));
	}

	void MainWidget::updateMousepos(const Vector &pos) {
		stringstream mousestr;
		mousestr << "Mousepointer: " << pos[0] << " + i * " << pos[1] << endl;
		mousePos->setText(tr(mousestr.str().c_str()));
	}

	void MainWidget::resetView() {
		Entity::getJulia().setVerticalLength(2);
		Entity::getJulia().setCenter(Vector());
		Entity::getMandelbrot().setVerticalLength(2);
		Entity::getMandelbrot().setCenter(Vector());
	}

	void MainWidget::saveMandelbrotAsImage() {
		QString filename = QFileDialog::getSaveFileName(this,tr("Save mandelbrot fractal as image"),tr(""),tr("*.png *.bmp *.jpg"));
		try {
			Entity::getMandelbrot().saveAsImage(filename.toStdString());
		} catch(Exception &) {
		}
	}

	void MainWidget::saveJuliaAsImage() {
		QString filename = QFileDialog::getSaveFileName(this,tr("Save julia fractal as image"),tr(""),tr("*.png *.bmp *.jpg"));
		try {
			Entity::getJulia().saveAsImage(filename.toStdString());
		} catch(Exception &) {
		}
	}

	void MainWidget::exitApplication() {
		close();
	}

	void MainWidget::showInfo() {
		QSplashScreen *splashScreen = new QSplashScreen(QPixmap("../data/mandelbrot/textures/Splashscreen.png"));
		splashScreen->show();
	}

	MainWidget::MainWidget(): QMainWindow() {
		QWidget *central_Widget = new QWidget();
		setCentralWidget(central_Widget);
		QGridLayout *layout = new QGridLayout();
		central_Widget->setLayout(layout);

		//add renderer
		renderer = new Renderer(*this);
		SXWidget *renderWidget = new SXWidget();
		renderWidget->setMinimumSize(700,450);
		renderWidget->addRenderListener(*renderer);
		QObject::connect(renderWidget,SIGNAL(finishedRendering()),renderWidget,SLOT(close()));
		QObject::connect(renderWidget,SIGNAL(wheelRotates(float)),this,SLOT(wheelRotates(float)));
		renderWidget->renderPeriodically(0.01f);
		layout->addWidget(renderWidget,1,1);

		//add dock widget
		QDockWidget *parameters = new QDockWidget("Parameters");
		parameters->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		QTabWidget *parametersTabs = new QTabWidget();
		parametersTabs->setTabPosition(QTabWidget::East);
		parameters->setWidget(parametersTabs);
		addDockWidget(Qt::RightDockWidgetArea,parameters);

		//parameters tab
		QWidget *parametersTab_top = new QWidget();
		QGridLayout *parametersTab_intermediateLayout = new QGridLayout();
		parametersTab_top->setLayout(parametersTab_intermediateLayout);
		QWidget *parametersTabWidget = new QWidget();
		QGridLayout *parametersTab = new QGridLayout();
		parametersTabWidget->setLayout(parametersTab);
		parametersTab_intermediateLayout->addWidget(parametersTabWidget,0,0,Qt::AlignTop);
		parametersTabWidget->setLayout(parametersTab);
		parametersTabs->addTab(parametersTab_top,tr("Parameters"));

		maxCount = new IntSlider("max iterations", 200, 1, 1000);
		maxCount->setMinimumWidth(200);
		parametersTab->addWidget(maxCount,0,0);
		QObject::connect(maxCount,SIGNAL(valueChanged(int)),this,SLOT(updateMaxCount()));

		QCheckBox *use64bitbox = new QCheckBox(tr("64 bit arithmetic"),this);
		use64bitbox->setCheckState(Qt::Unchecked);
		parametersTab->addWidget(use64bitbox,1,0);
		QObject::connect(use64bitbox,SIGNAL(stateChanged(int)),this,SLOT(use64bit(int)));

		QCheckBox *useJulia = new QCheckBox(tr("Show julia fractal"),this);
		useJulia->setCheckState(Qt::Checked);
		parametersTab->addWidget(useJulia,2,0);
		QObject::connect(useJulia,SIGNAL(stateChanged(int)),this,SLOT(useJulia(int)));

		realMandelbrotInterval = new QLabel();
		parametersTab->addWidget(realMandelbrotInterval,3,0);

		imaginaryMandelbrotInterval = new QLabel();
		parametersTab->addWidget(imaginaryMandelbrotInterval,4,0);

		realJuliaInterval = new QLabel();
		parametersTab->addWidget(realJuliaInterval,5,0);

		imaginaryJuliaInterval = new QLabel();
		parametersTab->addWidget(imaginaryJuliaInterval,6,0);

		juliaSet = new QLabel();
		parametersTab->addWidget(juliaSet,7,0);

		mousePos = new QLabel();
		parametersTab->addWidget(mousePos,8,0);

		QPushButton *resetViewButton = new QPushButton(tr("Reset zoom"));
		resetViewButton->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		parametersTab->addWidget(resetViewButton,9,0);
		QObject::connect(resetViewButton,SIGNAL(clicked()),this,SLOT(resetView()));

		//actions
		QAction *saveMandelbrotAction = new QAction(QIcon(tr("../data/shared/icons/Save.png")),tr("Save mandelbrot fractal"),this);
		saveMandelbrotAction->setStatusTip(tr("Save mandelbrot fractal"));
		QObject::connect(saveMandelbrotAction,SIGNAL(triggered()),this,SLOT(saveMandelbrotAsImage()));

		QAction *saveJuliaAction = new QAction(QIcon(tr("../data/shared/icons/Save.png")),tr("Save julia fractal"),this);
		saveJuliaAction->setStatusTip(tr("Save julia fractal"));
		QObject::connect(saveJuliaAction,SIGNAL(triggered()),this,SLOT(saveJuliaAsImage()));

		QAction *exitAction = new QAction(tr("Exit"),this);
		exitAction->setStatusTip(tr("Quit application"));
		QObject::connect(exitAction,SIGNAL(triggered()),this,SLOT(exitApplication()));

		QAction *infoAction = new QAction(QIcon(tr("../data/mandelbrot/icons/Mandelbrot.png")),tr("About mandelbrot fractal generator"),this);
		infoAction->setStatusTip(tr("Show application info"));
		QObject::connect(infoAction,SIGNAL(triggered()),this,SLOT(showInfo()));

		//file menu
		QMenu *fileMenu = menuBar()->addMenu("File");
		fileMenu->addAction(saveMandelbrotAction);
		fileMenu->addAction(saveJuliaAction);
		fileMenu->addAction(exitAction);

		//window menu
		QMenu* windowMenu = createPopupMenu();
        windowMenu->setTitle("Window");
        menuBar()->addMenu(windowMenu);

		//help menu
		QMenu *helpMenu = menuBar()->addMenu("Help");
		helpMenu->addAction(infoAction);

	}

	MainWidget::~MainWidget() {
	}

}

#endif