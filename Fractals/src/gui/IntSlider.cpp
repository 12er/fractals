#ifndef _GUI_INTSLIDER_CPP_
#define _GUI_INTSLIDER_CPP_

/**
 * Integer Input with a Slider
 * and a text input
 * (c) 2016 by Tristan Bauer
 */
#include <FractalGui.h>
#include <QGridLayout>
#include <QLabel>

namespace fractals {

	void IntSlider::updateSlider() {
		if(!slider->hasFocus()) {
			int value = text->value();
			slider->setSliderPosition(value);
		}
	}

	void IntSlider::updateText() {
		if(!text->hasFocus()) {
			double position = slider->sliderPosition();
			text->setValue(position);
		}
		valueChanged(text->value());
	}

	IntSlider::IntSlider(const string &label, int value, int minVal, int maxVal, QWidget *parent, Qt::WindowFlags f): QWidget(parent,f) {
		QGridLayout *layout = new QGridLayout();
		setLayout(layout);

		QLabel *lbl = new QLabel(QString(label.c_str()));
		layout->addWidget(lbl,0,0);

		slider = new QSlider(Qt::Horizontal,this);
		slider->setRange(minVal,maxVal);
		slider->setSliderPosition(value);
		layout->addWidget(slider,0,1);

		text = new QSpinBox(this);
		text->setRange(minVal,maxVal);
		text->setValue(value);
		layout->addWidget(text,0,2);

		QObject::connect(slider,SIGNAL(valueChanged(int)),this,SLOT(updateText()));
		QObject::connect(text,SIGNAL(valueChanged(int)),this,SLOT(updateSlider()));
	}

	IntSlider::~IntSlider() {
	}

	void IntSlider::setValue(int value) {
		slider->setSliderPosition(value);
		text->setValue(value);
	}

	int IntSlider::getValue() const {
		return text->value();
	}

	void IntSlider::setMinimum(int minVal) {
		slider->setMinimum(minVal);
		text->setMinimum(minVal);
	}

	int IntSlider::getMinimum() const {
		return text->minimum();
	}

	void IntSlider::setMaximum(int maxVal) {
		slider->setMaximum(maxVal);
		text->setMaximum(maxVal);
	}

	int IntSlider::getMaximum() const {
		return text->maximum();
	}

	void IntSlider::setRange(int minVal, int maxVal) {
		slider->setRange(minVal,maxVal);
		text->setRange(minVal,maxVal);
	}

}

#endif