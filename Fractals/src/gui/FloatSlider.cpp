#ifndef _GUI_FLOATSLIDER_CPP_
#define _GUI_FLOATSLIDER_CPP_

/**
 * Float Input with a Slider
 * and a text input
 * (c) 2016 by Tristan Bauer
 */
#include <FractalGui.h>
#include <QGridLayout>
#include <QLabel>

namespace fractals {

	void FloatSlider::updateSlider() {
		if(!slider->hasFocus()) {
			double minVal = text->minimum();
			double maxVal = text->maximum();
			double value = text->value();
			double percentage = (value-minVal)/(maxVal-minVal);
			slider->setSliderPosition((int)(percentage*4000));
		}
	}

	void FloatSlider::updateText() {
		if(!text->hasFocus()) {
			double minVal = text->minimum();
			double maxVal = text->maximum();
			double position = slider->sliderPosition();
			double percentage = position/4000.0;
			double value = (maxVal-minVal)*percentage + minVal;
			text->setValue(value);
		}
		valueChanged(text->value());
	}

	FloatSlider::FloatSlider(const string &label, double value, double minVal, double maxVal, QWidget *parent, Qt::WindowFlags f): QWidget(parent,f) {
		QGridLayout *layout = new QGridLayout();
		setLayout(layout);

		QLabel *lbl = new QLabel(QString(label.c_str()));
		layout->addWidget(lbl,0,0);

		slider = new QSlider(Qt::Horizontal,this);
		double percentage = (value-minVal)/(maxVal-minVal);
		slider->setRange(0,4000);
		slider->setSliderPosition((int)(percentage*4000));
		layout->addWidget(slider,0,1);

		text = new QDoubleSpinBox(this);
		text->setDecimals(5);
		text->setRange(minVal,maxVal);
		text->setValue(value);
		layout->addWidget(text,0,2);

		QObject::connect(slider,SIGNAL(valueChanged(int)),this,SLOT(updateText()));
		QObject::connect(text,SIGNAL(valueChanged(double)),this,SLOT(updateSlider()));
	}

	FloatSlider::~FloatSlider() {
	}

	void FloatSlider::setValue(double value) {
		double minVal = text->minimum();
		double maxVal = text->maximum();
		double percentage = (value-minVal)/(maxVal-minVal);
		slider->setSliderPosition((int)(percentage*4000));
		text->setValue(value);
	}

	double FloatSlider::getValue() const {
		return text->value();
	}

	void FloatSlider::setMinimum(double minVal) {
		double maxVal = text->maximum();
		double percentage = (text->value()-minVal)/(maxVal-minVal);
		slider->setSliderPosition((int)(percentage*4000));
		text->setMinimum(minVal);
	}

	double FloatSlider::getMinimum() const {
		return text->minimum();
	}

	void FloatSlider::setMaximum(double maxVal) {
		double minVal = text->minimum();
		double percentage = (text->value()-minVal)/(maxVal-minVal);
		slider->setSliderPosition((int)(percentage*4000));
		text->setMaximum(maxVal);
	}

	double FloatSlider::getMaximum() const {
		return text->maximum();
	}

	void FloatSlider::setRange(double minVal, double maxVal) {
		double percentage = (text->value()-minVal)/(maxVal-minVal);
		slider->setSliderPosition((int)(percentage*4000));
		text->setRange(minVal,maxVal);
	}

}

#endif