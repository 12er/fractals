#ifndef _GUI_WARNINGWIDGET_CPP_
#define _GUI_WARNINGWIDGET_CPP_

/**
 * Warning Widget
 * (c) 2016 by Tristan Bauer
 */
#include <FractalGui.h>
#include <QPushButton>
#include <QGridLayout>

namespace fractals {

	WarningWidget::WarningWidget(const QString &title, const QString &text, QWidget *w): QDialog() {
		setWindowTitle(title);
		if(w != 0) {
			setWindowIcon(w->windowIcon());
		}
		QLabel *warningIcon = new QLabel();
		warningIcon->setPixmap(QPixmap("../data/shared/icons/Warning.png"));
		QLabel *messageLabel = new QLabel(text);
		QPushButton *okButton = new QPushButton("OK");
		okButton->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		connect(okButton,SIGNAL(clicked()),this,SLOT(close()));
		QGridLayout *layout = new QGridLayout();
		layout->addWidget(warningIcon,0,0);
		layout->addWidget(messageLabel,0,1);
		layout->addWidget(okButton,1,0,1,2);
		setLayout(layout);
		layout->setSizeConstraint(QLayout::SetFixedSize);
	}

}

#endif