#ifndef _LANDSCAPE_ENTITIES_SKY_CPP_
#define _LANDSCAPE_ENTITIES_SKY_CPP_

/**
 * Sky entity
 * (c) 2016 by Tristan Bauer
 */
#include <Landscape.h>

namespace landscape {

	Sky::Sky() {
		position = &Renderer::shadeX.getUniformVector("viewer.position.vector");
		reflectedposition = &Renderer::shadeX.getUniformVector("reflected.position.vector");
		modelMatrix = &Renderer::shadeX.getUniformMatrix("sky.position.matrix");
		reflectedmodelMatrix = &Renderer::shadeX.getUniformMatrix("sky.reflectedposition.matrix");
	}

	Sky::~Sky() {
	}

	void Sky::update(SXRenderArea &area) {
		*modelMatrix = Matrix().translate(*position) * Matrix().scale(Vector(1400,1400,1400));
		*reflectedmodelMatrix = Matrix().translate(*reflectedposition) * Matrix().scale(Vector(1400,1400,1400));
	}

}

#endif