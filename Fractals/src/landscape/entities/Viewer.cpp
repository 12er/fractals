#ifndef _LANDSCAPE_ENTITIES_VIEWER_CPP_
#define _LANDSCAPE_ENTITIES_VIEWER_CPP_

/**
 * Viewer entity
 * (c) 2016 by Tristan Bauer
 */
#include <Landscape.h>

namespace landscape {

	Viewer::Viewer() {
		width = &Renderer::shadeX.getUniformFloat("present.width");
		height = &Renderer::shadeX.getUniformFloat("present.height");
		position = &Renderer::shadeX.getUniformVector("viewer.position.vector");
		view = &Renderer::shadeX.getUniformVector("viewer.view.vector");
		up = &Renderer::shadeX.getUniformVector("viewer.up.vector");
		viewMatrix = &Renderer::shadeX.getUniformMatrix("viewer.view.matrix");
		projectionMatrix = &Renderer::shadeX.getUniformMatrix("viewer.projection.matrix");
		input = VIEWER_ROTATE;
		mouseGrabbed = false;
		painting = false;
		distanceChange = 0;
	}

	Viewer::~Viewer() {
	}

	void Viewer::setViewerInput(ViewerInput input) {
		this->input = input;
		this->distanceChange = 0;
	}

	void Viewer::setMouseGrabbed(bool grabbed) {
		this->mouseGrabbed = grabbed;
	}

	void Viewer::setPainting(bool painting) {
		this->painting = painting;
	}

	void Viewer::changeDistance(float delta) {
		this->distanceChange += delta;
	}

	void Viewer::processInput(SXRenderArea &area) {
		float dt = (float)area.getDeltaTime();
		if(input == VIEWER_ROTATE) {
			Vector rotViewerDir;
			float radiusDisplacement = distanceChange;
			distanceChange = 0;
			if( (area.hasMouseKey(MOUSE_LEFT) && !painting) ||area.hasMouseKey(MOUSE_RIGHT)) {
				area.setMousePointer(oldMousePosition[0],oldMousePosition[1]);
				rotViewerDir = Vector(
					(float)area.getMouseDeltaX(),
					(float)area.getMouseDeltaY()
					);
				rotViewerDir.scalarmult(dt);
			} else if(area.hasMouseKey(MOUSE_MIDDLE)) {
				area.setMousePointer(oldMousePosition[0],oldMousePosition[1]);
				radiusDisplacement += dt * 10.0f * (float)area.getMouseDeltaY();
			} else {
				oldMousePosition = Vector(area.getMouseX(),area.getMouseY());
			}
			Vector v = *position * -1.0f;
			v.normalize();
			Vector w = v;
			w.crossmult(Vector(0,0,-1));
			w.normalize();
			Matrix verticalRotation = Matrix().rotate(w,rotViewerDir[1]);
			Matrix horicontalRotation = Matrix().rotate(Vector(0,0,-1),rotViewerDir[0]);

			*position = *position + v * radiusDisplacement;
			*position = horicontalRotation * verticalRotation * *position;
			*view = *position * -1.0f;
			view->normalize();
		} else {
			Vector rotViewerDir;
			if(mouseGrabbed || (area.hasMouseKey(MOUSE_LEFT) && !painting) || area.hasMouseKey(MOUSE_RIGHT)) {
				area.setShowCursor(false);
				area.setMousePointer(oldMousePosition[0],oldMousePosition[1]);
				rotViewerDir = Vector(
					(float)area.getMouseDeltaX(),
					(float)area.getMouseDeltaY()
					);
				rotViewerDir.scalarmult(dt);
			} else {
				area.setShowCursor(true);
				oldMousePosition = Vector(area.getMouseX(),area.getMouseY());
			}
			Vector vRight = *view % *up;

			float forward = 0;
			float right = 0;
			float speed = 100.0f;
			if(area.hasKey('W') && !area.hasKey('S')) {
				//move forward
				forward = (float)area.getDeltaTime();
			} else if(!area.hasKey('W') && area.hasKey('S')) {
				//move backward
				forward = (float)-area.getDeltaTime();
			}
			if(area.hasKey('D') && !area.hasKey('A')) {
				//move right
				right = (float)area.getDeltaTime();
				
			} else if(!area.hasKey('D') && area.hasKey('A')) {
				//move left
				right = (float)-area.getDeltaTime();
			}

			*position = *position + view->normalize() * forward * speed + vRight.normalize() * right * speed;
			*view = view->normalize() + vRight * rotViewerDir[0] + up->normalize() * rotViewerDir[1];
		}
	}

	void Viewer::update(SXRenderArea &area) {
		processInput(area);

		*viewMatrix = Matrix().viewMatrix(*position,*view,*up);
		*projectionMatrix = Matrix().perspectiveMatrix((float)Pi*0.5f , width->value , height->value , 1.0f , 1500.0f);
	}

}

#endif