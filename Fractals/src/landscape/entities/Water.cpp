#ifndef _LANDSCAPE_ENTITIES_WATER_CPP_
#define _LANDSCAPE_ENTITIES_WATER_CPP_

/**
 * Water entity
 * (c) 2016 by Tristan Bauer
 */
#include <Landscape.h>

namespace landscape {

	Water::Water() {
		waterheight = &Renderer::shadeX.getUniformFloat("present.waterheight");
		abovewater = &Renderer::shadeX.getUniformFloat("present.abovewater");
		wave1 = &Renderer::shadeX.getUniformVector("wave1.vector");
		wave2 = &Renderer::shadeX.getUniformVector("wave2.vector");
		position = &Renderer::shadeX.getUniformVector("viewer.position.vector");
		reflectedPosition = &Renderer::shadeX.getUniformVector("reflected.position.vector");
		view = &Renderer::shadeX.getUniformVector("viewer.view.vector");
		up = &Renderer::shadeX.getUniformVector("viewer.up.vector");
		modelMatrix = &Renderer::shadeX.getUniformMatrix("water.transform");
		viewMatrix = &Renderer::shadeX.getUniformMatrix("reflected.view.matrix");
	}

	Water::~Water() {
	}
	
	void Water::update(SXRenderArea &area) {
		//assign 1 to abovewater, if the viewer is
		//above the water, else assign 0 to abovewater
		if((*position)[2] > waterheight->value) {
			abovewater->value = 1;
		} else {
			abovewater->value = 0;
		}

		//reflect both up and view vector on
		//the water surface
		*reflectedPosition = *position;
		Vector reflectedView = *view;
		Vector reflectedUp = *up;
		(*reflectedPosition)[2] = 2 * waterheight->value - (*reflectedPosition)[2];
		reflectedView[2] = -reflectedView[2];
		reflectedUp[2] = -reflectedUp[2];

		//calculate position, snap vertices to certain positions
		//the water grid is a regular grid exactly fitting into the
		//unit square of the x-y plane having 21 vertex locations along both axes
		float delta = 140;
		Vector snappedPosition;
		snappedPosition[0] = floor( ((*position)[0] + (delta/2.0f))/delta );
		snappedPosition[1] = floor( ((*position)[1] + (delta/2.0f))/delta );
		snappedPosition.scalarmult(delta);
		snappedPosition[2] = waterheight->value;

		//move waves
		*wave1 = Vector(0.1f,0.2f) * (float)area.getTime();
		*wave2 = Vector(-0.3f,0.05f) * (float)area.getTime();

		//calculate model and reflected view matrix
		*modelMatrix = Matrix().translate(snappedPosition) * Matrix().scale(Vector(1400,1400,1));
		*viewMatrix = Matrix().viewMatrix(*reflectedPosition,reflectedView,reflectedUp);
	}

}

#endif