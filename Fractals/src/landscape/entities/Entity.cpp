#ifndef _LANDSCAPE_ENTITIES_ENTITY_CPP_
#define _LANDSCAPE_ENTITIES_ENTITY_CPP_

/**
 * Entity class
 * (c) 2016 by Tristan Bauer
 */
#include <Landscape.h>

namespace landscape {

	vector<Entity *> Entity::entities;
	Terrain *Entity::terrain = 0;
	Viewer *Entity::viewer = 0;

	void Entity::initEntities() {
		viewer = new Viewer();
		entities.push_back(viewer);

		entities.push_back(new Water());

		entities.push_back(new Sky());
	
		terrain = new Terrain();
		terrain->resetVarianceMap();
		entities.push_back(terrain);
	}

	Terrain &Entity::getTerrain() {
		return *terrain;
	}

	Viewer &Entity::getViewer() {
		return *viewer;
	}

	void Entity::updateEntities(SXRenderArea &area) {
		for(vector<Entity *>::iterator iter = entities.begin() ; iter != entities.end() ; iter++) {
			Entity *entity = (*iter);
			entity->update(area);
		}
	}

	void Entity::destroyEntities() {
		for(vector<Entity *>::iterator iter = entities.begin() ; iter != entities.end() ; iter++) {
			delete (*iter);
		}
		entities.clear();
	}

}

#endif