#ifndef _LANDSCAPE_ENTITIES_TERRAIN_CPP_
#define _LANDSCAPE_ENTITIES_TERRAIN_CPP_

/**
 * Terrain entity
 * (c) 2016 by Tristan Bauer
 */
#include <Landscape.h>
#include <sx/Exception.h>
#include <boost/tokenizer.hpp>
#include <sstream>
#include <fstream>
#include <algorithm>
using namespace boost;
using namespace std;

namespace landscape {

	Terrain::Terrain() {
		stepcount = 0;
		maxStepcount = 22;

		//fill red channel of texture with normally distributed
		//random numbers
		Texture &gaussiandistribution = Renderer::shadeX.getTexture("gaussiandistribution.texture");
		unsigned int width = (unsigned int)gaussiandistribution.getWidth();
		unsigned int height = (unsigned int)gaussiandistribution.getHeight();
		vector<float> gaussianValues;
		gaussianValues.reserve(width*height*4);
		for(unsigned int i=0 ; i<width*height ; i++) {
			float X = GaussianDistribution::X();
			float temp = X;
			if(X != X || (temp == X && (temp-X) != 0)) {
				//infinity or not a number
				//replace X by zero
				X = 0;
			}
			gaussianValues.push_back(X);
			gaussianValues.push_back(0);
			gaussianValues.push_back(0);
			gaussianValues.push_back(1);
		}
		gaussiandistribution.setWidth(width);
		gaussiandistribution.setHeight(height);
		gaussiandistribution.setFloatBuffer(gaussianValues);
		gaussiandistribution.load();

		//init variance map
		Pass &initVarianceMap = Renderer::shadeX.getPass("terrain.initvariancemap.pass");
		initVarianceMap.render();

		//load initial terrain into mesh1
		restart();
	}

	Terrain::~Terrain() {
	}

	void Terrain::saveAsImage(const string &filename) {
		//inspect filename
		//create filenames for the normalmap
		//and a config xml file
		string extension;
		string localname;
		char_separator<char> pseparator("./\\");
		tokenizer<char_separator<char>> ptokens(filename,pseparator);
		for(tokenizer<char_separator<char>>::iterator iter = ptokens.begin() ; iter != ptokens.end() ; iter++) {
			localname = extension;
			extension = (*iter);
		}
		if(extension.size() + 1 >= filename.size() || localname.size() == 0) {
			//filename must at least contain a fileextension and a name
			throw Exception("Error: invalid filename");
		}
		if(filename.substr(filename.size()-1-extension.size(),1).compare(".") != 0) {
			//filename must have character . before the extension
			throw Exception("Error: invalid filename");
		}
		stringstream normalmapStream;
		stringstream configStream;
		string name = filename.substr(0,filename.size()-1-extension.size());
		normalmapStream << name << "_normalmap.png";
		configStream << name << ".xml";
		string normalmapName = normalmapStream.str();
		string configName = configStream.str();

		Texture &heightvaluesTexture = Renderer::shadeX.getTexture("terrain.heightvalues.texture");
		Texture &heightmap = Renderer::shadeX.getTexture("saveterrain.heightmap.texture");
		Texture &normalmap = Renderer::shadeX.getTexture("saveterrain.normalmap.texture");
		UniformFloat &maxheight = Renderer::shadeX.getUniformFloat("terrain.maximumheight");
		UniformFloat &waterheight = Renderer::shadeX.getUniformFloat("present.waterheight");
		Pass &saveterrain = Renderer::shadeX.getPass("saveterrain.pass");

		//set dimensions of output textures
		heightmap.setWidth(heightvaluesTexture.getWidth());
		heightmap.setHeight(heightvaluesTexture.getHeight());
		heightmap.setPixelFormat(FLOAT_RGBA);
		heightmap.load();
		normalmap.setWidth(heightvaluesTexture.getWidth());
		normalmap.setHeight(heightvaluesTexture.getHeight());
		normalmap.setPixelFormat(FLOAT_RGBA);
		normalmap.load();

		//transform heightmap and normalmap to a format suitable for saving them
		saveterrain.render();

		//save some data about the mesh in a config file
		ofstream file;
		try {
			file.open(configName.c_str());

			file << 
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
				"<terrain>\n"
				"	<minheight>0</minheight>\n"
				"	<maxheight>" << maxheight.value << "</maxheight>\n"
				"	<waterheight>" << waterheight.value << "</waterheight>\n"
				"	<minposition>\n"
				"		<x>-400</x>\n"
				"		<y>-400</y>\n"
				"	</minposition>\n"
				"	<maxposition>\n"
				"		<x>400</x>\n"
				"		<y>400</y>\n"
				"	</maxposition>\n"
				"	<bitmaps>\n"
				"		<heightmap>./" << localname << "." << extension << "</heightmap>\n"
				"		<normalmap>./" << localname << "_normalmap.png</normalmap>\n"
				"		<width>" << heightmap.getWidth() << "</width>\n"
				"		<height>" << heightmap.getHeight() << "</height>\n"
				"	</bitmaps>\n"
				"</terrain>";

				file.close();
		} catch(...) {
			try {
				file.close();
			} catch(...) {
			}
			stringstream errmsg;
			errmsg << "Error: writing configuration of terrain into file " << configName << " didn't work";
			throw Exception(errmsg.str(),EX_IO);
		}

		//now save the heightmap and the normalmap
		heightmap.save(filename);
		normalmap.save(normalmapName);
	}

	void Terrain::saveAsMesh(const string &filename) {
		//inspect filename
		//create filenames for the normalmap
		//and a config xml file
		string extension;
		string localname;
		char_separator<char> pseparator("./\\");
		tokenizer<char_separator<char>> ptokens(filename,pseparator);
		for(tokenizer<char_separator<char>>::iterator iter = ptokens.begin() ; iter != ptokens.end() ; iter++) {
			localname = extension;
			extension = (*iter);
		}
		if(extension.size() + 1 >= filename.size() || localname.size() == 0) {
			//filename must at least contain a fileextension and a name
			throw Exception("Error: invalid filename");
		}
		if(filename.substr(filename.size()-1-extension.size(),1).compare(".") != 0) {
			//filename must have character . before the extension
			throw Exception("Error: invalid filename");
		}

		BufferedMesh *terrainMesh = 0;
		if(updatecount % 2 == 0) {
			terrainMesh = &Renderer::shadeX.getBufferedMesh("terrain.mesh2");
		} else {
			terrainMesh = &Renderer::shadeX.getBufferedMesh("terrain.mesh1");
		}
		terrainMesh->save(filename);
	}

	void Terrain::loadFromImage(const string &heightmapPath, const string &blendmapPath, bool useBlendmap) {
		Texture &heightmap = Renderer::shadeX.getTexture("heightpaint.heightmap.texture");
		Texture &blendmap = Renderer::shadeX.getTexture("heightpaint.blendmap.texture");
		heightmap.setPath(heightmapPath);
		heightmap.load();
		if(!heightmap.isLoaded()) {
			throw Exception("Error: could not load heightmap",EX_INIT);
		}
		if(useBlendmap) {
			blendmap.setPath(blendmapPath);
			blendmap.load();
			if(!blendmap.isLoaded()) {
				throw Exception("Error: could not load blendmap",EX_INIT);
			}
		}

		//adjust the worldspace to texturespace transformations
		//and the useblendmap variable
		Texture &heightvalues = Renderer::shadeX.getTexture("terrain.heightvalues.texture");
		Matrix &heightvaluesTexcoord = Renderer::shadeX.getUniformMatrix("terrain.heightvalues.texcoord");
		Matrix &heightmapTexcoord = Renderer::shadeX.getUniformMatrix("heightpaint.heightmap.texcoord");
		Matrix &blendmapTexcoord = Renderer::shadeX.getUniformMatrix("heightpaint.blendmap.texcoord");
		UniformFloat &useblendmapUniform = Renderer::shadeX.getUniformFloat("heightpaint.useblendmap.float");

		if(heightmap.getWidth() == heightmap.getHeight() && heightmap.getWidth() == heightvalues.getWidth()) {
			//the width and height of the heightmap
			//exactly match the number of locations along the x/y axes,
			//where vertices are located.
			//hence a transformation can be specified
			//which puts each pixel exactly over a vertex
			heightmapTexcoord = heightvaluesTexcoord;
		} else {
			heightmapTexcoord = Matrix().translate(Vector(0.5f,0.5f,0))
				* Matrix().scale(Vector(0.5f,0.5f,1))
				* Matrix().scale(Vector(1/400.0f,1/400.0f,1));
		}

		if(useBlendmap && blendmap.getWidth() == blendmap.getHeight() && blendmap.getWidth() == heightvalues.getWidth()) {
			//the width and height of the blendmap
			//exactly match the number of locations along the x/y axes,
			//where vertices are located.
			//hence a transformation can be specified
			//which puts each pixel exactly over a vertex
			blendmapTexcoord = heightvaluesTexcoord;
		} else if(useBlendmap) {
			blendmapTexcoord = Matrix().translate(Vector(0.5f,0.5f,0))
				* Matrix().scale(Vector(0.5f,0.5f,1))
				* Matrix().scale(Vector(1/400.0f,1/400.0f,1));
		}

		if(useBlendmap) {
			useblendmapUniform.value = 1;
		} else {
			useblendmapUniform.value = 0;
		}

		//now perform the actual dislocation of the terrain vertices
		if(updatecount % 2 == 0) {
			//render geometry from mesh1 into mesh2, if stepcount is even
			Pass &mesh1_to_mesh2 = Renderer::shadeX.getPass("heightpaint.applyheightmap1_2.pass");
			mesh1_to_mesh2.render();

			RenderObject &obj1 = Renderer::shadeX.getRenderObject("terrain.object1");
			RenderObject &obj2 = Renderer::shadeX.getRenderObject("terrain.object2");
			obj1.setVisible(false);
			obj2.setVisible(true);
		} else {
			Pass &mesh2_to_mesh1 = Renderer::shadeX.getPass("heightpaint.applyheightmap2_1.pass");
			mesh2_to_mesh1.render();

			RenderObject &obj1 = Renderer::shadeX.getRenderObject("terrain.object1");
			RenderObject &obj2 = Renderer::shadeX.getRenderObject("terrain.object2");
			obj1.setVisible(true);
			obj2.setVisible(false);
		}
		//the heightvalues are rendered into the heightvaluesTexture
		Effect &heightvaluesEffect = Renderer::shadeX.getEffect("terrain.heightvalues.effect");
		heightvaluesEffect.render();

		updatecount++;

	}

	void Terrain::loadFromImage(const string &heightmapPath) {
		loadFromImage(heightmapPath,"",false);
	}

	void Terrain::loadFromImage(const string &heightmapPath, const string &blendmapPath) {
		loadFromImage(heightmapPath,blendmapPath,true);
	}

	void Terrain::restart() {
		stepcount = 0;
		updatecount = 0;

		Pass &resetPass = Renderer::shadeX.getPass("terrain.reset.pass");
		resetPass.render();

		RenderObject &obj1 = Renderer::shadeX.getRenderObject("terrain.object1");
		RenderObject &obj2 = Renderer::shadeX.getRenderObject("terrain.object2");
		obj1.setVisible(true);
		obj2.setVisible(false);

		//reset heightmap
		Texture &heightvaluesTexture = Renderer::shadeX.getTexture("terrain.heightvalues.texture");
		Texture &normalmapTexture = Renderer::shadeX.getTexture("terrain.normalmap.texture");
		Texture &normalmapTempbuffer = Renderer::shadeX.getTexture("normalmap.tempbuffer.texture");
		UniformFloat &pixelcount = Renderer::shadeX.getUniformFloat("normalmap.pixelcount");
		Texture &tempTexture = Renderer::shadeX.getTexture("terrain.tempbuffer.texture");
		RenderTarget &heightvaluesTarget = Renderer::shadeX.getRenderTarget("terrain.heightvalues.target");
		RenderTarget &normalmapTarget = Renderer::shadeX.getRenderTarget("terrain.normalmap.target");
		Matrix &heightvaluesTransform = Renderer::shadeX.getUniformMatrix("terrain.heightvalues.transform");
		Matrix &heightvaluesTexcoord = Renderer::shadeX.getUniformMatrix("terrain.heightvalues.texcoord");
		Matrix &normalmapTransform = Renderer::shadeX.getUniformMatrix("normalmap.transform");
		Matrix &normalmapTexcoord = Renderer::shadeX.getUniformMatrix("normalmap.texcoord");
		Effect &heightvaluesEffect = Renderer::shadeX.getEffect("terrain.heightvalues.effect");
		heightvaluesTexture.setWidth(2);
		heightvaluesTexture.setHeight(2);
		heightvaluesTexture.setPixelFormat(FLOAT_RGBA);
		heightvaluesTexture.load();
		normalmapTexture.setWidth(257);
		normalmapTexture.setHeight(257);
		normalmapTexture.setPixelFormat(FLOAT_RGBA);
		normalmapTexture.load();
		normalmapTempbuffer.setWidth(257);
		normalmapTempbuffer.setHeight(257);
		normalmapTempbuffer.setPixelFormat(FLOAT_RGBA);
		normalmapTempbuffer.load();
		pixelcount.value = 257;
		tempTexture.setWidth(2);
		tempTexture.setHeight(2);
		tempTexture.setPixelFormat(FLOAT_RGBA);
		tempTexture.load();
		heightvaluesTarget.setWidth(2);
		heightvaluesTarget.setHeight(2);
		heightvaluesTarget.setRenderToDisplay(false);
		heightvaluesTarget.load();
		normalmapTarget.setWidth(257);
		normalmapTarget.setHeight(257);
		normalmapTarget.setRenderToDisplay(false);
		normalmapTarget.load();
		heightvaluesTransform = 
			Matrix().scale(Vector(1.0f/2.0f,1.0f/2.0f,1)) *
			Matrix().scale(Vector(0.0025f,0.0025f,1))
			;
		heightvaluesTexcoord =
			Matrix().translate(Vector(0.5f,0.5f,0)) * Matrix().scale(Vector(0.5f,0.5f,1)) *
			Matrix().scale(Vector(1.0f/2.0f,1.0f/2.0f,1)) *
			Matrix().scale(Vector(0.0025f,0.0025f,1))
			;
		normalmapTransform = 
			Matrix().scale(Vector(256.0f/257.0f,256.0f/257.0f,1)) *
			Matrix().scale(Vector(0.0025f,0.0025f,1))
			;
		normalmapTexcoord = 
			Matrix().translate(Vector(0.5f,0.5f,0)) * Matrix().scale(Vector(0.5f,0.5f,1)) *
			Matrix().scale(Vector(256.0f/257.0f,256.0f/257.0f,1)) *
			Matrix().scale(Vector(0.0025f,0.0025f,1))
			;
		//the heightvalues are rendered into the heightvaluesTexture
		heightvaluesEffect.render();
	}

	void Terrain::clampHeight() {
		//one mesh serves as a source, the corrected geometry is stored in the
		//other mesh
		//each time the meshes are updated, they exchange they change roles
		if(updatecount % 2 == 0) {
			//render geometry from mesh1 into mesh2, if stepcount is even
			Pass &mesh1_to_mesh2 = Renderer::shadeX.getPass("terrain.clampheight.mesh1_to_mesh2");
			mesh1_to_mesh2.render();

			RenderObject &obj1 = Renderer::shadeX.getRenderObject("terrain.object1");
			RenderObject &obj2 = Renderer::shadeX.getRenderObject("terrain.object2");
			obj1.setVisible(false);
			obj2.setVisible(true);
		} else {
			//render geometry from mesh2 into mesh1, if stepcount is odd
			Pass &mesh2_to_mesh1 = Renderer::shadeX.getPass("terrain.clampheight.mesh2_to_mesh1");
			mesh2_to_mesh1.render();

			RenderObject &obj1 = Renderer::shadeX.getRenderObject("terrain.object1");
			RenderObject &obj2 = Renderer::shadeX.getRenderObject("terrain.object2");
			obj1.setVisible(true);
			obj2.setVisible(false);
		}
		
		//the heightvalues are rendered into the heightvaluesTexture
		Effect &heightvaluesEffect = Renderer::shadeX.getEffect("terrain.heightvalues.effect");
		heightvaluesEffect.render();

		updatecount++;
	}

	void Terrain::subdivide() {
		if(stepcount >= maxStepcount) {
			stringstream msg;
			msg << "subdivision is limited to " << maxStepcount << " steps";
			throw Exception(msg.str());
		}

		//calculate the width of the terrain before (oldWidth)
		//and after (newWidth) the subdivision 
		//the width of the terrain describes the number of locations along
		//the x axis, at which vertices are located
		float oldWidth = 2;
		float newWidth = 2;
		int oldHalfCount = -1;
		if(stepcount > 0) {
			oldHalfCount = (stepcount - 1) / 2;
		}
		for(int i=0 ; i<=oldHalfCount ; i++) {
			oldWidth = oldWidth*2 - 1;
		}
		unsigned int newHalfCount = stepcount / 2;
		for(unsigned int i=0 ; i<=newHalfCount ; i++) {
			newWidth = newWidth*2 - 1;
		}

		//first of all it is necessary to render the grid
		//heightvalues into a texture
		//the heightvalues of this texture will be used by the geometry shader to
		//read the heights of all four vertices of each quad to perform midpoint displacement,
		//as the mesh is made of triangles forming quads
		//first of all a transformation of the mesh into texture space must be established
		Texture &heightvaluesTexture = Renderer::shadeX.getTexture("terrain.heightvalues.texture");
		Texture &normalmapTexture = Renderer::shadeX.getTexture("terrain.normalmap.texture");
		Texture &normalmapTempbuffer = Renderer::shadeX.getTexture("normalmap.tempbuffer.texture");
		UniformFloat &pixelcount = Renderer::shadeX.getUniformFloat("normalmap.pixelcount");
		Texture &tempTexture = Renderer::shadeX.getTexture("terrain.tempbuffer.texture");
		RenderTarget &heightvaluesTarget = Renderer::shadeX.getRenderTarget("terrain.heightvalues.target");
		RenderTarget &normalmapTarget = Renderer::shadeX.getRenderTarget("terrain.normalmap.target");
		Matrix &heightvaluesTransform = Renderer::shadeX.getUniformMatrix("terrain.heightvalues.transform");
		Matrix &heightvaluesTexcoord = Renderer::shadeX.getUniformMatrix("terrain.heightvalues.texcoord");
		Matrix &normalmapTransform = Renderer::shadeX.getUniformMatrix("normalmap.transform");
		Matrix &normalmapTexcoord = Renderer::shadeX.getUniformMatrix("normalmap.texcoord");
		Effect &heightvaluesEffect = Renderer::shadeX.getEffect("terrain.heightvalues.effect");
		if(stepcount == 0) {
			//only for the first step it's necessary to update
			//the heightvalues before the subdivision
			//then it will be done after the subdivision
			heightvaluesTexture.setWidth(oldWidth);
			heightvaluesTexture.setHeight(oldWidth);
			heightvaluesTexture.setPixelFormat(FLOAT_RGBA);
			heightvaluesTexture.load();
			normalmapTexture.setWidth(max(oldWidth,257.0f));
			normalmapTexture.setHeight(max(oldWidth,257.0f));
			normalmapTexture.setPixelFormat(FLOAT_RGBA);
			normalmapTexture.load();
			normalmapTempbuffer.setWidth(max(oldWidth,257.0f));
			normalmapTempbuffer.setHeight(max(oldWidth,257.0f));
			normalmapTempbuffer.setPixelFormat(FLOAT_RGBA);
			normalmapTempbuffer.load();
			pixelcount.value = max(oldWidth,257.0f);
			tempTexture.setWidth(oldWidth);
			tempTexture.setHeight(oldWidth);
			tempTexture.setPixelFormat(FLOAT_RGBA);
			tempTexture.load();
			heightvaluesTarget.setWidth(oldWidth);
			heightvaluesTarget.setHeight(oldWidth);
			heightvaluesTarget.setRenderToDisplay(false);
			heightvaluesTarget.load();
			normalmapTarget.setWidth(max(oldWidth,257.0f));
			normalmapTarget.setHeight(max(oldWidth,257.0f));
			normalmapTarget.setRenderToDisplay(false);
			normalmapTarget.load();
			heightvaluesTransform = 
				Matrix().scale(Vector((oldWidth-1)/oldWidth,(oldWidth-1)/oldWidth,1)) *
				 Matrix().scale(Vector(0.0025f,0.0025f,1))
				;
			heightvaluesTexcoord =
				Matrix().translate(Vector(0.5f,0.5f,0)) * Matrix().scale(Vector(0.5f,0.5f,1)) *
				Matrix().scale(Vector((oldWidth-1)/oldWidth,(oldWidth-1)/oldWidth,1)) *
				 Matrix().scale(Vector(0.0025f,0.0025f,1))
				;
			float normalRatio = max((oldWidth-1)/oldWidth , 256.0f/257.0f);
			normalmapTransform = 
				Matrix().scale(Vector(normalRatio,normalRatio,1)) *
				Matrix().scale(Vector(0.0025f,0.0025f,1))
				;
			normalmapTexcoord = 
				Matrix().translate(Vector(0.5f,0.5f,0)) * Matrix().scale(Vector(0.5f,0.5f,1)) *
				Matrix().scale(Vector(normalRatio,normalRatio,1)) *
				Matrix().scale(Vector(0.0025f,0.0025f,1))
				;
			//then the heightvalues can be rendered into the heightvaluesTexture
			heightvaluesEffect.render();
		}

		//matrix using world space coordinates to calculate texture space coordinates,
		//which are used to read from the texture of normally distributed values
		Texture &gaussiandistribution = Renderer::shadeX.getTexture("gaussiandistribution.texture");
		float textureWidth = (unsigned int)gaussiandistribution.getWidth();
		float delta = 1/textureWidth;
		float smallest = delta * 0.5f;
		
		//calculate random displacement of the texture
		float u1 = ((float)rand())/((float)RAND_MAX);
		float u2 = ((float)rand())/((float)RAND_MAX);
		unsigned int displacementX = textureWidth * u1;
		unsigned int displacementY = textureWidth * u2;
		Matrix &XCoords = Renderer::shadeX.getUniformMatrix("gaussiandistribution.transform");
		XCoords = 
			Matrix().translate(Vector(smallest + delta*displacementX,smallest + delta*displacementY,0)) * Matrix().scale(Vector((newWidth-1)*delta,(newWidth-1)*delta,1)) *
			Matrix().translate(Vector(0.5f,0.5f,0)) * Matrix().scale(Vector(0.5f,0.5f,1)) * Matrix().scale(Vector(0.0025f,0.0025f,1));

		//submit current step count
		UniformFloat &stepcountUniform = Renderer::shadeX.getUniformFloat("terrain.stepcount");
		stepcountUniform.value = (float)stepcount;

		//there are two meshes, such that one mesh serves as a source,
		//and the geometry generated from the source is stored into the other mesh
		//each step both meshes change roles
		//on even stepcounts "terrain.mesh1" serves as a source, otherwise "terrain.mesh2" is the source

		if(updatecount % 2 == 0) {
			//render geometry from mesh1 into mesh2, if stepcount is even
			Pass &mesh1_to_mesh2 = Renderer::shadeX.getPass("terrain.subdivide.mesh1_to_mesh2");
			mesh1_to_mesh2.render();

			RenderObject &obj1 = Renderer::shadeX.getRenderObject("terrain.object1");
			RenderObject &obj2 = Renderer::shadeX.getRenderObject("terrain.object2");
			obj1.setVisible(false);
			obj2.setVisible(true);
		} else {
			//render geometry from mesh2 into mesh1, if stepcount is odd
			Pass &mesh2_to_mesh1 = Renderer::shadeX.getPass("terrain.subdivide.mesh2_to_mesh1");
			mesh2_to_mesh1.render();

			RenderObject &obj1 = Renderer::shadeX.getRenderObject("terrain.object1");
			RenderObject &obj2 = Renderer::shadeX.getRenderObject("terrain.object2");
			obj1.setVisible(true);
			obj2.setVisible(false);
		}

		//after the subdivision update the heightmap
		//and calculate normalvectors from it
		if(oldWidth != newWidth) {
			heightvaluesTexture.setWidth(newWidth);
			heightvaluesTexture.setHeight(newWidth);
			heightvaluesTexture.setPixelFormat(FLOAT_RGBA);
			heightvaluesTexture.load();
			normalmapTexture.setWidth(max(newWidth,257.0f));
			normalmapTexture.setHeight(max(newWidth,257.0f));
			normalmapTexture.setPixelFormat(FLOAT_RGBA);
			normalmapTexture.load();
			normalmapTempbuffer.setWidth(max(newWidth,257.0f));
			normalmapTempbuffer.setHeight(max(newWidth,257.0f));
			normalmapTempbuffer.setPixelFormat(FLOAT_RGBA);
			normalmapTempbuffer.load();
			pixelcount.value = max(newWidth,257.0f);
			tempTexture.setWidth(newWidth);
			tempTexture.setHeight(newWidth);
			tempTexture.setPixelFormat(FLOAT_RGBA);
			tempTexture.load();
			heightvaluesTarget.setWidth(newWidth);
			heightvaluesTarget.setHeight(newWidth);
			heightvaluesTarget.setRenderToDisplay(false);
			heightvaluesTarget.load();
			normalmapTarget.setWidth(max(newWidth,257.0f));
			normalmapTarget.setHeight(max(newWidth,257.0f));
			normalmapTarget.setRenderToDisplay(false);
			normalmapTarget.load();
			heightvaluesTransform = 
				Matrix().scale(Vector((newWidth-1)/newWidth,(newWidth-1)/newWidth,1)) *
				 Matrix().scale(Vector(0.0025f,0.0025f,1))
				;
			heightvaluesTexcoord =
				Matrix().translate(Vector(0.5f,0.5f,0)) * Matrix().scale(Vector(0.5f,0.5f,1)) *
				Matrix().scale(Vector((newWidth-1)/newWidth,(newWidth-1)/newWidth,1)) *
				 Matrix().scale(Vector(0.0025f,0.0025f,1))
				;
			float normalRatio = max((newWidth-1)/newWidth , 256.0f/257.0f);
			normalmapTransform = 
				Matrix().scale(Vector(normalRatio,normalRatio,1)) *
				Matrix().scale(Vector(0.0025f,0.0025f,1))
				;
			normalmapTexcoord = 
				Matrix().translate(Vector(0.5f,0.5f,0)) * Matrix().scale(Vector(0.5f,0.5f,1)) *
				Matrix().scale(Vector(normalRatio,normalRatio,1)) *
				Matrix().scale(Vector(0.0025f,0.0025f,1))
				;
		}
		//the heightvalues are rendered into the heightvaluesTexture
		heightvaluesEffect.render();

		stepcount++;
		updatecount++;
	}

	Vector Terrain::getToolPosition(const Vector &mousePosition) {
		//calculate viewing direction and position in world space	
		Vector &raycastStart = Renderer::shadeX.getUniformVector("paint.start.vector");
		Vector &raycastDirection = Renderer::shadeX.getUniformVector("paint.raydirection.vector");
		Matrix &view = Renderer::shadeX.getUniformMatrix("viewer.view.matrix");
		Matrix &projection = Renderer::shadeX.getUniformMatrix("viewer.projection.matrix");
		Vector &viewerPosition = Renderer::shadeX.getUniformVector("viewer.position.vector");
		UniformFloat &width = Renderer::shadeX.getUniformFloat("present.width");
		UniformFloat &height = Renderer::shadeX.getUniformFloat("present.height");
		Matrix inverseViewProjection = projection * view;
		inverseViewProjection.inverse();
		Vector mPos = inverseViewProjection * Vector(mousePosition[0]*2.0f/width.value - 1.0f,mousePosition[1]*2.0f/height.value - 1.0f);
		mPos = mPos * (1/mPos[3]);
		raycastDirection = mPos + viewerPosition * -1.0f;
		raycastDirection.normalize();

		//if the viewer is within the terrain area
		//simply use the viewer position as a starting point for ray casting
		raycastStart = viewerPosition;
		if(viewerPosition[0] < -400 || viewerPosition[0] > 400 || viewerPosition[1] < -400 || viewerPosition[1] > 400) {
			//viewer is not within the area of the terrain
			//hence cut the view line with the border of the
			//terrain area to obtain a starting point for ray casting
			float eastFactor = -1;
			float westFactor = -1;
			float northFactor = -1;
			float southFactor = -1;
			float factor = -1;
			if(raycastDirection[0] != 0) {
				eastFactor = (400.0f - viewerPosition[0]) / raycastDirection[0];
				westFactor = (-400.0f - viewerPosition[0]) / raycastDirection[0];
			}
			if(raycastDirection[1] != 0) {
				northFactor = (400.0f - viewerPosition[1]) / raycastDirection[1];
				southFactor = (-400.0f - viewerPosition[1]) / raycastDirection[1];
			}
			Vector east = viewerPosition + raycastDirection * eastFactor;
			Vector west = viewerPosition + raycastDirection * westFactor;
			Vector north = viewerPosition + raycastDirection * northFactor;
			Vector south = viewerPosition + raycastDirection * southFactor;
			if(eastFactor >= 0) {
				float xcoord = abs( Vector(0,1,0).innerprod(east) );
				//hit terrain border
				if(xcoord <= 400 && factor < 0) {
					factor = eastFactor;
				} else if(xcoord <= 400) {
					factor = min(factor,eastFactor);
				}
			}
			if(westFactor >= 0) {
				float xcoord = abs( Vector(0,1,0).innerprod(west) );
				//hit terrain border
				if(xcoord <= 400 && factor < 0) {
					factor = westFactor;
				} else if(xcoord <= 400) {
					factor = min(factor,westFactor);
				}
			}
			if(northFactor >= 0) {
				float xcoord = abs( Vector(1,0,0).innerprod(north) );
				if(xcoord <= 400 && factor < 0) {
					factor = northFactor;
				} else if(xcoord <= 400) {
					factor = min(factor,northFactor);
				}
			}
			if(southFactor >= 0) {
				float xcoord = abs( Vector(1,0,0).innerprod(south) );
				if(xcoord <= 400 && factor < 0) {
					factor = southFactor;
				} else if(xcoord <= 400) {
					factor = min(factor,southFactor);
				}
			}
			if(factor < 0) {
				//ray is not cutting the border of the terrain
				//hence stop here, as the ray would not hit
				//the terrain
				throw Exception("Error: ray does not hit the terrain", EX_NODATA);
			}
			raycastStart = viewerPosition + raycastDirection * factor;
		}

		//cast the ray into the direction
		//specified by the mouse
		//and get the intersection point with the terrain
		Vector toolPosition;
		bool hasToolPosition = false;

		Pass &getToolPosition = Renderer::shadeX.getPass("paint.getposition.pass");
		getToolPosition.render();

		BufferedMesh &toolPositionMesh = Renderer::shadeX.getBufferedMesh("paint.toolposition.mesh");
		VertexBuffer *hasPositionBuffer = toolPositionMesh.getBuffer("hasPosition");
		if(hasPositionBuffer == 0) {
			throw Exception("Error: missing hasPosition buffer", EX_NODATA);
		}
		//read the location in the vertexbuffer indicating
		//if an intersection point was found
		const float *hasPosition = hasPositionBuffer->unlockRead();
		hasToolPosition = hasPosition[0] > 0.5f;
		hasPositionBuffer->lock();
		VertexBuffer *toolPositionBuffer = toolPositionMesh.getBuffer("toolposition");
		if(toolPositionBuffer == 0) {
			throw Exception("Error: missing toolposition buffer", EX_NODATA);
		}
		//read the location in the vertexbuffer
		//specifying the intersection point
		const float *toolPos = toolPositionBuffer->unlockRead();
		toolPosition = Vector(toolPos[0],toolPos[1],toolPos[2]);
		toolPositionBuffer->lock();

		if(!hasToolPosition) {
			throw Exception("Error: ray does not hit the terrain", EX_NODATA);
		}
		return toolPosition;
	}

	void Terrain::variancePaint(const Vector &mousePosition) {
		try {
			Vector toolPosition = getToolPosition(mousePosition);

			Vector &toolPos = Renderer::shadeX.getUniformVector("variancepaint.paintlocation.vector");
			UniformFloat &paintSize = Renderer::shadeX.getUniformFloat("variancepaint.size.float");
			Matrix &toolModelMatrix = Renderer::shadeX.getUniformMatrix("variancepaint.variancepainter.model");
			toolPos = toolPosition;
			toolModelMatrix = Matrix().translate(toolPos) * Matrix().scale(Vector(paintSize.value));

			//now do the actual painting
			Effect &paintEffect = Renderer::shadeX.getEffect("variancepaint.paint.effect");
			paintEffect.render();
		} catch(Exception &) {
		}
	}

	void Terrain::heightPaint(const Vector &mousePosition, bool raise) {
		try {
			//if the mouse does not change position,
			//the old toolposition is used
			Vector toolPosition = oldToolPosition;
			if(!oldMousePosition.equals(mousePosition,0)) {
				//mouse changed position, hence
				//a new toolposition is calculated
				toolPosition = getToolPosition(mousePosition);
				oldMousePosition = mousePosition;
				oldToolPosition = toolPosition;
			}

			Vector &toolPos = Renderer::shadeX.getUniformVector("heightpaint.paintlocation.vector");
			UniformFloat &paintSize = Renderer::shadeX.getUniformFloat("heightpaint.size.float");
			UniformFloat &raiseFactor = Renderer::shadeX.getUniformFloat("heightpaint.raise.float");
			Matrix &toolModelMatrix = Renderer::shadeX.getUniformMatrix("heightpaint.heightpainter.model");
			toolPos = toolPosition;
			toolModelMatrix = Matrix().translate(toolPos) * Matrix().scale(Vector(paintSize.value));
		
			//determine if the paint tool should raise
			//or lower the terrain
			if(raise) {
				raiseFactor.value = 1;
			} else {
				raiseFactor.value = -1;
			}

			//now do the actual painting
			if(updatecount % 2 == 0) {
				//render geometry from mesh1 into mesh2, if stepcount is even
				Pass &mesh1_to_mesh2 = Renderer::shadeX.getPass("heightpaint.movevertices1_2.pass");
				mesh1_to_mesh2.render();

				RenderObject &obj1 = Renderer::shadeX.getRenderObject("terrain.object1");
				RenderObject &obj2 = Renderer::shadeX.getRenderObject("terrain.object2");
				obj1.setVisible(false);
				obj2.setVisible(true);
			} else {
				Pass &mesh2_to_mesh1 = Renderer::shadeX.getPass("heightpaint.movevertices2_1.pass");
				mesh2_to_mesh1.render();

				RenderObject &obj1 = Renderer::shadeX.getRenderObject("terrain.object1");
				RenderObject &obj2 = Renderer::shadeX.getRenderObject("terrain.object2");
				obj1.setVisible(true);
				obj2.setVisible(false);
			}
			//the heightvalues are rendered into the heightvaluesTexture
			Effect &heightvaluesEffect = Renderer::shadeX.getEffect("terrain.heightvalues.effect");
			heightvaluesEffect.render();

			updatecount++;
		} catch(Exception &) {
		}
	
	}

	void Terrain::raiseHeightPaint(const Vector &mousePosition) {
		heightPaint(mousePosition,true);
	}

	void Terrain::lowerHeightPaint(const Vector &mousePosition) {
		heightPaint(mousePosition,false);
	}

	void Terrain::resetVarianceMap() {
		Pass &resetVariancePass = Renderer::shadeX.getPass("variancepaint.reset.pass");
		resetVariancePass.render();
	}

	void Terrain::update(SXRenderArea &area) {
		Vector &varianceToolPos = Renderer::shadeX.getUniformVector("variancepaint.paintlocation.vector");
		UniformFloat &variancePaintSize = Renderer::shadeX.getUniformFloat("variancepaint.size.float");
		Matrix &varianceToolModelMatrix = Renderer::shadeX.getUniformMatrix("variancepaint.variancepainter.model");
		varianceToolModelMatrix = Matrix().translate(varianceToolPos) * Matrix().scale(Vector(variancePaintSize.value));

		Vector &heightToolPos = Renderer::shadeX.getUniformVector("heightpaint.paintlocation.vector");
		UniformFloat &heightPaintSize = Renderer::shadeX.getUniformFloat("heightpaint.size.float");
		Matrix &heightToolModelMatrix = Renderer::shadeX.getUniformMatrix("heightpaint.heightpainter.model");
		heightToolModelMatrix = Matrix().translate(heightToolPos) * Matrix().scale(Vector(heightPaintSize.value));

		Matrix &waterPlaneMatrix = Renderer::shadeX.getUniformMatrix("present.waterplane.matrix");
		UniformFloat &waterHeight = Renderer::shadeX.getUniformFloat("present.waterheight");
		waterPlaneMatrix = Matrix().translate(Vector(0,0,waterHeight.value));
	}

	unsigned int Terrain::getStepCount() const {
		return stepcount;
	}

	void Terrain::setMaxStepcount(unsigned int count) {
		this->maxStepcount = count;
	}

	unsigned int Terrain::getMaxStepcount() const {
		return maxStepcount;
	}

}

#endif