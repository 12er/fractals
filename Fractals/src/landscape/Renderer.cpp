#ifndef _LANDSCAPE_RENDERER_CPP_
#define _LANDSCAPE_RENDERER_CPP_

/**
 * Fractal Landscape objects
 * (c) 2016 by Tristan Bauer
 */
#include <Landscape.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
using namespace sx;

namespace landscape {

	ShadeX Renderer::shadeX;

	Renderer::Renderer() {
	}

	void Renderer::setView(View view) {
		this->view = view;
		if(view == VIEW_VARIANCE || view == VIEW_HEIGHT) {
			Entity::getViewer().setPainting(true);
		} else {
			Entity::getViewer().setPainting(false);
		}
	}

	void Renderer::create(SXRenderArea &area) {
		view = VIEW_SHADED;
		try {
			//Load shaders, geometry, textures and other resources
			//The actual fractal computations are all happening in the shaders!
			//The shaders can be found in the configuration file terrain.c
			shadeX.addResources("../data/landscape/configuration/terrain.c");
			shadeX.addResources("../data/landscape/configuration/saveterrain.c");
			shadeX.addResources("../data/landscape/configuration/present_resources.c");
			shadeX.addResources("../data/landscape/configuration/sky.c");
			shadeX.addResources("../data/landscape/configuration/water.c");
			shadeX.addResources("../data/landscape/configuration/present_shaded.c");
			shadeX.addResources("../data/landscape/configuration/present_wireframe.c");
			shadeX.addResources("../data/landscape/configuration/present_variance.c");
			shadeX.addResources("../data/landscape/configuration/present_height.c");
			shadeX.addResources("../data/landscape/configuration/variancepaint.c");
			shadeX.addResources("../data/landscape/configuration/heightpaint.c");
			shadeX.addResources("../data/landscape/configuration/copyback.c");
		} catch(Exception &e) {
			Logger::get() << Level(L_ERROR) << e.getMessage();
			area.stopRendering();
			return;
		}

		try {
			shadeX.load();
		} catch(Exception &e) {
			Logger::get() << Level(L_ERROR) << e.getMessage();
			area.stopRendering();
			return;
		}
		Entity::initEntities();
	}

	void Renderer::reshape(SXRenderArea &area) {

		RenderTarget &screen = shadeX.getRenderTarget("present.target");
		Texture &reflectionmap = shadeX.getTexture("present.reflectionmap.texture");
		Texture &refractionmap = shadeX.getTexture("present.refractionmap.texture");
		Texture &underwaterscreen = shadeX.getTexture("present.underwater.texture");
		RenderTarget &offlinescreen = shadeX.getRenderTarget("present.offline.rendertarget");
		
		UniformFloat &width = shadeX.getUniformFloat("present.width");
		UniformFloat &height = shadeX.getUniformFloat("present.height");

		screen.setWidth(area.getWidth());
		screen.setHeight(area.getHeight());
		screen.load();
		reflectionmap.setWidth(area.getWidth());
		reflectionmap.setHeight(area.getHeight());
		reflectionmap.setPixelFormat(BYTE_RGBA);
		reflectionmap.load();
		refractionmap.setWidth(area.getWidth());
		refractionmap.setHeight(area.getHeight());
		refractionmap.setPixelFormat(FLOAT_RGBA);
		refractionmap.load();
		underwaterscreen.setWidth(area.getWidth());
		underwaterscreen.setHeight(area.getHeight());
		underwaterscreen.setPixelFormat(FLOAT_RGBA);
		underwaterscreen.load();
		offlinescreen.setWidth(area.getWidth());
		offlinescreen.setHeight(area.getHeight());
		offlinescreen.setRenderToDisplay(false);
		offlinescreen.load();
		width.value = area.getWidth();
		height.value = area.getHeight();
	}

	void Renderer::render(SXRenderArea &area) {
		Entity::updateEntities(area);
		if(view == VIEW_SHADED) {
			UniformFloat &abovewater = shadeX.getUniformFloat("present.abovewater");
			if(abovewater.value > 0.5f) {
				Effect &effect = shadeX.getEffect("present.shaded.effect");
				effect.render();
			} else {
				Effect &effect = shadeX.getEffect("present.underwater.effect");
				effect.render();
			}
		} else if(view == VIEW_WIREFRAME) {
			Effect &effect = shadeX.getEffect("present.wireframe.effect");
			effect.render();
		} else if(view == VIEW_VARIANCE) {
			if(area.hasMouseKey(MOUSE_LEFT)) {
				Vector mousePosition(area.getMouseX(),area.getMouseY());
				Entity::getTerrain().variancePaint(mousePosition);
			}

			Effect &effect = shadeX.getEffect("present.variance.effect");
			effect.render();
		} else if(view == VIEW_HEIGHT) {
			if(area.hasMouseKey(MOUSE_LEFT)) {
				Vector mousePosition(area.getMouseX(),area.getMouseY());
				if(area.hasKey(SX_SHIFT)) {
					Entity::getTerrain().lowerHeightPaint(mousePosition);
				} else {
					Entity::getTerrain().raiseHeightPaint(mousePosition);
				}
			}

			Effect &effect = shadeX.getEffect("present.height.effect");
			effect.render();
		}
	}

	void Renderer::stop(SXRenderArea &area) {
		Entity::destroyEntities();
		shadeX.clear();
	}

}

#endif