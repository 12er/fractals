#ifndef _LANDSCAPE_GUI_MAINWIDGET_CPP_
#define _LANDSCAPE_GUI_MAINWIDGET_CPP_

/**
 * Fractal Landscape generator
 * (c) 2016 by Tristan Bauer
 */
#include <Landscape.h>
#include <QGridLayout>
#include <sx/SXWidget.h>
#include <sx/Exception.h>
#include <QDockWidget>
#include <QTabWidget>
#include <QMenuBar>
#include <QMenu>
#include <QToolBar>
#include <QToolButton>
#include <QLabel>
#include <QComboBox>
#include <QStringList>
#include <QAction>
#include <QPushButton>
#include <QFileDialog>
#include <QSplashScreen>
using namespace sx;

namespace landscape {

	const string MainWidget::VIEW_SHADED = "Shaded View";
	const string MainWidget::VIEW_WIREFRAME = "Wireframe View";
	const string MainWidget::VIEW_VARIANCE = "Variance View";
	const string MainWidget::VIEW_HEIGHT = "Heightmap View";

	const string MainWidget::CONTROL_ROTATE = "Rotate";
	const string MainWidget::CONTROL_WASD = "WASD";

	const string MainWidget::PARAM_GLOBAL = "Global Parameters";
	const string MainWidget::PARAM_LIGHT = "Lighting";
	const string MainWidget::PARAM_VARIANCE = "Variance Paint";
	const string MainWidget::PARAM_HEIGHT = "Height Paint";

	void MainWidget::showParametersTab(const QString &title) {
		int index = -1;
		for(unsigned int i=0 ; parametersTabs->isTabEnabled(i) ; i++) {
			if(parametersTabs->tabText(i).compare(title) == 0) {
				index = i;
				break;
			}
		}
		if(index != -1) {
			parametersTabs->setCurrentIndex(index);
		}
	}

	void MainWidget::updateControl() {
		Viewer &viewer = Entity::getViewer();
		if(cameraOptions->currentText().compare( tr(CONTROL_ROTATE.c_str()) ) == 0) {
			viewer.setViewerInput(VIEWER_ROTATE);
		} else if(cameraOptions->currentText().compare( tr(CONTROL_WASD.c_str()) ) == 0) {
			viewer.setViewerInput(VIEWER_WASD);
		}
	}

	void MainWidget::updateView() {
		if(viewOptions->currentText().compare( tr(VIEW_SHADED.c_str()) ) == 0) {
			renderer->setView(landscape::VIEW_SHADED);
		} else if(viewOptions->currentText().compare( tr(VIEW_WIREFRAME.c_str()) ) == 0) {
			renderer->setView(landscape::VIEW_WIREFRAME);
		} else if(viewOptions->currentText().compare( tr(VIEW_VARIANCE.c_str()) ) == 0) {
			renderer->setView(landscape::VIEW_VARIANCE);
			showParametersTab(tr(PARAM_VARIANCE.c_str()));
		} else if(viewOptions->currentText().compare( tr(VIEW_HEIGHT.c_str()) ) == 0) {
			renderer->setView(landscape::VIEW_HEIGHT);
			showParametersTab(tr(PARAM_HEIGHT.c_str()));
		}
	}

	void MainWidget::updateVariance() {
		UniformFloat &varianceUniform = Renderer::shadeX.getUniformFloat("terrain.sqrtvariance");
		varianceUniform.value = sqrt(globalVariance->getValue());
	}

	void MainWidget::updateH() {
		UniformFloat &HUniform = Renderer::shadeX.getUniformFloat("terrain.H");
		HUniform.value = globalH->getValue();
	}

	void MainWidget::updateMaxHeight() {
		Terrain &terrain = Entity::getTerrain();
		UniformFloat &maxHeightUniform = Renderer::shadeX.getUniformFloat("terrain.maximumheight");
		maxHeightUniform.value = maxHeight->getValue();
		initialHeight->setMaximum(maxHeight->getValue());
		terrain.clampHeight();
	}

	void MainWidget::updateTerrainHeight() {
		Terrain &terrain = Entity::getTerrain();
		UniformFloat &initHeight = Renderer::shadeX.getUniformFloat("terrain.initialheight");
		initHeight.value = (float) min(initialHeight->getValue() , maxHeight->getValue());
		//only proceed, if no subdivision steps have been done yet
		if(terrain.getStepCount() == 0) {
			terrain.restart();
		}
	}

	void MainWidget::updateKernelSize() {
		UniformFloat &kernelSizeUniform = Renderer::shadeX.getUniformFloat("terrain.heightvalues.kernelsize");
		kernelSizeUniform.value = (float) kernelSize->getValue();
	}

	void MainWidget::updateWaterHeight() {
		UniformFloat &waterHeightUniform = Renderer::shadeX.getUniformFloat("present.waterheight");
		waterHeightUniform.value = (float) waterHeight->getValue();
	}

	void MainWidget::updateLightRot() {
		Vector &lightdir = Renderer::shadeX.getUniformVector("present.lightdir");
		float angleZ = Pi * (float) lightRotZ->getValue() / 180.0f;
		float angleY = Pi * (float) lightRotY->getValue() / 180.0f;
		lightdir = Matrix().rotate(Vector(0,0,1),angleZ)
			* Matrix().rotate(Vector(0,-1,0),angleY)
			* Vector(-1,0,0);
	}

	void MainWidget::updatePaintVariance() {
		UniformFloat &vUniform = Renderer::shadeX.getUniformFloat("variancepaint.variance.float");
		vUniform.value = (float) paintVariance->getValue();
	}

	void MainWidget::updatePaintVarianceH() {
		UniformFloat &hUniform = Renderer::shadeX.getUniformFloat("variancepaint.h.float");
		hUniform.value = (float) paintH->getValue();
	}

	void MainWidget::updatePaintVarianceRadius() {
		UniformFloat &radius = Renderer::shadeX.getUniformFloat("variancepaint.size.float");
		radius.value = (float) paintVarianceRadius->getValue();
	}

	void MainWidget::updatePaintVarianceInfluence() {
		UniformFloat &iUniform = Renderer::shadeX.getUniformFloat("variancepaint.influence.float");
		iUniform.value = (float) paintVarianceInfluence->getValue();
	}

	void MainWidget::updatePaintVarianceIntensity() {
		UniformFloat &iUniform = Renderer::shadeX.getUniformFloat("variancepaint.intensity.float");
		iUniform.value = (float) paintVarianceIntensity->getValue();
	}

	void MainWidget::resetVarianceMap() {
		Entity::getTerrain().resetVarianceMap();
	}

	void MainWidget::updateHeightRadius() {
		UniformFloat &iHR = Renderer::shadeX.getUniformFloat("heightpaint.size.float");
		iHR.value = (float) paintHeightRadius->getValue();
	}

	void MainWidget::updateHeightIntensity() {
		UniformFloat &iHI = Renderer::shadeX.getUniformFloat("heightpaint.intensity.float");
		iHI.value = (float) paintHeightIntensity->getValue();
	}

	void MainWidget::saveAsImage() {
		QString filename = QFileDialog::getSaveFileName(this,tr("Save terrain as image"),tr(""),tr("16 bit per channel(*.png)"));
		try {
			Entity::getTerrain().saveAsImage(filename.toStdString());
		} catch(Exception &) {
		}
	}

	void MainWidget::saveAsMesh() {
		QString filename = QFileDialog::getSaveFileName(this,tr("Save terrain as mesh"),tr(""),tr("Collada mesh(*.dae)"));
		try {
			Entity::getTerrain().saveAsMesh(filename.toStdString());
		} catch(Exception &) {
		}
	}

	void MainWidget::loadFromImage() {
		LoadMapDialog *dialog = new LoadMapDialog(this);
		dialog->setModal(true);
		dialog->show();
		dialog->exec();
		string heightmapPath;
		string blendmapPath;
		try {
			heightmapPath = dialog->getHeightmapPath();
			blendmapPath = dialog->getBlendmapPath();
		} catch(Exception &) {
			//user has not completed input
			//hence do nothing
			return;
		}
		try {
			Terrain &terrain = Entity::getTerrain();
			if(dialog->isUsingBlendmap()) {
				terrain.loadFromImage(heightmapPath,blendmapPath);
			} else {
				terrain.loadFromImage(heightmapPath);
			}
		} catch(Exception &e) {
			WarningWidget *warning = new WarningWidget(tr("Loading Error"),tr(e.getMessage().c_str()),this);
			warning->setModal(true);
			warning->show();
			return;
		}
	}

	void MainWidget::restartTerrain() {
		Terrain &terrain = Entity::getTerrain();
		terrain.restart();

		printStatus(tr("Steps: 0"));
	}

	void MainWidget::subdivideTerrain() {
		try {
			Terrain &terrain = Entity::getTerrain();
			terrain.subdivide();
			printStatus(tr("Steps: ") + QString::number(terrain.getStepCount()) );
		} catch(Exception &e) {
			WarningWidget *warning = new WarningWidget("Subdivision Limit",tr(e.getMessage().c_str()),this);
			warning->setModal(true);
			warning->show();
			return;
		}
	}

	void MainWidget::updateParametersTab() {
		if(parametersTabs->tabText(parametersTabs->currentIndex()).compare(tr(PARAM_VARIANCE.c_str())) == 0) {
			viewOptions->setCurrentIndex(viewOptions->findText(tr(VIEW_VARIANCE.c_str())));
		} else if(parametersTabs->tabText(parametersTabs->currentIndex()).compare(tr(PARAM_HEIGHT.c_str())) == 0) {
			viewOptions->setCurrentIndex(viewOptions->findText(tr(VIEW_HEIGHT.c_str())));
		}
	}

	void MainWidget::wheelRotates(float degrees) {
		Entity::getViewer().changeDistance(degrees);
	}

	void MainWidget::showInfo() {
		QSplashScreen *splashScreen = new QSplashScreen(QPixmap("../data/landscape/textures/Splashscreen.png"));
		splashScreen->show();
	}

	MainWidget::MainWidget(): QMainWindow() {
		QWidget *central_Widget = new QWidget();
		setCentralWidget(central_Widget);
		QGridLayout *layout = new QGridLayout();
		central_Widget->setLayout(layout);

		//add sx widget
		renderer = new Renderer();
		SXWidget *renderWidget = new SXWidget();
		renderWidget->setMinimumSize(700,450);
		renderWidget->addRenderListener(*renderer);
		QObject::connect(renderWidget,SIGNAL(finishedRendering()),renderWidget,SLOT(close()));
		QObject::connect(renderWidget,SIGNAL(wheelRotates(float)),this,SLOT(wheelRotates(float)));
		renderWidget->renderPeriodically(0.01f);
		layout->addWidget(renderWidget,1,1);

		//add status label
		statusLabel = new QLabel("Steps: 0");
		statusLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
		layout->addWidget(statusLabel,2,1);

		//add dock widget
		QDockWidget *parameters = new QDockWidget("Parameters");
		parameters->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		parametersTabs = new QTabWidget();
		parametersTabs->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Expanding);
		parametersTabs->setTabPosition(QTabWidget::East);
		parameters->setWidget(parametersTabs);
		addDockWidget(Qt::RightDockWidgetArea, parameters);

		//global parameters tab
		QWidget *globalParameters_top = new QWidget();
		QGridLayout *globalParameters_intermediateLayout = new QGridLayout();
		globalParameters_top->setLayout(globalParameters_intermediateLayout);
		QWidget *globalParametersWidget = new QWidget();
		QGridLayout *globalParameters = new QGridLayout();
		globalParametersWidget->setLayout(globalParameters);
		globalParameters_intermediateLayout->addWidget(globalParametersWidget,0,0,Qt::AlignTop);
		globalParametersWidget->setLayout(globalParameters);
		parametersTabs->addTab(globalParameters_top,tr(PARAM_GLOBAL.c_str()));
		
		globalVariance = new FloatSlider("Variance",40000,0,100000);
		globalParameters->addWidget(globalVariance,0,0);
		QObject::connect(globalVariance,SIGNAL(valueChanged(double)),this,SLOT(updateVariance()));

		globalH = new FloatSlider("H",0.6f,0,1);
		globalParameters->addWidget(globalH,1,0);
		QObject::connect(globalH,SIGNAL(valueChanged(double)),this,SLOT(updateH()));

		maxHeight = new FloatSlider("maximum height",200,0,1000);
		globalParameters->addWidget(maxHeight,2,0);
		QObject::connect(maxHeight,SIGNAL(valueChanged(double)),this,SLOT(updateMaxHeight()));

		initialHeight = new FloatSlider("initial height",50,0,200);
		globalParameters->addWidget(initialHeight,3,0);
		QObject::connect(initialHeight,SIGNAL(valueChanged(double)),this,SLOT(updateTerrainHeight()));

		kernelSize = new FloatSlider("kernel size",0.0015f,0,0.1f);
		globalParameters->addWidget(kernelSize,4,0);
		QObject::connect(kernelSize,SIGNAL(valueChanged(double)),this,SLOT(updateKernelSize()));

		waterHeight = new FloatSlider("water height",55,0,1000);
		globalParameters->addWidget(waterHeight,5,0);
		QObject::connect(waterHeight,SIGNAL(valueChanged(double)),this,SLOT(updateWaterHeight()));

		//lighting tab
		QWidget *lighting_top = new QWidget();
		QGridLayout *lighting_intermediateLayout = new QGridLayout();
		lighting_top->setLayout(lighting_intermediateLayout);
		QWidget *lightingWidget = new QWidget();
		QGridLayout *lighting = new QGridLayout();
		lightingWidget->setLayout(lighting);
		lighting_intermediateLayout->addWidget(lightingWidget,0,0,Qt::AlignTop);
		parametersTabs->addTab(lighting_top,tr(PARAM_LIGHT.c_str()));

		lightRotZ = new FloatSlider("horicontal light angle",45,0,360);
		lighting->addWidget(lightRotZ,0,0);
		QObject::connect(lightRotZ,SIGNAL(valueChanged(double)),this,SLOT(updateLightRot()));

		lightRotY = new FloatSlider("vertical light angle",45,0,90);
		lighting->addWidget(lightRotY,1,0);
		QObject::connect(lightRotY,SIGNAL(valueChanged(double)),this,SLOT(updateLightRot()));

		//variance paint tab
		QWidget *variancePaint_top = new QWidget();
		QGridLayout *variancePaint_intermediateLayout = new QGridLayout();
		variancePaint_top->setLayout(variancePaint_intermediateLayout);
		QWidget *variancePaintWidget = new QWidget();
		QGridLayout *variancePaint = new QGridLayout();
		variancePaintWidget->setLayout(variancePaint);
		variancePaint_intermediateLayout->addWidget(variancePaintWidget,0,0,Qt::AlignTop);
		parametersTabs->addTab(variancePaint_top,tr(PARAM_VARIANCE.c_str()));

		paintVariance = new FloatSlider("Variance",40000,0,100000);
		variancePaint->addWidget(paintVariance,0,0);
		QObject::connect(paintVariance,SIGNAL(valueChanged(double)),this,SLOT(updatePaintVariance()));

		paintH = new FloatSlider("H",0.6,0,1);
		variancePaint->addWidget(paintH,1,0);
		QObject::connect(paintH,SIGNAL(valueChanged(double)),this,SLOT(updatePaintVarianceH()));

		paintVarianceRadius = new FloatSlider("Radius",30,0,1000);
		variancePaint->addWidget(paintVarianceRadius,2,0);
		QObject::connect(paintVarianceRadius,SIGNAL(valueChanged(double)),this,SLOT(updatePaintVarianceRadius()));

		paintVarianceInfluence = new FloatSlider("Influence",1,0,1);
		variancePaint->addWidget(paintVarianceInfluence,3,0);
		QObject::connect(paintVarianceInfluence,SIGNAL(valueChanged(double)),this,SLOT(updatePaintVarianceInfluence()));

		paintVarianceIntensity = new FloatSlider("Intensity",1,0,1);
		variancePaint->addWidget(paintVarianceIntensity,4,0);
		QObject::connect(paintVarianceIntensity,SIGNAL(valueChanged(double)),this,SLOT(updatePaintVarianceIntensity()));

		QPushButton *paintVarianceReset = new QPushButton("Reset Variancemap");
		paintVarianceReset->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		variancePaint->addWidget(paintVarianceReset,5,0);
		QObject::connect(paintVarianceReset,SIGNAL(clicked()),this,SLOT(resetVarianceMap()));

		//height paint tab
		QWidget *heightPaint_top = new QWidget();
		QGridLayout *heightPaint_intermediateLayout = new QGridLayout();
		heightPaint_top->setLayout(heightPaint_intermediateLayout);
		QWidget *heightPaintWidget = new QWidget();
		QGridLayout *heightPaint = new QGridLayout();
		heightPaintWidget->setLayout(heightPaint);
		heightPaint_intermediateLayout->addWidget(heightPaintWidget,0,0,Qt::AlignTop);
		parametersTabs->addTab(heightPaint_top,tr(PARAM_HEIGHT.c_str()));

		paintHeightRadius = new FloatSlider("Radius",30,0,1000);
		heightPaint->addWidget(paintHeightRadius,0,0);
		QObject::connect(paintHeightRadius,SIGNAL(valueChanged(double)),this,SLOT(updateHeightRadius()));

		paintHeightIntensity = new FloatSlider("Intensity",0.02f,0,1);
		heightPaint->addWidget(paintHeightIntensity,1,0);
		QObject::connect(paintHeightIntensity,SIGNAL(valueChanged(double)),this,SLOT(updateHeightIntensity()));

		QLabel *commentLabel = new QLabel("Press Shift to lower the terrain with the mouse");
		heightPaint->addWidget(commentLabel,2,0);

		//actions
		QAction *saveImageAction = new QAction(QIcon("../data/shared/icons/Save.png"), "Save as Image", this);
		saveImageAction->setShortcut(tr("Ctrl+S"));
		saveImageAction->setStatusTip("Save terrain as image");
		QObject::connect(saveImageAction,SIGNAL(triggered()),this,SLOT(saveAsImage()));

		QAction *saveMeshAction = new QAction(QIcon("../data/shared/icons/Save.png"), "Save as Mesh", this);
		saveMeshAction->setStatusTip("Save terrain as mesh");
		QObject::connect(saveMeshAction,SIGNAL(triggered()),this,SLOT(saveAsMesh()));

		QAction *openImageAction = new QAction(QIcon("../data/shared/icons/Open.png"), "Load from Image", this);
		openImageAction->setShortcut(tr("Ctrl+O"));
		openImageAction->setStatusTip("Load terrain from image");
		QObject::connect(openImageAction,SIGNAL(triggered()),this,SLOT(loadFromImage()));

		QAction *exitAction = new QAction("Exit",this);
		exitAction->setStatusTip("Exit application");
		QObject::connect(exitAction,SIGNAL(triggered()),this,SLOT(close()));

		QAction *stepAction = new QAction(QIcon("../data/landscape/icons/Step.png"), "Step", this);
        stepAction->setShortcut(tr("Ctrl+T"));
        stepAction->setStatusTip("Subdivide Terrain");
		QObject::connect(stepAction,SIGNAL(triggered()),this,SLOT(subdivideTerrain()));

		QAction *resetAction = new QAction(QIcon("../data/landscape/icons/Reset.png"), "Reset", this);
		resetAction->setStatusTip("Reset Terrain");
		QObject::connect(resetAction,SIGNAL(triggered()),this,SLOT(restartTerrain()));

		QAction *infoAction = new QAction(QIcon("../data/landscape/icons/Landscape.png"), "About Landscape creator", this);
		QObject::connect(infoAction,SIGNAL(triggered()),this,SLOT(showInfo()));

		//file toolbar
		QToolBar *fileToolbar = new QToolBar("Files");
		addToolBar(Qt::TopToolBarArea,fileToolbar);

		QToolButton *openButton = new QToolButton();
		openButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
		openButton->setDefaultAction(openImageAction);
		fileToolbar->addWidget(openButton);

		QToolButton *saveButton = new QToolButton();
		saveButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
		saveButton->setDefaultAction(saveImageAction);
		fileToolbar->addWidget(saveButton);

		//terrain creation toolbar
		QToolBar *terrainCreationToolbar = new QToolBar("Terrain Creation");
		addToolBar(Qt::TopToolBarArea,terrainCreationToolbar);

		QToolButton *stepButton = new QToolButton();
		stepButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
		stepButton->setDefaultAction(stepAction);
		terrainCreationToolbar->addWidget(stepButton);

		QToolButton *resetButton = new QToolButton();
		resetButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
		resetButton->setDefaultAction(resetAction);
		terrainCreationToolbar->addWidget(resetButton);

		//view toolbar
		QToolBar *viewToolbar = new QToolBar("View");
		addToolBar(Qt::TopToolBarArea,viewToolbar);

		QLabel *viewLabel = new QLabel("View");
		viewLabel->setMargin(5);
		viewToolbar->addWidget(viewLabel);

		QStringList viewList;
		viewList << tr(VIEW_SHADED.c_str()) << tr(VIEW_WIREFRAME.c_str()) << tr(VIEW_VARIANCE.c_str()) << tr(VIEW_HEIGHT.c_str());
		viewOptions = new QComboBox();
		viewOptions->addItems(viewList);
		viewToolbar->addWidget(viewOptions);
		QObject::connect(viewOptions,SIGNAL(currentIndexChanged(int)),this,SLOT(updateView()));

		QLabel *cameraLabel = new QLabel("Control");
		cameraLabel->setMargin(5);
		viewToolbar->addWidget(cameraLabel);

		QStringList cameraList;
		cameraList << tr(CONTROL_ROTATE.c_str()) << tr(CONTROL_WASD.c_str());
		cameraOptions = new QComboBox();
		cameraOptions->addItems(cameraList);
		viewToolbar->addWidget(cameraOptions);
		QObject::connect(cameraOptions,SIGNAL(currentIndexChanged(int)),this,SLOT(updateControl()));

		//file menu
		QMenu *fileMenu = menuBar()->addMenu("File");
		fileMenu->addAction(saveImageAction);
		fileMenu->addAction(saveMeshAction);
		fileMenu->addAction(openImageAction);
		fileMenu->addSeparator();
		fileMenu->addAction(exitAction);

		//terrain creation menu
		QMenu *terrainCreationMenu = menuBar()->addMenu("Terrain Creation");
		terrainCreationMenu->addAction(stepAction);
		terrainCreationMenu->addAction(resetAction);

		//window menu
		QMenu* windowMenu = createPopupMenu();
        windowMenu->setTitle("Window");
        menuBar()->addMenu(windowMenu);

		//help menu
		QMenu *helpMenu = menuBar()->addMenu("Help");
		helpMenu->addAction(infoAction);

		//setup additional connections
		//which need the rest of the gui to be created already
		QObject::connect(parametersTabs,SIGNAL(currentChanged(int)),this,SLOT(updateParametersTab()));
	}

	MainWidget::~MainWidget() {
	}

	void MainWidget::printStatus(const QString &message) {
		statusLabel->setText(message);
	}

}

#endif