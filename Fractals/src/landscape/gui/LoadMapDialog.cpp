#ifndef _LANDSCAPE_GUI_LOADMAPDIALOG_CPP_
#define _LANDSCAPE_GUI_LOADMAPDIALOG_CPP_

/**
 * Widget for obtaining the paths
 * of heightmap and blendmap
 * (c) 2016 by Tristan Bauer
 */
#include <Landscape.h>
#include <QCheckBox>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QFileDialog>
#include <sx/Exception.h>

namespace landscape {

	void LoadMapDialog::getHeightmap() {
		heightmapPath = QFileDialog::getOpenFileName(this,tr("Open a heightmap"),tr(""),tr("*.png *.bmp *.jpg")).toStdString();
	}

	void LoadMapDialog::getBlendmap() {
		blendmapPath = QFileDialog::getOpenFileName(this,tr("Open a blendmap"),tr(""),tr("*.png *.bmp *.jpg")).toStdString();
	}

	void LoadMapDialog::updateUseBlendmap(int checkboxState) {
		if(checkboxState == Qt::Checked) {
			blendmapLabel->setVisible(true);
			blendmapButton->setVisible(true);
			useBlendmap = true;
		} else {
			blendmapLabel->setVisible(false);
			blendmapButton->setVisible(false);
			useBlendmap = false;
		}
	}

	void LoadMapDialog::receiveOk() {
		if(heightmapPath.size() == 0) {
			WarningWidget *warning = new WarningWidget(tr("Incomplete input"),tr("The path of the heightmap hasn't been specified!"),this);
			warning->setModal(true);
			warning->show();
		} else if(useBlendmap && blendmapPath.size() == 0) {
			WarningWidget *warning = new WarningWidget(tr("Incomplete input"),tr("The path of the blendmap hasn't been specified!"),this);
			warning->setModal(true);
			warning->show();
		} else {
			receivedOk = true;
			close();
		}
	}

	LoadMapDialog::LoadMapDialog(QWidget *w): QDialog(w) {
		this->receivedOk = false;
		this->useBlendmap = false;

		setWindowIcon(QIcon("../data/landscape/icons/Landscape.png"));
		setWindowTitle("Load a blendmap");
		QGridLayout *layout = new QGridLayout();
		setLayout(layout);

		QLabel *heightmapLabel = new QLabel("Select an image for the heightmap");
		layout->addWidget(heightmapLabel,0,0);

		QPushButton *heightmapButton = new QPushButton("Set heightmap path");
		layout->addWidget(heightmapButton,0,1);
		QObject::connect(heightmapButton,SIGNAL(clicked()),this,SLOT(getHeightmap()));

		QCheckBox *useBlendMap = new QCheckBox(tr("Use a blendmap"),this);
		layout->addWidget(useBlendMap,1,0);
		QObject::connect(useBlendMap,SIGNAL(stateChanged(int)),this,SLOT(updateUseBlendmap(int)));

		blendmapLabel = new QLabel("Select an image for the blendmap");
		layout->addWidget(blendmapLabel,2,0);
		blendmapLabel->setVisible(false);

		blendmapButton = new QPushButton("Set blendmap path");
		layout->addWidget(blendmapButton,2,1);
		blendmapButton->setVisible(false);
		QObject::connect(blendmapButton,SIGNAL(clicked()),this,SLOT(getBlendmap()));

		QPushButton *loadButton = new QPushButton("Load");
		layout->addWidget(loadButton,3,0);
		QObject::connect(loadButton,SIGNAL(clicked()),this,SLOT(receiveOk()));

		layout->setSizeConstraint(QLayout::SetFixedSize);
	}

	LoadMapDialog::~LoadMapDialog() {
	}

	string LoadMapDialog::getHeightmapPath() const {
		if(!receivedOk) {
			throw Exception("Error: incomplete input",EX_NODATA);
		}
		return heightmapPath;
	}

	string LoadMapDialog::getBlendmapPath() const {
		if(!receivedOk) {
			throw Exception("Error: incomplete input",EX_NODATA);
		}
		return blendmapPath;
	}

	bool LoadMapDialog::isUsingBlendmap() const {
		return useBlendmap;
	}

}

#endif