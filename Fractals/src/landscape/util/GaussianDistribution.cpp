#ifndef _LANDSCAPE_UTIL_GAUSSIANDISTRIBUTION_CPP_
#define _LANDSCAPE_UTIL_GAUSSIANDISTRIBUTION_CPP_

/**
 * Gaussian Distibution
 * (c) 2016 by Tristan Bauer
 */
#include <Landscape.h>
#include <cmath>
#include <ctime>
#include <sx/SXMath.h>
using namespace std;

namespace landscape {

	bool GaussianDistribution::initialized = false;

	float GaussianDistribution::X() {
		if(!initialized) {
			unsigned int t = (unsigned int)time(0);
			srand(t);
			initialized = true;
		}
		//using the box miller transform
		//to create a standard normally distributed random number
		//from two independend uniformly distributed random numbers
		float u1 = ((float)rand())/((float)RAND_MAX);
		float u2 = ((float)rand())/((float)RAND_MAX);
		return sqrt(-2 * log(u1)) * cos(2 * (float)sx::Pi * u2);
	}

}

#endif