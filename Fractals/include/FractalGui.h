#ifndef _FRACTALGUI_H_
#define _FRACTALGUI_H_

/**
 * Common gui classes for the
 * fractal generators
 * (c) 2016 by Tristan Bauer
 */
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QLabel>
#include <QSlider>
#include <QDialog>
#include <QWidget>
#include <string>
using namespace std;

namespace fractals {

	class WarningWidget: public QDialog {
		Q_OBJECT
	private:
		WarningWidget(const WarningWidget &);
		WarningWidget &operator = (const WarningWidget &);
	public:
		WarningWidget(const QString &title, const QString &text, QWidget *w);
	};

	/**
	 *
	 */
	class FloatSlider: public QWidget {

		Q_OBJECT

	signals:
		void valueChanged(double);
	private slots:
		void updateSlider();
		void updateText();
	private:
		QSlider *slider;
		QDoubleSpinBox *text;

		FloatSlider(const FloatSlider &);
		FloatSlider &operator = (const FloatSlider &);
	public:
		FloatSlider(const string &label, double value, double minVal, double maxVal, QWidget *parent = 0, Qt::WindowFlags f = 0);
		~FloatSlider();

		void setValue(double value);
		double getValue() const;
		void setMinimum(double minVal);
		double getMinimum() const;
		void setMaximum(double maxVal);
		double getMaximum() const;
		void setRange(double minVal, double maxVal);
	};

	/**
	 *
	 */
	class IntSlider: public QWidget {

		Q_OBJECT

	signals:
		void valueChanged(int);
	private slots:
		void updateSlider();
		void updateText();
	private:
		QSlider *slider;
		QSpinBox *text;

		IntSlider(const IntSlider &);
		IntSlider &operator = (const IntSlider &);
	public:
		IntSlider(const string &label, int value, int minVal, int maxVal, QWidget *parent = 0, Qt::WindowFlags f = 0);
		~IntSlider();

		void setValue(int value);
		int getValue() const;
		void setMinimum(int minVal);
		int getMinimum() const;
		void setMaximum(int maxVal);
		int getMaximum() const;
		void setRange(int minVal, int maxVal);
	};

}

#endif