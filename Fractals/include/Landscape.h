#ifndef _LANDSCAPE_H_
#define _LANDSCAPE_H_

/**
 * Fractal Landscape generator
 * (c) 2016 by Tristan Bauer
 */
#include <FractalGui.h>
#include <QWidget>
#include <QMainWindow>
#include <QComboBox>
#include <QPushButton>
#include <sx/SX.h>
#include <map>
using namespace sx;
using namespace std;
using namespace fractals;

namespace landscape {

	/**
	 * random variable having a
	 * gaussian distribution
	 */
	class GaussianDistribution {
	private:
		static bool initialized;
	public:
		static float X();
	};

	class Terrain;
	class Viewer;

	/**
	 * entity interface
	 */
	class Entity {
	private:
		static vector<Entity *> entities;
		static Terrain *terrain;
		static Viewer *viewer;
	public:
		static void initEntities();
		static Terrain &getTerrain();
		static Viewer &getViewer();
		static void updateEntities(SXRenderArea &area);
		static void destroyEntities();
		/**
		 * updates the object
		 */
		virtual void update(SXRenderArea &area) = 0;
	};

	enum ViewerInput {
		VIEWER_ROTATE,
		VIEWER_WASD
	};

	/**
	 * viewer of the scene,
	 * handles user input
	 */
	class Viewer: public Entity {
	private:
		ViewerInput input;
		Vector oldMousePosition;

		bool mouseGrabbed;
		bool painting;
		float distanceChange;

		UniformFloat *width;
		UniformFloat *height;
		Vector *position;
		Vector *view;
		Vector *up;
		Matrix *viewMatrix;
		Matrix *projectionMatrix;

		void processInput(SXRenderArea &area);

		Viewer(const Viewer &);
		Viewer &operator = (const Viewer &);
	public:
		Viewer();
		~Viewer();
		void setViewerInput(ViewerInput input);
		void setMouseGrabbed(bool grabbed);
		void setPainting(bool painting);
		void changeDistance(float delta);
		void update(SXRenderArea &area);
	};

	class Water: public Entity {
	private:
		UniformFloat *waterheight;
		UniformFloat *abovewater;
		Vector *wave1;
		Vector *wave2;
		Vector *position;
		Vector *reflectedPosition;
		Vector *view;
		Vector *up;
		Matrix *modelMatrix;
		Matrix *viewMatrix;

		Water(const Water &);
		Water &operator = (const Water &);
	public:
		Water();
		~Water();
		void update(SXRenderArea &area);
	};

	class Sky: public Entity {
	private:
		Vector *position;
		Vector *reflectedposition;
		Matrix *modelMatrix;
		Matrix *reflectedmodelMatrix;

		Sky(const Sky &);
		Sky &operator = (const Sky &);
	public:
		Sky();
		~Sky();
		void update(SXRenderArea &area);
	};

	/**
	 * terrain class
	 */
	class Terrain: public Entity {
	private:
		unsigned int stepcount;
		unsigned int maxStepcount;
		unsigned int updatecount;

		Vector oldMousePosition;
		Vector oldToolPosition;

		void loadFromImage(const string &heightmapPath, const string &blendmapPath, bool useBlendmap);
		void heightPaint(const Vector &mousePosition, bool raise);

		Terrain(const Terrain &);
		Terrain &operator = (const Terrain &);
	public:
		Terrain();
		~Terrain();

		void saveAsImage(const string &filename);
		void saveAsMesh(const string &filename);
		void loadFromImage(const string &heightmapPath);
		void loadFromImage(const string &heightmapPath, const string &blendmapPath);

		void restart();
		void clampHeight();
		void subdivide();
		Vector getToolPosition(const Vector &mousePosition);
		void variancePaint(const Vector &mousePosition);
		void resetVarianceMap();
		void raiseHeightPaint(const Vector &mousePosition);
		void lowerHeightPaint(const Vector &mousePosition);

		void update(SXRenderArea &area);

		unsigned int getStepCount() const;
		void setMaxStepcount(unsigned int count);
		unsigned int getMaxStepcount() const;
	};

	enum View {
		VIEW_SHADED,
		VIEW_WIREFRAME,
		VIEW_VARIANCE,
		VIEW_HEIGHT
	};

	class Renderer: public SXRenderListener {
	private:
		View view;

		Renderer(const Renderer &);
		Renderer &operator = (const Renderer &);
	public:
		static ShadeX shadeX;

		Renderer();
		void setView(View view);
		void create(SXRenderArea &area);
		void reshape(SXRenderArea &area);
		void render(SXRenderArea &area);
		void stop(SXRenderArea &area);
	};

	class LoadMapDialog: public QDialog {
		Q_OBJECT
	private:
		string heightmapPath;
		string blendmapPath;

		bool receivedOk;
		QLabel *blendmapLabel;
		QPushButton *blendmapButton;
		bool useBlendmap;

		LoadMapDialog(const LoadMapDialog &);
		LoadMapDialog &operator = (const LoadMapDialog &);
	private slots:
		void getHeightmap();
		void getBlendmap();
		void updateUseBlendmap(int checkboxState);
		void receiveOk();
	public:
		LoadMapDialog(QWidget *w);
		~LoadMapDialog();
		string getHeightmapPath() const;
		string getBlendmapPath() const;
		bool isUsingBlendmap() const;
	};

	/**
	 * Landscape generator main window
	 */
	class MainWidget: public QMainWindow {
		
		Q_OBJECT

	private:
		Renderer *renderer;

		QLabel *statusLabel;

		QTabWidget *parametersTabs;

		FloatSlider *globalVariance;
		FloatSlider *globalH;
		FloatSlider *maxHeight;
		FloatSlider *initialHeight;
		FloatSlider *kernelSize;
		FloatSlider *waterHeight;

		FloatSlider *lightRotZ;
		FloatSlider *lightRotY;

		FloatSlider *paintVariance;
		FloatSlider *paintH;
		FloatSlider *paintVarianceRadius;
		FloatSlider *paintVarianceInfluence;
		FloatSlider *paintVarianceIntensity;

		FloatSlider *paintHeightRadius;
		FloatSlider *paintHeightIntensity;

		QComboBox *viewOptions;
		QComboBox *cameraOptions;

		void showParametersTab(const QString &title);

		MainWidget(const MainWidget &);
		MainWidget &operator = (const MainWidget &);
	public slots:
		void updateControl();
		void updateView();

		void updateVariance();
		void updateH();
		void updateMaxHeight();
		void updateTerrainHeight();
		void updateKernelSize();
		void updateWaterHeight();

		void updateLightRot();

		void updatePaintVariance();
		void updatePaintVarianceH();
		void updatePaintVarianceRadius();
		void updatePaintVarianceInfluence();
		void updatePaintVarianceIntensity();
		void resetVarianceMap();

		void updateHeightRadius();
		void updateHeightIntensity();

		void saveAsImage();
		void saveAsMesh();
		void loadFromImage();

		void restartTerrain();
		void subdivideTerrain();
		void updateParametersTab();

		void wheelRotates(float degrees);

		void showInfo();
	public:

		static const string VIEW_SHADED;
		static const string VIEW_WIREFRAME;
		static const string VIEW_VARIANCE;
		static const string VIEW_HEIGHT;

		static const string CONTROL_ROTATE;
		static const string CONTROL_WASD;

		static const string PARAM_GLOBAL;
		static const string PARAM_LIGHT;
		static const string PARAM_VARIANCE;
		static const string PARAM_HEIGHT;
		
		MainWidget();
		~MainWidget();

		void printStatus(const QString &message);
	};

}

#endif