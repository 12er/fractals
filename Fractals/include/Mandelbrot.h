#ifndef _MANDELBROT_H_
#define _MANDELBROT_H_

/**
 * Mandelbrot fractal generator
 * (c) 2016 by Tristan Bauer
 */
#include <FractalGui.h>
#include <QWidget>
#include <QSlider>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QMainWindow>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QDialog>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <map>
using namespace sx;
using namespace std;
using namespace fractals;

namespace mandelbrot {

	class Viewer;
	class Mandelbrot;
	class Julia;

	/**
	 * entity interface
	 */
	class Entity {
	private:
		static vector<Entity *> entities;

		static private Viewer *viewer;
		static private Mandelbrot *mandelbrot;
		static private Julia *julia;
	public:
		static void initEntities();
		static void reshapeEntities(SXRenderArea &area);
		static void updateEntities(SXRenderArea &area);
		static void destroyEntities();

		static Viewer &getViewer();
		static Mandelbrot &getMandelbrot();
		static Julia &getJulia();
		/**
		 * updates the object
		 */
		virtual void reshape(SXRenderArea &area) = 0;
		virtual void update(SXRenderArea &area) = 0;
	};

	class Viewer: public Entity {
	private:
		DVector oldMousePosition;
		float mouseWheelChange;
		bool useJulia;
		bool showJulia;
		float changeShowTime;

		Viewer(const Viewer &);
		Viewer &operator = (const Viewer &);
	public:
		Viewer();
		~Viewer();
		void setUseJulia(bool useJulia);
		void setShowJulia(bool showJulia);
		bool isShowingJulia() const;
		void wheelRotates(float degrees);
		void reshape(SXRenderArea &area);
		void update(SXRenderArea &area);
	};

	enum FractalPrecision {
		FRACTAL_32_BIT,
		FRACTAL_64_BIT
	};

	class Julia: public Entity {
	private:
		bool updateNecessary;
		FractalPrecision precision;

		Julia(const Julia &);
		Julia &operator = (const Julia &);
	public:
		Julia();
		~Julia();
		void setVerticalLength(double length);
		double getVerticalLength();
		void setCenter(const DVector &center);
		DVector getCenter();
		void setC(const DVector &c);
		DVector getC();
		void setPrecision(FractalPrecision precision);
		void saveAsImage(const string &path);
		void setUpdateNecessary();
		void reshape(SXRenderArea &area);
		void update(SXRenderArea &area);
	};

	class Mandelbrot: public Entity {
	private:
		bool updateNecessary;
		FractalPrecision precision;

		Mandelbrot(const Mandelbrot &);
		Mandelbrot &operator = (const Mandelbrot &);
	public:
		Mandelbrot();
		~Mandelbrot();
		void setVerticalLength(double length);
		double getVerticalLength();
		void setCenter(const DVector &center);
		DVector getCenter();
		void setPrecision(FractalPrecision precision);
		void saveAsImage(const string &path);
		void setUpdateNecessary();
		void reshape(SXRenderArea &area);
		void update(SXRenderArea &area);
	};

	class UpdateArea {
	public:
		virtual void updateMandelbrotArea(const Vector &minPos, const Vector &maxPos) = 0;
		virtual void updateJuliaArea(const Vector &minPos, const Vector &maxPos) = 0;
		virtual void updateJuliaSet(const Vector &c) = 0;
		virtual void updateMousepos(const Vector &pos) = 0;
	};

	class Renderer: public SXRenderListener {
	private:
		UpdateArea *updateArea;

		DMatrix oldMandelbrotTransform;
		DMatrix oldJuliaTransform;
		DVector oldMousepos;
		bool oldShowJulia;

		Renderer(const Renderer &);
		Renderer &operator = (const Renderer &);
	public:
		static ShadeX shadeX;

		Renderer(UpdateArea &updateArea);
		~Renderer();
		void refreshUpdateArea(SXRenderArea &area);
		void create(SXRenderArea &area);
		void reshape(SXRenderArea &area);
		void render(SXRenderArea &area);
		void stop(SXRenderArea &area);
	};

	class MainWidget: public QMainWindow, public UpdateArea {
		
		Q_OBJECT

	private:
		Renderer *renderer;

		IntSlider *maxCount;
		QLabel *realMandelbrotInterval;
		QLabel *imaginaryMandelbrotInterval;
		QLabel *realJuliaInterval;
		QLabel *imaginaryJuliaInterval;
		QLabel *juliaSet;
		QLabel *mousePos;

		MainWidget(const MainWidget &);
		MainWidget &operator = (const MainWidget &);
	public slots:
		void wheelRotates(float degrees);

		void updateMaxCount();
		void use64bit(int);
		void useJulia(int);
		void updateMandelbrotArea(const Vector &minPos, const Vector &maxPos);
		void updateJuliaArea(const Vector &minPos, const Vector &maxPos);
		void updateJuliaSet(const Vector &c);
		void updateMousepos(const Vector &pos);
		void resetView();

		void saveMandelbrotAsImage();
		void saveJuliaAsImage();
		void exitApplication();
		void showInfo();
	public:
		MainWidget();
		~MainWidget();
	};

}

#endif